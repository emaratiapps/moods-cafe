package com.themoodscafe.app.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by ian on 14/07/14.
 */
public class PhoneUtil {

	public static void dial(Context context, String phoneNo) {
		dial(context, phoneNo, null);
	}

	public static void dial(Context context, String phoneNo, String title) {
		if (title == null) {
			title = "Call: " + phoneNo;
		}

		String cleanNo = phoneNo.replaceAll("[^\\d]", "");

		if (phoneNo.startsWith("+")) {
			cleanNo = '+' + cleanNo;
		}

		Intent intent = new Intent(Intent.ACTION_DIAL);
		intent.setData(Uri.parse("tel:" + cleanNo));

		try {
			context.startActivity(Intent.createChooser(intent, title));
		} catch (ActivityNotFoundException e) {
			Toast.makeText(context, "Phone app not found", Toast.LENGTH_LONG)
					.show();
		}
	}
}
