package com.themoodscafe.app.util;

/**
 * Created by ian on 09/05/15.
 */
public class TextUtil {

	public static String join(String delimiter, String... strings) {
		StringBuilder sb = new StringBuilder();
		String sep = "";
		for (String s : strings) {
			sb.append(sep).append(s);
			sep = delimiter;
		}
		return sb.toString();
	}
}
