package com.themoodscafe.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 17/03/15.
 */
public class Prefers {
	private static final String TAG                   = Prefers.class.getSimpleName();
	private static final String ARG_COMMON_PREFERENCE = TAG + ".CommonPreferenceArg";

	private static final Prefers INSTANCE = new Prefers();

	private Context context;

	private Prefers() {}

	public static Prefers with(Context context) {
		INSTANCE.context = context;
		return INSTANCE;
	}

	private SharedPreferences.Editor editCommonPrefs() {
		return getCommonPrefs().edit();
	}

	private SharedPreferences getCommonPrefs() {
		return context.getSharedPreferences(ARG_COMMON_PREFERENCE, Context.MODE_PRIVATE);
	}

	public void putBoolean(@NonNull final String key, final boolean value) {
		editCommonPrefs().putBoolean(key, value).commit();
	}

	public boolean getBoolean(@NonNull final String key, final boolean defaultValue) {
		return getCommonPrefs().getBoolean(key, defaultValue);
	}

	public void putDouble(@NonNull final String key, final double value) {
		editCommonPrefs().putLong(key, Double.doubleToRawLongBits(value));
	}

	public double getDouble(@NonNull final String key, final double defaultValue) {
		SharedPreferences pref = getCommonPrefs();
		if (!pref.contains(key)) {
			return defaultValue;
		}
		return Double.longBitsToDouble(getCommonPrefs().getLong(key, 0));
	}

	public void putInt(@NonNull String key, int value) {
		editCommonPrefs().putInt(key, value).commit();
	}

	public int getInt(@NonNull String key, int defaultValue) {
		return getCommonPrefs().getInt(key, defaultValue);
	}

	public static final String DELIMETER = ",";

	public void putIntSet(@NonNull String key, @NonNull List<Integer> values) {
		StringBuilder builder = new StringBuilder();
		String sep = "";
		for (int v : values) {
			builder.append(sep).append(String.valueOf(v));
			sep = DELIMETER;
		}
		editCommonPrefs().putString(key, builder.toString()).commit();
	}

	@Nullable
	public List<Integer> getIntSet(@NonNull String key) {
		String stored = getCommonPrefs().getString(key, null);
		if (stored == null) {
			return null;
		}

		if (stored.contains(DELIMETER)) {
			String[] split = stored.split(DELIMETER);
			List<Integer> values = new ArrayList<>(split.length);
			for (String s : split) {
				try {
					values.add(Integer.parseInt(s));
				} catch (NumberFormatException e) {
					/* TODO Need to clear this Exception */
					Log.e(TAG, "java.lang.NumberFormatException", e);
				}
			}
			return values;
		}

		List<Integer> values = new ArrayList<>(1);
		try {
			values.add(Integer.parseInt(stored));
		} catch (NumberFormatException e) {
			/* TODO: Pointer index out of range */
			Log.e(TAG, "java.lang.NumberFormatException", e);
		}
		return values;
	}
}
