package com.themoodscafe.app.util;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;

import java.util.Hashtable;

/**
 * Created by ian on 26/01/15.
 */
public class FontUtil {

	private static final String TAG        = FontUtil.class.getSimpleName();
	private static final String FONT_DIR   = "fonts";
	private static final String BVOLI_FONT = FONT_DIR + "/mvboli_0.ttf";

	private static final Hashtable<String, Typeface> CACHE = new Hashtable<>();

	@Nullable
	public static Typeface get(Context context, String typefacePath) {
		synchronized (CACHE) {
			if (!CACHE.containsKey(typefacePath)) {
				try {
					Typeface t = Typeface.createFromAsset(context.getAssets(), typefacePath);
					CACHE.put(typefacePath, t);
				} catch (Exception e) {
					Log.e(TAG, "Could not get typeface '" + typefacePath + "' because " + e.getMessage());
					return null;
				}
			}
			return CACHE.get(typefacePath);
		}
	}

	public static void setTypeface(@NonNull TextView view, String typefacePath) {
		Typeface face = FontUtil.get(view.getContext(), typefacePath);
		if (face != null) {
			view.setTypeface(face);
		}
	}

	public static void cacheAll(Context context) {
		get(context, BVOLI_FONT);
	}

	public static void setBvoliTypeface(@NonNull TextView view) {
		setTypeface(view, BVOLI_FONT);
	}
}