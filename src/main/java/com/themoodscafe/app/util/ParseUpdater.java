package com.themoodscafe.app.util;

import android.support.annotation.NonNull;
import android.util.Log;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQuery.FetchFrom;
import com.themoodscafe.app.service.DataStoreQueryListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 05/05/15.
 */
public final class ParseUpdater {

	private static Initializer initializer;

	public static <T extends ParseObject> boolean isNew(T obj, @NonNull List<T> existing) {
		for (T localObj : existing) {
			String localObjectId = localObj.getObjectId();
			if (localObjectId != null && localObjectId.equals(obj.getObjectId())) {
				return false;
			}
		}
		return true;
	}

	public static <T extends ParseObject> List<T> getNew(@NonNull List<T> newList, @NonNull final List<T> oldList) {
		final List<T> newItems = new ArrayList<>(newList.size());

		for (T obj : newList) {
			if (isNew(obj, oldList)) {
				newItems.add(obj);
			}
		}

		return newItems;
	}

	public static boolean initialize(InitializeListener listener) {
		boolean restarted;
		if (initializer != null) {
			restarted = true;
			initializer.cancel();
			initializer = null;
		} else {
			restarted = false;
		}

		initializer = new Initializer();
		initializer.setListener(listener);
		initializer.execute();
		return restarted;
	}

	public static void registerClasses() {
		new Initializer().registerClasses();
	}

	public void detach() {
		if (initializer != null) {
			initializer.setListener(null);
		}
	}

	public void cancel() {
		if (initializer != null) {
			initializer.cancel();
		}
	}

	public interface InitializeListener {
		void onUpdateStart(@NonNull Class<?> theClass);

		void onUpdateComplete(@NonNull Class<?> theClass);

		void onError(@NonNull Class<?> theClass, @NonNull String reason);

		void onProgress(int progress);

		void onAllComplete();
	}

	private static class Initializer {
		private static final String TAG = Initializer.class.getSimpleName();
		private final List<Class<? extends BaseParseClass>> classList;
		private final int                                   classCount;
		private final int                                   taskCount;
		private       Class<? extends BaseParseClass>       currentClass;
		private       WeakReference<InitializeListener>     listenerReference;
		private boolean isCancelled = false;

		public Initializer() {
			classList = new ArrayList<>();
			classList.add(Addon.ParseClass.class);
			classList.add(AddonDetail.ParseClass.class);
			classList.add(/**/Customer.ParseClass.class);
			classList.add(Ingredient.ParseClass.class);
			classList.add(IngredientCategory.ParseClass.class);
			classList.add(/**/IngredientCategoryDetail.ParseClass.class);
			classList.add(IngredientVariation.ParseClass.class);
			classList.add(/**/Order.ParseClass.class);
			classList.add(/**/OrderItem.ParseClass.class);
			classList.add(/**/OrderItemAddon.ParseClass.class);
			classList.add(/**/OrderItemIngredient.ParseClass.class);
			classList.add(/**/OrderStatus.ParseClass.class);
			classList.add(/**/OrderStatusDetail.ParseClass.class);
			classList.add(/**/OrderStatusUpdate.ParseClass.class);
			classList.add(/**/PhoneNumber.ParseClass.class);
			classList.add(Product.ParseClass.class);
			classList.add(/**/ProductDetail.ParseClass.class);
			classList.add(ProductMainCategory.ParseClass.class);
			classList.add(/**/ProductMainCategoryDetail.ParseClass.class);
			classList.add(ProductSubCategory.ParseClass.class);
			classList.add(/**/ProductSubCategoryDetail.ParseClass.class);
			classList.add(ProductVariation.ParseClass.class);
			classList.add(StockStatus.ParseClass.class);
			classList.add(/**/StockStatusDetail.ParseClass.class);
			classList.add(Variation.ParseClass.class);
			classList.add(VariationDetail.ParseClass.class);
			classCount = classList.size();
			taskCount = classCount * 2 + 1;
		}

		private InitializeListener getListener() {
			return listenerReference != null ? listenerReference.get() : null;
		}

		public void setListener(InitializeListener listener) {
			listenerReference = new WeakReference<>(listener);
		}

		public void cancel() {
			isCancelled = true;
		}

		public void execute() {
			ParseUser.enableAutomaticUser();
			registerClasses();
			updateNextClass();
		}

		public void registerClasses() {
			int i = 0;
			for (Class<? extends ParseObject> cls : classList) {
				if (isCancelled) return;

				ParseObject.registerSubclass(cls);
				Log.v(TAG, cls.getCanonicalName() + " class registered");

				InitializeListener listener = getListener();
				if (listener != null) {
					listener.onProgress(++i * 100 / taskCount);
				}
			}
		}

		private void onComplete() {
			InitializeListener listener = getListener();
			if (listener != null) {
				listener.onProgress(100);
				listener.onAllComplete();
			}
		}

		private <T extends BaseParseClass> void update(@NonNull Class<T> theClass) {
			currentClass = theClass;
			classList.remove(theClass);

			DataStoreQuery<T> query = new DataStoreQuery<>(theClass);
			query.setFetchFrom(FetchFrom.BOTH_STORAGE);
			query.setListener(new DataStoreQueryListener<T>() {
				@Override
				public void onQuery(@NonNull ParseQuery<T> query) {}

				@Override
				public void onSuccess(@NonNull List<T> results) {
					InitializeListener listener = getListener();
					if (listener != null) {
						listener.onUpdateComplete(currentClass);
					}
					onPostUpdate();
				}

				@Override
				public void onError(@NonNull String message) {
					InitializeListener listener = getListener();
					if (listener != null) {
						listener.onError(currentClass, message);
					}
					onPostUpdate();
				}

			});
			InitializeListener listener = getListener();
			if (listener != null) {
				listener.onUpdateStart(currentClass);
			}
			query.execute();
		}

		private void onPostUpdate() {
			int sizeLeft = classList.size();
			InitializeListener listener = getListener();
			if (listener != null) {
				listener.onProgress((classCount * 2 - sizeLeft) * 100 / taskCount);
			}

			if (sizeLeft > 0) {
				updateNextClass();
			} else {
				onComplete();
			}
		}

		private void updateNextClass() {
			if (isCancelled) return;

			if (classList.contains(Addon.ParseClass.class)) update(Addon.ParseClass.class);
			else if (classList.contains(AddonDetail.ParseClass.class)) update(AddonDetail.ParseClass.class);
		/**/
			else if (classList.contains(Customer.ParseClass.class)) update(Customer.ParseClass.class);
			else if (classList.contains(Ingredient.ParseClass.class)) update(Ingredient.ParseClass.class);
			else if (classList.contains(IngredientCategory.ParseClass.class))
				update(IngredientCategory.ParseClass.class);
		/**/
			else if (classList.contains(IngredientCategoryDetail.ParseClass.class))
				update(IngredientCategoryDetail.ParseClass.class);
			else if (classList.contains(IngredientVariation.ParseClass.class))
				update(IngredientVariation.ParseClass.class);
		/**/
			else if (classList.contains(Order.ParseClass.class)) update(Order.ParseClass.class);
		/**/
			else if (classList.contains(OrderItem.ParseClass.class)) update(OrderItem.ParseClass.class);
		/**/
			else if (classList.contains(OrderItemAddon.ParseClass.class)) update(OrderItemAddon.ParseClass.class);
		/**/
			else if (classList.contains(OrderItemIngredient.ParseClass.class))
				update(OrderItemIngredient.ParseClass.class);
			else if (classList.contains(OrderStatus.ParseClass.class)) update(OrderStatus.ParseClass.class);
		/**/
			else if (classList.contains(OrderStatusDetail.ParseClass.class)) update(OrderStatusDetail.ParseClass.class);
		/**/
			else if (classList.contains(OrderStatusUpdate.ParseClass.class)) update(OrderStatusUpdate.ParseClass.class);
		/**/
			else if (classList.contains(PhoneNumber.ParseClass.class)) update(PhoneNumber.ParseClass.class);
			else if (classList.contains(Product.ParseClass.class)) update(Product.ParseClass.class);
		/**/
			else if (classList.contains(ProductDetail.ParseClass.class)) update(ProductDetail.ParseClass.class);
			else if (classList.contains(ProductMainCategory.ParseClass.class))
				update(ProductMainCategory.ParseClass.class);
		/**/
			else if (classList.contains(ProductMainCategoryDetail.ParseClass.class))
				update(ProductMainCategoryDetail.ParseClass.class);
			else if (classList.contains(ProductSubCategory.ParseClass.class))
				update(ProductSubCategory.ParseClass.class);
		/**/
			else if (classList.contains(ProductSubCategoryDetail.ParseClass.class))
				update(ProductSubCategoryDetail.ParseClass.class);
			else if (classList.contains(ProductVariation.ParseClass.class)) update(ProductVariation.ParseClass.class);
			else if (classList.contains(StockStatus.ParseClass.class)) update(StockStatus.ParseClass.class);
		/**/
			else if (classList.contains(StockStatusDetail.ParseClass.class)) update(StockStatusDetail.ParseClass.class);
			else if (classList.contains(Variation.ParseClass.class)) update(Variation.ParseClass.class);
			else if (classList.contains(VariationDetail.ParseClass.class)) update(VariationDetail.ParseClass.class);
		}

	}
}
