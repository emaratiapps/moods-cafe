package com.themoodscafe.app.util;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

/**
 * Created by ian on 10/05/15.
 */
public interface SqlUtilListener {
	interface SqlGetWritableListener extends SqlUtilListener {
		void onWritableDatabase(@NonNull SQLiteDatabase db);
		void onSqlError(@NonNull String reason);
	}

	interface SqlGetReadableListener extends SqlUtilListener {
		void onReadableDatabase(@NonNull SQLiteDatabase db);
		void onSqlError(@NonNull String reason);
	}
}
