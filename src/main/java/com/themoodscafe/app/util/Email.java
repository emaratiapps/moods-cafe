package com.themoodscafe.app.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 14/07/14.
 */
public class Email {
	private static final String TAG = Email.class.getSimpleName();

	private Context emailContext;
	private Uri     emailUri;
	private Uri     imageUri;
	private String  chooserTitle;

	private Email(Context context) {
		this.emailContext = context;
	}

	private void setEmailUri(Uri emailUri) {
		this.emailUri = emailUri;
	}

	private void setImageUri(Uri imageUri) {
		this.imageUri = imageUri;
	}

	public void setChooserTitle(String chooserTitle) {
		this.chooserTitle = chooserTitle;
	}

	public static class Builder {
		public static final String TAG = Email.TAG + ".Builder";

		private Context builderContext;
		private List<String> toList  = new ArrayList<String>();
		private List<String> ccList  = new ArrayList<String>();
		private List<String> bccList = new ArrayList<String>();
		private String subject;
		private String body;
		private String chooserTitle;
		private Uri    imageUri;

		public Builder(Context context) {
			this.builderContext = context;
		}

		public Builder addRecipient(String email) {
			this.toList.add(email);
			return this;
		}

		public Builder addCc(String email) {
			this.ccList.add(email);
			return this;
		}

		public Builder addBcc(String email) {
			this.bccList.add(email);
			return this;
		}

		public Builder setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public Builder setMessage(String message) {
			this.body = message;
			return this;
		}

		public Builder setChooserTitle(String message) {
			this.chooserTitle = message;
			return this;
		}

		public Builder setImageUri(Uri uri) {
			this.imageUri = uri;
			return this;
		}

		private static void appendList(StringBuilder builder, List<String> list) {
			int len = list.size();
			if (len > 1) {
				for (String s : list) {
					builder.append(s).append(',');
				}
			} else if (len == 1) {
				builder.append(list.get(0));
			}
		}

		private static String encode(String str) {
			final String encoding = "UTF-8";
			try {
				return URLEncoder.encode(str, encoding);
			} catch (UnsupportedEncodingException e) {
				Log.e(TAG, "UnsupportedEncodingException: " + encoding, e);
			}
			return "%20";
		}

		private String buildData() {
			StringBuilder builder = new StringBuilder();
			builder.append("mailto:");

			if (toList.size() > 0) {
				appendList(builder, toList);
			} else {
				builder.append("%20");
			}

			builder.append('?');
			boolean separate = false;

			if (ccList.size() > 0) {
				builder.append("cc=");
				appendList(builder, toList);
				separate = true;
			}

			if (bccList.size() > 0) {
				if (separate) {
					builder.append("&");
				} else {
					separate = true;
				}
				builder.append("bcc=");
				appendList(builder, toList);
			}

			if (subject != null && subject.length() > 0) {
				if (separate) {
					builder.append("&");
				} else {
					separate = true;
				}
				builder.append("subject=").append(encode(subject));

			}

			if (body != null && body.length() > 0) {
				if (separate) {
					builder.append("&");
				}
				builder.append("body=").append(encode(body));
			}

			return builder.toString();
		}

		private static String abbreviate(List<String> list) {
			String to = list.get(0);
			if (list.size() > 1) {
				to += ", &#8230;";
			}
			return to;
		}

		private static String abbreviate(String str, int cutOff) {
			return str.length() <= cutOff ? str : str.substring(0, cutOff) + "&#8230;";
		}

		private static String abbreviate(String str) {
			return abbreviate(str, 15);
		}

		private String createChooserTitle() {
			if (toList.size() > 0) {
				return "Email: " + abbreviate(toList);
			} else if (ccList.size() > 0) {
				return "Cc: " + abbreviate(ccList);
			} else if (bccList.size() > 0) {
				return "Bcc: " + abbreviate(bccList);
			} else if (subject != null && subject.length() > 0) {
				return "\"" + abbreviate(subject) + "\"";
			} else if (body != null && body.length() > 0) {
				return "\"" + abbreviate(body) + "\"";
			}
			return null;
		}

		public Email build() {
			String mailData = buildData();
			Log.d(TAG, "Email data:\r\n" + mailData);

			Email email = new Email(builderContext);
			email.setEmailUri(Uri.parse(mailData));

			if (imageUri != null) {
				email.setImageUri(imageUri);
			}

			email.setChooserTitle(chooserTitle != null ? chooserTitle : createChooserTitle());

			return email;
		}
	}

	private static boolean openChooser(Email email) {
		Intent sendToIntent = new Intent(Intent.ACTION_SENDTO);
		sendToIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		sendToIntent.setData(email.emailUri);
		if (email.imageUri != null) {
			sendToIntent.putExtra(Intent.EXTRA_STREAM, email.imageUri);
		}

		Context context = email.emailContext;
		if (context != null) {
			try {
				context.startActivity(Intent.createChooser(sendToIntent, email.chooserTitle));
				return true;
			} catch (ActivityNotFoundException e) {
				Toast.makeText(context, "Email app not found", Toast.LENGTH_LONG)
						.show();
			}
		}
		return false;
	}

	public static boolean compose(Context context, String recipient) {
		return openChooser(new Builder(context).addRecipient(recipient).build());
	}

	public static boolean compose(Context context, String[] recipients) {
		Builder b = new Builder(context);
		for (String r : recipients) {
			b.addRecipient(r);
		}
		return openChooser(b.build());
	}

	public static boolean compose(Context context, ArrayList<String> recipients) {
		Builder b = new Builder(context);
		for (String r : recipients) {
			b.addRecipient(r);
		}
		return openChooser(b.build());
	}

	public boolean compose() {
		return openChooser(this);
	}

}