package com.themoodscafe.app.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.util.SqlUtilListener.SqlGetReadableListener;
import com.themoodscafe.app.util.SqlUtilListener.SqlGetWritableListener;

import java.lang.ref.WeakReference;

/**
 * Created by ian on 07/05/15.
 */
public class SqlUtil extends SQLiteOpenHelper {
	private static final String TAG              = SqlUtil.class.getSimpleName();
	private static final int    DATABASE_VERSION = 1;
	private static final String DATABASE_NAME    = "catalogue.db";

	public SqlUtil(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(Addon.SQL_CREATE_TABLE
				           + AddonDetail.SQL_CREATE_TABLE
				           + /**/Customer.SQL_CREATE_TABLE
				           + Ingredient.SQL_CREATE_TABLE
				           + IngredientCategory.SQL_CREATE_TABLE
				           + /**/IngredientCategoryDetail.SQL_CREATE_TABLE
				           + IngredientVariation.SQL_CREATE_TABLE
				           + /**/Order.SQL_CREATE_TABLE
				           + /**/OrderItem.SQL_CREATE_TABLE
				           + /**/OrderItemAddon.SQL_CREATE_TABLE
				           + /**/OrderItemIngredient.SQL_CREATE_TABLE
				           + /**/OrderStatus.SQL_CREATE_TABLE
				           + /**/OrderStatusDetail.SQL_CREATE_TABLE
				           + /**/OrderStatusUpdate.SQL_CREATE_TABLE
				           + /**/PhoneNumber.SQL_CREATE_TABLE
				           + Product.SQL_CREATE_TABLE
				           + /**/ProductDetail.SQL_CREATE_TABLE
				           + ProductMainCategory.SQL_CREATE_TABLE
				           + /**/ProductMainCategoryDetail.SQL_CREATE_TABLE
				           + ProductSubCategory.SQL_CREATE_TABLE
				           + /**/ProductSubCategoryDetail.SQL_CREATE_TABLE
				           + ProductVariation.SQL_CREATE_TABLE
				           + StockStatus.SQL_CREATE_TABLE
				           + /**/StockStatusDetail.SQL_CREATE_TABLE
				           + Variation.SQL_CREATE_TABLE
				           + VariationDetail.SQL_CREATE_TABLE
		          );
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(Addon.SQL_DELETE_TABLE
				           + AddonDetail.SQL_DELETE_TABLE
				           + /**/Customer.SQL_DELETE_TABLE
				           + Ingredient.SQL_DELETE_TABLE
				           + IngredientCategory.SQL_DELETE_TABLE
				           + /**/IngredientCategoryDetail.SQL_DELETE_TABLE
				           + IngredientVariation.SQL_DELETE_TABLE
				           + /**/Order.SQL_DELETE_TABLE
				           + /**/OrderItem.SQL_DELETE_TABLE
				           + /**/OrderItemAddon.SQL_DELETE_TABLE
				           + /**/OrderItemIngredient.SQL_DELETE_TABLE
				           + /**/OrderStatus.SQL_DELETE_TABLE
				           + /**/OrderStatusDetail.SQL_DELETE_TABLE
				           + /**/OrderStatusUpdate.SQL_DELETE_TABLE
				           + /**/PhoneNumber.SQL_DELETE_TABLE
				           + Product.SQL_DELETE_TABLE
				           + /**/ProductDetail.SQL_DELETE_TABLE
				           + ProductMainCategory.SQL_DELETE_TABLE
				           + /**/ProductMainCategoryDetail.SQL_DELETE_TABLE
				           + ProductSubCategory.SQL_DELETE_TABLE
				           + /**/ProductSubCategoryDetail.SQL_DELETE_TABLE
				           + ProductVariation.SQL_DELETE_TABLE
				           + StockStatus.SQL_DELETE_TABLE
				           + /**/StockStatusDetail.SQL_DELETE_TABLE
				           + Variation.SQL_DELETE_TABLE
				           + VariationDetail.SQL_DELETE_TABLE);
		onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}





	private static abstract class SqlUtilTask<L extends SqlUtilListener> extends AsyncTask<Void, Void, SQLiteDatabase> {
		private final Context          context;
		private       WeakReference<L> listenerReference;

		public SqlUtilTask(Context context) {
			this.context = context;
		}

		public SqlUtilTask(Context context, L listener) {
			this(context);
			setListener(listener);
		}

		public void setListener(L listener) {
			this.listenerReference = new WeakReference<L>(listener);
		}

		public L getListener() {
			return listenerReference != null ? listenerReference.get() : null;
		}

		public Context getContext() {
			return context;
		}
	}

	public static void getWritableDatabseInBackground(@NonNull Context context, SqlGetWritableListener listener) {
		new SqlUtilTask<SqlGetWritableListener>(context, listener) {
			private String error;

			@Override
			protected SQLiteDatabase doInBackground(Void... params) {
				SqlUtil util = new SqlUtil(getContext());
				try {
					return util.getWritableDatabase();
				} catch (SQLiteException e) {
					Log.e(TAG, "android.database.SQLiteException", e);
					error = e.getMessage();
				}
				return null;
			}

			@Override
			protected void onPostExecute(SQLiteDatabase sqLiteDatabase) {
				SqlGetWritableListener listener = getListener();
				if (listener != null) {
					if (sqLiteDatabase != null) {
						listener.onWritableDatabase(sqLiteDatabase);
						sqLiteDatabase.close();
					} else {
						listener.onSqlError(error);
					}
				}
			}
		}.execute();
	}

	public static void getReadableDatabseInBackground(@NonNull Context context, SqlGetReadableListener listener) {
		new SqlUtilTask<SqlGetReadableListener>(context, listener) {
			private String error;

			@Override
			protected SQLiteDatabase doInBackground(Void... params) {
				SqlUtil util = new SqlUtil(getContext());
				try {
					return util.getWritableDatabase();
				} catch (SQLiteException e) {
					Log.e(TAG, "android.database.SQLiteException", e);
					error = e.getMessage();
				}
				return null;
			}

			@Override
			protected void onPostExecute(SQLiteDatabase sqlDb) {
				SqlGetReadableListener listener = getListener();
				if (listener != null) {
					if (sqlDb != null) {
						listener.onReadableDatabase(sqlDb);
						sqlDb.close();
					} else {
						listener.onSqlError(error);
					}
				}
			}
		}.execute();
	}
}