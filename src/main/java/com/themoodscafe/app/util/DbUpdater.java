package com.themoodscafe.app.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;
import com.parse.*;
import com.themoodscafe.app.MainApplication;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.util.SqlUtilListener.SqlGetReadableListener;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 05/05/15.
 */
public final class DbUpdater {

	private static Initializer initializer;

	public static <T extends ParseObject> boolean isNew(T obj, @NonNull List<T> existing) {
		for (T localObj : existing) {
			String localObjectId = localObj.getObjectId();
			if (localObjectId != null && localObjectId.equals(obj.getObjectId())) {
				return false;
			}
		}
		return true;
	}

	public static <T extends ParseObject> List<T> getNew(@NonNull List<T> newList, @NonNull final List<T> oldList) {
		final List<T> newItems = new ArrayList<>(newList.size());

		for (T obj : newList) {
			if (isNew(obj, oldList)) {
				newItems.add(obj);
			}
		}

		return newItems;
	}

	public static boolean initialize(InitializeListener listener) {
		if (initializer != null) {
			return false;
		}
		initializer = new Initializer();
		initializer.setInitializeListener(listener);
		initializer.execute();
		return true;
	}

	public static void registerClasses(InitializeListener listener) {
		Initializer initializer = new Initializer(true);
		initializer.setInitializeListener(listener);
		initializer.execute();
	}

	public static void registerClasses() {
		registerClasses(null);
	}

	public void detach() {
		if (initializer != null) {
			initializer.setInitializeListener(null);
		}
	}

	public void cancel() {
		if (initializer != null) {
			initializer.cancel();
		}
	}

	public interface InitializeListener {
		void onUpdateStart(@NonNull Class<?> theClass);

		void onUpdateComplete(@NonNull Class<?> theClass);

		void onError(@NonNull Class<?> theClass, @NonNull String reason);

		void onProgress(int progress);

		void onAllComplete();
	}

	private static class Initializer {
		private static final String TAG = Initializer.class.getSimpleName();
		private final List<Class<? extends BaseParseClass>> classList;
		private final int                                   classCount;
		private final int                                   taskCount;
		private       Class<? extends BaseParseClass>       currentClass;
		private       WeakReference<InitializeListener>     initListenerReference;
		private boolean isCancelled = false;
		private final boolean registerOnly;

		public Initializer(boolean registerOnly) {
			this.registerOnly = registerOnly;
			classList = new ArrayList<>();
			classList.add(Addon.ParseClass.class);
			classList.add(AddonDetail.ParseClass.class);
			classList.add(/**/Customer.ParseClass.class);
			classList.add(Ingredient.ParseClass.class);
			classList.add(IngredientCategory.ParseClass.class);
			classList.add(/**/IngredientCategoryDetail.ParseClass.class);
			classList.add(IngredientVariation.ParseClass.class);
			classList.add(/**/Order.ParseClass.class);
			classList.add(/**/OrderItem.ParseClass.class);
			classList.add(/**/OrderItemAddon.ParseClass.class);
			classList.add(/**/OrderItemIngredient.ParseClass.class);
			classList.add(/**/OrderStatus.ParseClass.class);
			classList.add(/**/OrderStatusDetail.ParseClass.class);
			classList.add(/**/OrderStatusUpdate.ParseClass.class);
			classList.add(/**/PhoneNumber.ParseClass.class);
			classList.add(Product.ParseClass.class);
			classList.add(/**/ProductDetail.ParseClass.class);
			classList.add(ProductMainCategory.ParseClass.class);
			classList.add(/**/ProductMainCategoryDetail.ParseClass.class);
			classList.add(ProductSubCategory.ParseClass.class);
			classList.add(/**/ProductSubCategoryDetail.ParseClass.class);
			classList.add(ProductVariation.ParseClass.class);
			classList.add(StockStatus.ParseClass.class);
			classList.add(/**/StockStatusDetail.ParseClass.class);
			classList.add(Variation.ParseClass.class);
			classList.add(VariationDetail.ParseClass.class);
			classCount = classList.size();
			taskCount = registerOnly ? classCount + 1 : classCount * 2 + 1;
		}

		public Initializer() {
			this(false);
		}

		private InitializeListener getInitializeListener() {
			return initListenerReference != null ? initListenerReference.get() : null;
		}

		public void setInitializeListener(InitializeListener listener) {
			initListenerReference = new WeakReference<>(listener);
		}

		public void cancel() {
			isCancelled = true;
		}

		public void execute() {
			ParseUser.enableAutomaticUser();
			int i = 0;
			for (Class<? extends ParseObject> cls : classList) {
				if (isCancelled) return;
				ParseObject.registerSubclass(cls);

				InitializeListener listener = getInitializeListener();
				if (listener != null) {
					listener.onProgress(++i * 100 / taskCount);
				}
			}
			if (registerOnly) {
				onComplete();
			} else {
				updateNextClass();
			}
		}

		private void onComplete() {
			InitializeListener listener = getInitializeListener();
			if (listener != null) {
				listener.onProgress(100);
				listener.onAllComplete();
			}
		}

		private void onPostUpdate() {
			int sizeLeft = classList.size();
			InitializeListener listener = getInitializeListener();
			if (listener != null) {
				listener.onProgress((classCount * 2 - sizeLeft) * 100 / taskCount);
			}

			if (sizeLeft > 0) {
				updateNextClass();
			} else {
				onComplete();
			}
		}

		protected <T extends BaseParseClass> void synchronizeDb(@NonNull final List<T> results) {
			final T item = results.get(0);
			results.remove(item);

			Context context = MainApplication.getAppContext();
			SqlUtil.getReadableDatabseInBackground(context, new SqlGetReadableListener() {
				@Override
				public void onReadableDatabase(@NonNull SQLiteDatabase db) {
					final BaseEntry entry = item.newDatabaseEntryInstance(db);
					final boolean isNew = entry.existsInDb(db);
					final SQLiteDatabase readableDb = db;

					Context context = MainApplication.getAppContext();
					SqlUtil.getWritableDatabseInBackground(context, new SqlGetWritableListener() {
						@Override
						public void onWritableDatabase(@NonNull SQLiteDatabase db) {
							if (isNew) {
								entry.insert(db);
							} else {
								entry.update(db, readableDb);
							}
							onNextDatabaseItem(results);
						}

						@Override
						public void onSqlError(@NonNull String reason) {
							Log.e(TAG, reason);
							onNextDatabaseItem(results);
						}
					});
				}

				@Override
				public void onSqlError(@NonNull String reason) {
					Log.e(TAG, reason);
					onNextDatabaseItem(results);
				}
			});
		}

		protected <T extends BaseParseClass> void onNextDatabaseItem(@NonNull List<T> results) {
			if (results.size() > 0) {
				synchronizeDb(results);
			} else {
				onPostUpdate();
			}
		}

		private <T extends BaseParseClass> void executeUpdate(@NonNull ParseUpdate<T> parseUpdate) {
			currentClass = parseUpdate.getParseClass();
			classList.remove(currentClass);
			parseUpdate.setListener(new DataStoreQueryListener<T>() {
				@Override
				public void onQuery(@NonNull ParseQuery<T> query) {}

				@Override
				public void onSuccess(@NonNull List<T> results) {
					InitializeListener listener = getInitializeListener();
					if (listener != null) {
						listener.onUpdateComplete(currentClass);
					}
					onPostUpdate();
				}

				@Override
				public void onError(@NonNull String message) {
					InitializeListener listener = getInitializeListener();
					if (listener != null) {
						listener.onError(currentClass, message);
					}
					onPostUpdate();
				}
			});
			InitializeListener listener = getInitializeListener();
			if (listener != null) {
				listener.onUpdateStart(currentClass);
			}
			parseUpdate.queryParse();
		}

		private void updateNextClass() {
			if (isCancelled) return;

			/**
			 *  IMPORTANT: Keep this update order
			 */
			if (classList.contains(Customer.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(Customer.ParseClass.class));
			} else if (classList.contains(PhoneNumber.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(PhoneNumber.ParseClass.class));
			} else if (classList.contains(StockStatus.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(StockStatus.ParseClass.class));
			} else if (classList.contains(StockStatusDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(StockStatusDetail.ParseClass.class));
			} else if (classList.contains(Addon.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(Addon.ParseClass.class));
			} else if (classList.contains(AddonDetail.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(AddonDetail.ParseClass.class));
			} else if (classList.contains(Variation.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(Variation.ParseClass.class));
			} else if (classList.contains(VariationDetail.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(VariationDetail.ParseClass.class));
			} else if (classList.contains(IngredientCategory.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(IngredientCategory.ParseClass.class));
			} else if (classList.contains(IngredientCategoryDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(IngredientCategoryDetail.ParseClass.class));
			} else if (classList.contains(Ingredient.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(Ingredient.ParseClass.class));
			} else if (classList.contains(IngredientVariation.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(IngredientVariation.ParseClass.class));
			} else if (classList.contains(OrderStatus.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(OrderStatus.ParseClass.class));
			} else if (classList.contains(OrderStatusDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(OrderStatusDetail.ParseClass.class));
			} else if (classList.contains(Order.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(Order.ParseClass.class));
			} else if (classList.contains(OrderStatusUpdate.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(OrderStatusUpdate.ParseClass.class));
			} else if (classList.contains(ProductMainCategoryDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(ProductMainCategoryDetail.ParseClass.class));
			} else if (classList.contains(ProductSubCategory.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(ProductSubCategory.ParseClass.class));
			} else if (classList.contains(ProductSubCategoryDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(ProductSubCategoryDetail.ParseClass.class));
			} else if (classList.contains(Product.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(Product.ParseClass.class));
			} else if (classList.contains(ProductDetail.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(ProductDetail.ParseClass.class));
			} else if (classList.contains(ProductMainCategory.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(ProductMainCategory.ParseClass.class));
			} else if (classList.contains(ProductVariation.ParseClass.class)) {
				executeUpdate(new ParseUpdate<>(ProductVariation.ParseClass.class));
			} else if (classList.contains(OrderItem.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(OrderItem.ParseClass.class));
			} else if (classList.contains(OrderItemAddon.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(OrderItemAddon.ParseClass.class));
			} else if (classList.contains(OrderItemIngredient.ParseClass.class)) {
				/**/
				executeUpdate(new ParseUpdate<>(OrderItemIngredient.ParseClass.class));
			}
		}

	}

	public static class ParseUpdate<T extends BaseParseClass> implements FindCallback<T> {
		private static final String TAG = ParseUpdate.class.getSimpleName();

		private Class<T> parseClass;

		public ParseUpdate(Class<T> parseClass) {
			this.parseClass = parseClass;
		}

		public void setParseClass(Class<T> parseClass) {
			this.parseClass = parseClass;
		}

		public Class<T> getParseClass() {
			return parseClass;
		}

		private WeakReference<DataStoreQueryListener<T>> queryListenerReference;

		public void setListener(DataStoreQueryListener<T> listener) {
			queryListenerReference = new WeakReference<>(listener);
		}

		protected DataStoreQueryListener<T> getListener() {
			return queryListenerReference != null ? queryListenerReference.get() : null;
		}

		public void queryParse() {
			ParseQuery<T> query = ParseQuery.getQuery(parseClass);
			DataStoreQueryListener<T> listener = getListener();
			if (listener != null) {
				listener.onQuery(query);
			}
			query.findInBackground(this);
		}

		@Override
		public void done(List<T> list, ParseException e) {
			if (e != null) {
				onResultError("Could not load " + parseClass.getSimpleName() + ": " + e.getMessage());
			} else if (list == null) {
				onResultError("Could not load " + parseClass.getSimpleName() + ": null list");
			} else {
				onResultSuccess(list);
			}
		}

		protected void onResultSuccess(@NonNull List<T> results) {
			DataStoreQueryListener<T> listener = getListener();
			if (listener != null) {
				listener.onSuccess(results);
			}
		}

		protected void onResultError(@NonNull String message) {
			DataStoreQueryListener<T> listener = getListener();
			if (listener != null) {
				listener.onError(message);
			} else {
				Log.v(TAG, "[" + parseClass.getCanonicalName() + "] " + message);
			}
		}
	}
}
