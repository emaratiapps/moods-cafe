package com.themoodscafe.app.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.widget.Toast;
import com.themoodscafe.app.MainApplication;

public class NetUtil {

	private static final String DEFAULT_WARNING = "No internet";
	private static       long   warningInterval = 15 * 60 * 1000;
	private static       long   lastWarningTime = 0;

	private static NetworkInfo getNetworkInfo(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo();
	}

	public static boolean isOnline(Context context) {
		NetworkInfo netInfo = getNetworkInfo(context);
		return netInfo != null && netInfo.isConnected();
	}

	public static boolean isOnlineOrConnecting(Context context) {
		NetworkInfo netInfo = getNetworkInfo(context);
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}

	public static void setWarningInterval(long warningInterval) {
		NetUtil.warningInterval = warningInterval;
	}

	private static void toast(Context context, @NonNull String message) {
		long now = System.currentTimeMillis();
		if (now - lastWarningTime > warningInterval) {
			lastWarningTime = now;
			Toast.makeText(context, message, Toast.LENGTH_SHORT)
					.show();
		}
	}

	private static String getString(Context context, int id) {
		return String.valueOf(context.getResources().getText(id));
	}

	public static boolean isOnlineInform(Context context, String message) {
		if (!isOnline(context)) {
			toast(context, message);
			return false;
		}
		return true;
	}

	public static boolean isOnlineInform(Context context, int message) {
		return isOnlineInform(context, getString(context, message));
	}

	public static boolean isOnlineInform(Context context) {
		return isOnlineInform(context, DEFAULT_WARNING);
	}

	public static boolean isOnlineOrConnectingInform(Context context, String message) {
		if (!isOnlineOrConnecting(context)) {
			toast(context, message);
			return false;
		}
		return true;
	}

	public static boolean isOnlineOrConnectingInform(Context context, int message) {
		return isOnlineOrConnectingInform(context, getString(context, message));
	}

	public static boolean isOnlineOrConnectingInform(Context context) {
		return isOnlineOrConnectingInform(context, DEFAULT_WARNING);
	}

	public static boolean isOnline() {
		return isOnline(MainApplication.getAppContext());
	}

	public static boolean isOnlineOrConnecting() {
		return isOnlineOrConnecting(MainApplication.getAppContext());
	}

	public static boolean isOnlineInform(String message) {
		return isOnlineInform(MainApplication.getAppContext(), message);
	}


	public static boolean isOnlineInform(int message) {
		return isOnlineInform(MainApplication.getAppContext(), message);
	}

	public static boolean isOnlineInform() {
		return isOnlineInform(MainApplication.getAppContext());
	}

	public static boolean isOnlineOrConnectingInform(String message) {
		return isOnlineOrConnectingInform(MainApplication.getAppContext(), message);
	}
}