package com.themoodscafe.app.util;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class WebUtil {
	public static void browse(Context context, String link) {
		browse(context, link, null);
	}

	public static void browse(Context context, String link, int titleResource) {
		browse(context, link, context.getString(titleResource));
	}

	public static void browse(Context context, String link, String title) {
		link = link.trim();

		if (title == null) {
			title = link;
		}

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(Intent.createChooser(intent, title));
	}

	public static String getAppPackage(Context context) {
		return context.getApplicationContext().getPackageName();
	}

	public static void googlePlay(Context context, String packageName) {
		Uri uri = Uri.parse("market://details?id=" + packageName);
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		try {
			context.startActivity(goToMarket);
		} catch (ActivityNotFoundException e) {
			WebUtil.browse(context, "http://play.google.com/store/apps/details?id=" + packageName);
		}
	}

	public static void rate(Context context) {
		googlePlay(context, getAppPackage(context));
	}

	public static void share(Context context) {
		String marketUrl = "https://market.android.com/details?id=" + getAppPackage(context);

		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, marketUrl);

		try {
			context.startActivity(intent);
		} catch (ActivityNotFoundException e) {
			Log.w("ShareUtil", "No activity found", e);
		}
	}
}
