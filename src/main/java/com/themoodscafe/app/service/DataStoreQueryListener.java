package com.themoodscafe.app.service;

import android.support.annotation.NonNull;
import com.parse.ParseQuery;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;

import java.util.List;

/**
 * Created by ian on 07/05/15.
 */
public interface DataStoreQueryListener<T extends BaseParseClass> {
	void onQuery(@NonNull ParseQuery<T> query);
	void onSuccess(@NonNull List<T> results);
	void onError(@NonNull String message);
}