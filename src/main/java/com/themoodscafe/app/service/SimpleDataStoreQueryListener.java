package com.themoodscafe.app.service;

import android.support.annotation.NonNull;
import com.parse.ParseQuery;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;

import java.util.List;

/**
 * Created by ian on 07/05/15.
 */
public class SimpleDataStoreQueryListener<T extends BaseParseClass> implements DataStoreQueryListener<T> {
	public void onQuery(@NonNull ParseQuery<T> query) {}
	public void onSuccess(@NonNull List<T> results) {}
	public void onError(@NonNull String message) {}
}