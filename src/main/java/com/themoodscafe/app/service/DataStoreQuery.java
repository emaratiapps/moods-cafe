package com.themoodscafe.app.service;

import android.support.annotation.NonNull;
import android.util.Log;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;
import com.themoodscafe.app.util.ParseUpdater;

import java.util.Date;
import java.util.List;

/**
 * Created by ian on 07/05/15.
 */
public class DataStoreQuery<T extends BaseParseClass> implements FindCallback<T> {
	private static final String TAG = DataStoreQuery.class.getSimpleName();

	public enum FetchFrom {
		LOCAL_STORAGE,
		REMOTE_STORAGE,
		BOTH_STORAGE,
	}

	private final Class<T> theClass;
	private       List<T>  resultList;
	private FetchFrom fetchFrom     = FetchFrom.LOCAL_STORAGE;
	private boolean   didLocalFetch = false;
	private boolean   pinRemoteData = true;
	private DataStoreQueryListener<T> listener;

	public DataStoreQuery(Class<T> theClass) {
		super();
		this.theClass = theClass;
	}

	public String getClassName() {
		return theClass.getSimpleName();
	}

	public DataStoreQuery<T> setFetchFrom(@NonNull FetchFrom fetchFrom) {
		this.fetchFrom = fetchFrom;
		return this;
	}

	public void setPinRemoteData(boolean pinRemoteData) {
		this.pinRemoteData = pinRemoteData;
	}

	private DataStoreQueryListener<T> getListener() {
		return listener;
//		return listenerReference != null ? listenerReference.get() : null;
	}

	public DataStoreQuery<T> setListener(DataStoreQueryListener<T> listener) {
		this.listener = listener;
//		listenerReference = new SoftReference<>(listener);
		return this;
	}

	@Override
	public void done(List<T> list, ParseException e) {
		if (e != null) {
			onError("Could not load " + getClassName() + ": " + e.getMessage());
		} else if (list == null) {
			onError("Could not load " + getClassName() + ": null list");
		} else {
			onSuccess(list);
		}
	}

	private void queryDatastore(boolean fromLocal) {
		ParseQuery<T> query = ParseQuery.getQuery(theClass);
		query.setLimit(1000);
		if (fromLocal) {
			query.fromLocalDatastore();
		}

		DataStoreQueryListener<T> listener = getListener();
		if (listener != null) {
			listener.onQuery(query);
		}

		query.findInBackground(this);
	}

	public void execute() {
		switch (fetchFrom) {
		case BOTH_STORAGE:
		case LOCAL_STORAGE:
			queryDatastore(true);
			break;

		case REMOTE_STORAGE:
			queryDatastore(false);
			break;

		default:
			throw new IllegalStateException("Invalid value for FetchFrom");
		}
	}

	private void removeOutdatedItems(List<T> newItems, List<T> oldItems) {
		for (final T newItem : newItems) {

			String id = newItem.getObjectId();
			if (id == null) continue;

			T removeItem = null;
			for (T oldItem : oldItems) {
				if (id.equals(oldItem.getObjectId())) {

					Date newUpdatedAt = newItem.getUpdatedAt();

					if (newUpdatedAt != null && newUpdatedAt.after(newItem.getUpdatedAt())) {
						oldItem.unpinInBackground();
						removeItem = oldItem;
					}

					break;
				}
			}

			if (removeItem != null) {
				oldItems.remove(removeItem);
			}
		}
	}

	private void onSuccess(@NonNull List<T> list) {
		if (fetchFrom == FetchFrom.BOTH_STORAGE && !didLocalFetch) {
			didLocalFetch = true;
			resultList = list;
			log("Local data: " + resultList.size() + " items");
			queryDatastore(false);
			return;
		}

		if (resultList == null) {
			resultList = list;
			String source = fetchFrom == FetchFrom.LOCAL_STORAGE ? "Local" : "Remote";
			log(source + " data only: " + list.size() + " items");
			if (pinRemoteData && fetchFrom == FetchFrom.REMOTE_STORAGE) {
				ParseObject.pinAllInBackground(resultList);
			}
		} else {
			removeOutdatedItems(list, resultList);
			List<T> newItems = ParseUpdater.getNew(list, resultList);
			if (pinRemoteData && newItems.size() > 0) {
				ParseObject.pinAllInBackground(newItems);
				resultList.addAll(newItems);
			}
			log("Remote data: " + newItems.size() + " items");
		}

		DataStoreQueryListener<T> listener = getListener();
		if (listener != null) {
			listener.onSuccess(resultList);
			setListener(null);
		}
	}

	private void onError(@NonNull String message) {
		log("Error: " + message);
		DataStoreQueryListener<T> listener = getListener();
		if (listener != null) {
			listener.onError(message);
			setListener(null);
		}
	}

	public static <T extends BaseParseClass> void query(@NonNull Class<T> theClass, DataStoreQueryListener<T> listener) {
		DataStoreQuery<T> query = new DataStoreQuery<>(theClass);
		query.setListener(listener);
		query.execute();
	}

	private void log(@NonNull String message) {
		Log.v(TAG, "[" + theClass.getCanonicalName() + "] " + message);
	}
}