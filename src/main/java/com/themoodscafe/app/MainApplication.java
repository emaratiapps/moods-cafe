package com.themoodscafe.app;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import com.parse.Parse;
import com.themoodscafe.app.util.FontUtil;
import com.themoodscafe.app.util.ParseUpdater;

/**
 * Created by ian on 21/01/15.
 */
public class MainApplication extends Application {
	private static final String TAG = MainApplication.class.getSimpleName();

	private static Context context = null;

	@NonNull
	public static Context getAppContext() {
		if (context == null) {
			throw new IllegalStateException(
					TAG + " or its subclass should be in AndroidManifest.xml -> <application> -> android:name");
		}
		return MainApplication.context;
	}

	public void onCreate() {
		super.onCreate();
		MainApplication.context = getApplicationContext();

		Parse.enableLocalDatastore(this);
		Parse.initialize(this, BuildConfig.PARSE_APP_ID, BuildConfig.PARSE_CLIENT_KEY);
		ParseUpdater.registerClasses();
		FontUtil.cacheAll(this);
	}
}