package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderStatusDetail extends BaseDetailEntry<OrderStatus> {
	private static final String TAG                 = OrderStatusDetail.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_ORDER_STATUS = COL_ORDER_STATUS;
	public static final  String SQL_CREATE_TABLE    = sqlCreateDetailTable(TABLE, COLUMN_ORDER_STATUS, OrderStatus.TABLE);
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public OrderStatusDetail() {}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	protected OrderStatusDetail(Parcel parcel) {
		super(parcel);
	}

	public static final Creator<OrderStatusDetail> CREATOR = new Creator<OrderStatusDetail>() {
		@Override
		public OrderStatusDetail createFromParcel(Parcel parcel) {
			return new OrderStatusDetail(parcel);
		}

		@Override
		public OrderStatusDetail[] newArray(int i) {
			return new OrderStatusDetail[i];
		}
	};

	@Override
	protected String getReferenceColumn() {
		return COLUMN_ORDER_STATUS;
	}

	@ParseClassName("OrderStatusDetail")
	public static class ParseClass extends BaseDetailParseClass<OrderStatusDetail, OrderStatus.ParseClass> {

		public ParseClass() {
			super(COL_ORDER_STATUS);
		}

		public ParseClass(@NonNull OrderStatus.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_ORDER_STATUS, referenceObject, languageCode, name);
		}

		public void setOrderStatus(@NonNull OrderStatus.ParseClass status) {
			setReferencedObject(status);
		}

		public OrderStatus.ParseClass getOrderStatus() {
			return getReferencedObject();
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return OrderStatus.TABLE;
		}

		@NonNull
		@Override
		public OrderStatusDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderStatusDetail entry = new OrderStatusDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
