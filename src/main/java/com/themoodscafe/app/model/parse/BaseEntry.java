package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.parse.ParseObject;
import com.themoodscafe.app.util.TextUtil;

import java.util.Date;

/**
 * Created by ian on 08/05/15.
 */
public abstract class BaseEntry implements BaseColumns, Parcelable {
	private static final   String TAG                       = BaseEntry.class.getSimpleName();
	/* Built In */
	public static final    String COLUMN_OBJECT_ID          = "objectId";
	public static final    String COLUMN_CREATED_AT         = "createdAt";
	public static final    String COLUMN_UPDATED_AT         = "updatedAt";
	public static final    String COLUMN_CREATED_BY         = "createdBy";
	public static final    String COLUMN_UPDATED_BY         = "updatedBy";
	/* Object IDs */
	protected static final String COL_ADDON                 = "addon";
	protected static final String COL_CUSTOMER              = "customer";
	protected static final String COL_INGREDIENT            = "ingredient";
	protected static final String COL_INGREDIENT_CATEGORY   = "ingredientCategory";
	protected static final String COL_INGREDIENT_VARIATION  = "ingredientVariation";
	protected static final String COL_ORDER                 = "order";
	protected static final String COL_ORDER_ITEM            = "orderItem";
	protected static final String COL_ORDER_STATUS          = "orderStatus";
	protected static final String COL_PRODUCT               = "product";
	protected static final String COL_PRODUCT_MAIN_CATEGORY = "productMainCategory";
	protected static final String COL_PRODUCT_SUB_CATEGORY  = "productSubCategory";
	protected static final String COL_PRODUCT_VARIATION     = "productVariation";
	protected static final String COL_VARIATION             = "variation";
	protected static final String COL_STOCK_STATUS          = "stockStatus";
	/* Detail entries */
	protected static final String COL_NAME                  = "name";
	protected static final String COL_DESCRIPTION           = "description";
	protected static final String COL_LANGUAGE_CODE         = "languageCode";
	/* Sortable entries */
	protected static final String COL_SORT_INDEX            = "sortIndex";
	/* Generic */
	protected static final String COL_EMAIL                 = "email";
	protected static final String COL_PHONE_NUMBER          = "phoneNumber";
	protected static final String COL_IS_PRIMARY            = "isPrimary";
	protected static final String COL_IS_TAKEAWAY           = "isTakeaway";
	protected static final String COL_HAS_INGREDIENTS       = "hasIngredients";
	protected static final String COL_IS_DEFAULT            = "isDefault";
	protected static final String COL_IS_DISABLED           = "isDisabled";
	protected static final String COL_SEQUENCE              = "sequence";
	protected static final String COL_NOTE                  = "note";
	protected static final String COL_QUANTITY              = "quantity";
	protected static final String COL_PRICE                 = "price";
	protected static final String COL_PRICE_TOTAL           = "priceTotal";
	protected static final String COL_MIN_PER_RECIPE        = "minPerRecipe";
	protected static final String COL_MAX_PER_RECIPE        = "maxPerRecipe";

	/* Image files */
	protected static final String COL_IMAGE                = "image";
	protected static final String COL_IMAGE_FILE_NAME      = "imageFileName";
	protected static final String COL_IMAGE_FILE_URL       = "imageFileUrl";
	protected static final String COL_TRAY_IMAGE           = "trayImage";
	protected static final String COL_TRAY_IMAGE_FILE_NAME = "trayImageFileName";
	protected static final String COL_TRAY_IMAGE_FILE_URL  = "trayImageFileUrl";

	protected static final String SEP = ",";

	protected long   id;
	protected String backendObjectId;
	protected long   createdAt;
	protected long   updatedAt;
	protected String createdBy;
	protected String updatedBy;

	protected BaseEntry() {}

	protected BaseEntry(Parcel parcel) {
		id = parcel.readLong();
		backendObjectId = parcel.readString();
		createdAt = parcel.readLong();
		updatedAt = parcel.readLong();
		createdBy = parcel.readString();
		updatedBy = parcel.readString();
	}

	public static final String TYPE_NULL      = "NULL";
	public static final String TYPE_INT       = "INTEGER";
	public static final String TYPE_FLOAT     = "REAL";
	public static final String TYPE_TEXT      = "TEXT";
	public static final String TYPE_BLOB      = "BLOB";
	public static final String TYPE_BOOL      = TYPE_INT;
	public static final String TYPE_DATE      = TYPE_INT;
	public static final String TYPE_OBJECT_ID = TYPE_TEXT;

	protected static String typeInt(@NonNull String column) { return column + " " + TYPE_INT; }

	protected static String typeFloat(@NonNull String column) { return column + " " + TYPE_FLOAT; }

	protected static String typeText(@NonNull String column) { return column + " " + TYPE_TEXT; }

	protected static String typeBool(@NonNull String column) { return column + " " + TYPE_BOOL; }

	protected static String typeDate(@NonNull String column) { return column + " " + TYPE_DATE; }

	protected static String typeObjectId(@NonNull String column) { return column + " " + TYPE_OBJECT_ID; }

	protected static String typeRemoteFile(@NonNull String filenameColumn, @NonNull String urlColumn) {
		return typeText(filenameColumn) + "," + typeText(urlColumn);
	}

	protected static String typeReference(@NonNull String column, @NonNull String referenceTable, @NonNull String referenceColumn) {
		return "FOREIGN KEY(" + column + ") REFERENCES " + referenceTable + "(" + referenceColumn + ")";
	}

	protected static String typeReferenceId(@NonNull String column, @NonNull String table) {
		return typeReference(column, table, _ID);
	}

	protected static String sqlCreateTableWithColumnArray(@NonNull String tableName, String[] columnArray) {
		return "CREATE TABLE " + tableName
				+ "(" + typeInt(_ID) + " PRIMARY_KEY"
				+ typeObjectId(COLUMN_OBJECT_ID) + SEP
				+ typeDate(COLUMN_CREATED_AT) + SEP
				+ typeDate(COLUMN_UPDATED_AT) + SEP
				+ typeObjectId(COLUMN_CREATED_BY) + SEP
				+ typeObjectId(COLUMN_UPDATED_BY) + SEP
				+ TextUtil.join(SEP, columnArray)
				+ ");";
	}

	protected static String sqlCreateTable(@NonNull String tableName, String... columns) {
		return sqlCreateTableWithColumnArray(tableName, columns);
	}

	protected static String sqlDeleteTable(@NonNull String tableName) {
		return "DROP TABLE IF EXISTS " + tableName + ";";
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBackendObjectId() {
		return backendObjectId;
	}

	public void setBackendObjectId(String backendObjectId) {
		this.backendObjectId = backendObjectId;
	}

	public long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(long createdAt) {
		this.createdAt = createdAt;
	}

	public long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@NonNull
	public abstract String getTableName();

	protected String getNullableColumn() {
		return null;
	}

	public boolean isUpdatedThan(@NonNull BaseEntry old) {
		return updatedAt > old.updatedAt;
	}

	public boolean isNewerThan(@NonNull BaseEntry old) {
		return createdAt > old.createdAt;
	}

	public Cursor getCursor(@NonNull SQLiteDatabase readableDb, boolean useObjectId, @Nullable String... columns) {
		int length = 4;
		if (columns != null) {
			length += columns.length;
		}
		String[] proj = new String[length];
		int i = 0;
		proj[i++] = _ID;
		proj[i++] = COLUMN_OBJECT_ID;
		proj[i++] = COLUMN_CREATED_AT;
		proj[i++] = COLUMN_UPDATED_AT;
		proj[i++] = COLUMN_CREATED_BY;
		proj[i++] = COLUMN_UPDATED_BY;
		if (columns != null) {
			for (String c : columns) {
				proj[i++] = c;
			}
		}
		String selection = (useObjectId ? COLUMN_OBJECT_ID : _ID) + "=";
		String[] args = new String[]{(useObjectId ? backendObjectId : String.valueOf(id))};
		return getCursor(readableDb, proj, selection, args, null, null, null);
	}

	public Cursor getCursor(@NonNull SQLiteDatabase readableDb, boolean useObjectId) {
		String selection = (useObjectId ? COLUMN_OBJECT_ID : _ID) + "=";
		String[] args = new String[]{(useObjectId ? backendObjectId : String.valueOf(id))};
		return getCursor(readableDb, getAllColumns(), selection, args, null, null, null);
	}

	public static long getIdFromObjectId(@NonNull SQLiteDatabase readableDb, @NonNull String tableName, @NonNull BaseParseClass parseobj) {
		String[] columns = new String[]{_ID, COLUMN_OBJECT_ID};
		String selection = COLUMN_OBJECT_ID + "=";
		String[] args = new String[]{parseobj.getObjectId()};
		Cursor cursor = readableDb.query(tableName, columns, selection, args, null, null, null);
		long result = -1;
		if (cursor.moveToFirst()) {
			result = getLong(cursor, _ID);
		}
		cursor.close();
		return result;
	}

	@NonNull
	public Cursor getCursor(@NonNull SQLiteDatabase readableDb, String[] columns, String selection,
	                        String[] selectionArgs, String groupBy, String having,
	                        String orderBy) {
		return readableDb.query(getTableName(), columns, selection, selectionArgs, groupBy, having, orderBy);
	}

	public boolean existsInDb(@NonNull SQLiteDatabase readableDb) {
		Cursor cursor = getCursor(readableDb, true);
		boolean exists = cursor.moveToFirst();
		cursor.close();
		return exists;
	}

	public long insert(@NonNull SQLiteDatabase writableDb, @NonNull ContentValues contentValues) {
		return writableDb.insert(getTableName(), getNullableColumn(), contentValues);
	}

	public long insert(@NonNull SQLiteDatabase writableDb) {
		return insert(writableDb, getAllValues());
	}

	public int update(@NonNull SQLiteDatabase writableDb, @NonNull ContentValues contentValues) {
		return writableDb.update(getTableName(), contentValues, _ID + "=", new String[]{String.valueOf(id)});
	}

	public int update(@NonNull SQLiteDatabase writableDb, @NonNull SQLiteDatabase readableDb) {
		ContentValues cv = getChangedValues(readableDb);
		if (cv != null) {
			return update(writableDb, cv);
		}
		return 0;
	}

	protected abstract void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row);

	@Nullable
	protected ContentValues getChangedValues(@NonNull SQLiteDatabase readableDb) {
		Cursor row = getCursor(readableDb, true);
		if (!row.moveToFirst()) {
			return new ContentValues();
		}

		long createdAt = getLong(row, COLUMN_CREATED_AT);
		long updatedAt = getLong(row, COLUMN_UPDATED_AT);
		String createdBy = getString(row, COLUMN_CREATED_BY);
		String updatedBy = getString(row, COLUMN_UPDATED_BY);

		ContentValues cv = new ContentValues();
		if (createdAt != this.createdAt) {
			cv.put(COLUMN_CREATED_AT, this.createdAt);
		}
		if (updatedAt != this.updatedAt) {
			cv.put(COLUMN_UPDATED_AT, this.updatedAt);
		}
		if (createdBy == null || !createdBy.equals(this.createdBy)) {
			cv.put(COLUMN_CREATED_BY, this.createdBy);
		}
		if (updatedBy == null || !updatedBy.equals(this.updatedBy)) {
			cv.put(COLUMN_UPDATED_BY, this.updatedBy);
		}
		onAllChangedValues(cv, row);

		row.close();
		return cv;
	}

	protected abstract void onAllContentValues(@NonNull ContentValues cv);

	@NonNull
	public ContentValues getAllValues() {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_OBJECT_ID, backendObjectId);
		cv.put(COLUMN_CREATED_AT, createdAt);
		cv.put(COLUMN_UPDATED_AT, updatedAt);
		cv.put(COLUMN_CREATED_BY, createdBy);
		cv.put(COLUMN_UPDATED_BY, updatedBy);
		onAllContentValues(cv);
		return cv;
	}

	@NonNull
	protected String[] getAllColumns() {
		return new String[]{
				COLUMN_OBJECT_ID,
				COLUMN_CREATED_AT,
				COLUMN_UPDATED_AT,
				COLUMN_CREATED_BY,
				COLUMN_UPDATED_BY
		};
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(id);
		dest.writeString(backendObjectId);
		dest.writeLong(createdAt);
		dest.writeLong(updatedAt);
		dest.writeString(createdBy);
		dest.writeString(updatedBy);
	}

	public static abstract class BaseParseClass<ENTRY extends BaseEntry> extends ParseObject {
		protected BaseParseClass() {}

		protected BaseParseClass(@NonNull String table) {
			super(table);
		}

		@NonNull
		public abstract ENTRY newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb);// { return null; }

		protected void fillEntry(@NonNull ENTRY entry) {
			entry.setBackendObjectId(getObjectId());
			Date createdAt = getUpdatedAt();
			entry.setUpdatedAt(createdAt != null ? createdAt.getTime() : 0);
			Date updatedAt = getUpdatedAt();
			entry.setUpdatedAt(updatedAt != null ? updatedAt.getTime() : 0);
//			String createdBy = getUpdatedBy();
//			entry.setUpdatedBy(createdBy != null ? createdBy.getTime() : 0);
//			String updatedBy = getUpdatedBy();
//			entry.setUpdatedBy(updatedBy != null ? updatedBy.getTime() : 0);
		}
	}

	protected static String getString(@NonNull Cursor cursor, String column) {
		return cursor.getString(cursor.getColumnIndex(column));
	}

	protected static int getInt(@NonNull Cursor cursor, String column) {
		return cursor.getInt(cursor.getColumnIndex(column));
	}

	protected static Long getLong(@NonNull Cursor cursor, String column) {
		return cursor.getLong(cursor.getColumnIndex(column));
	}

	protected static float getFloat(@NonNull Cursor cursor, String column) {
		return cursor.getFloat(cursor.getColumnIndex(column));
	}

	protected static boolean getBoolean(@NonNull Cursor cursor, String column) {
		return getInt(cursor, column) != 0;
	}

	protected static String getStringOrThrow(@NonNull Cursor cursor, String column) {
		return cursor.getString(cursor.getColumnIndexOrThrow(column));
	}

	protected static int getIntOrThrow(@NonNull Cursor cursor, String column) {
		return cursor.getInt(cursor.getColumnIndexOrThrow(column));
	}

	protected static Long getLongOrThrow(@NonNull Cursor cursor, String column) {
		return cursor.getLong(cursor.getColumnIndexOrThrow(column));
	}

	protected static float getFloatOrThrow(@NonNull Cursor cursor, String column) {
		return cursor.getFloat(cursor.getColumnIndexOrThrow(column));
	}

	protected static void putStringIfChanged(@NonNull Cursor row, String columnName, String value, @NonNull ContentValues cv) {
		String old = getString(row, columnName);
		if (old == null || !old.equals(value)) {
			cv.put(columnName, value);
		}
	}

	protected static void putIntIfChanged(@NonNull Cursor row, String columnName, int value, @NonNull ContentValues cv) {
		int old = getInt(row, columnName);
		if (old != value) {
			cv.put(columnName, value);
		}
	}

	protected static void putLongIfChanged(@NonNull Cursor row, String columnName, long value, @NonNull ContentValues cv) {
		long old = getLong(row, columnName);
		if (old != value) {
			cv.put(columnName, value);
		}
	}

	protected static void putFloatIfChanged(@NonNull Cursor row, String columnName, float value, @NonNull ContentValues cv) {
		float old = getFloat(row, columnName);
		if (old != value) {
			cv.put(columnName, value);
		}
	}

	protected static void putBooleanIfChanged(@NonNull Cursor row, String columnName, boolean value, @NonNull ContentValues cv) {
		boolean old = getBoolean(row, columnName);
		if (old != value) {
			cv.put(columnName, value);
		}
	}
}
