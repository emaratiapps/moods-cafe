package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class PhoneNumber extends BaseEntry {
	private static final String TAG                 = PhoneNumber.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_CUSTOMER     = COL_CUSTOMER;
	public static final  String COLUMN_PHONE_NUMBER = COL_PHONE_NUMBER;
	public static final  String COLUMN_IS_PRIMARY   = COL_IS_PRIMARY;
	public static final  String SQL_CREATE_TABLE    = sqlCreateTable(
			TABLE,
			typeReferenceId(COLUMN_CUSTOMER, Customer.TABLE),
			typeText(COLUMN_PHONE_NUMBER),
			typeBool(COLUMN_IS_PRIMARY));
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public static final Creator<PhoneNumber> CREATOR = new Creator<PhoneNumber>() {
		@Override
		public PhoneNumber createFromParcel(Parcel parcel) {
			return new PhoneNumber(parcel);
		}

		@Override
		public PhoneNumber[] newArray(int i) {
			return new PhoneNumber[i];
		}
	};

	protected long     customerId;
	protected String  phoneNumber;
	protected boolean isPrimary;

	public PhoneNumber() {}

	protected PhoneNumber(Parcel parcel) {
		super(parcel);
		customerId = parcel.readLong();
		phoneNumber = parcel.readString();
		isPrimary = parcel.readInt() != 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(customerId);
		dest.writeString(phoneNumber);
		dest.writeInt(isPrimary ? 1 : 0);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putLongIfChanged(row, COLUMN_CUSTOMER, customerId, cv);
		putStringIfChanged(row, COLUMN_PHONE_NUMBER, phoneNumber, cv);
		putBooleanIfChanged(row, COLUMN_IS_PRIMARY, isPrimary, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_CUSTOMER, customerId);
		cv.put(COLUMN_PHONE_NUMBER, phoneNumber);
		cv.put(COLUMN_IS_PRIMARY, isPrimary);
	}

	@ParseClassName("PhoneNumber")
	public static class ParseClass extends BaseParseClass<PhoneNumber> {

		public ParseClass() {}

		public ParseClass(@NonNull Customer.ParseClass customer,
		                  @NonNull String phoneNumber) {
			super(TABLE);
			setCustomer(customer);
			setPhoneNumber(phoneNumber);
		}

		public String getPhoneNumber() {
			return getString(COL_PHONE_NUMBER);
		}

		public void setPhoneNumber(@NonNull String phoneNumber) {
			put(COL_PHONE_NUMBER, phoneNumber);
		}

		public void setIsPrimary(boolean isPrimary) {
			put(COL_IS_PRIMARY, isPrimary);
		}

		public boolean isPrimary() {
			return getBoolean(COL_IS_PRIMARY);
		}

		public Customer.ParseClass getCustomer() {
			return (Customer.ParseClass) getParseObject(COL_CUSTOMER);
		}

		public void setCustomer(@NonNull Customer.ParseClass customer) {
			put(COL_CUSTOMER, customer);
		}

		@NonNull
		@Override
		public PhoneNumber newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			PhoneNumber entry = new PhoneNumber();
			fillEntry(entry);
			entry.setCustomerId(getIdFromObjectId(readableDb, Customer.TABLE, getCustomer()));
			entry.setPhoneNumber(getPhoneNumber());
			entry.setIsPrimary(isPrimary());
			return entry;
		}
	}
}