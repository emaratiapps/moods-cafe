package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.support.annotation.NonNull;

/**
 * Created by ian on 08/05/15.
 */
public abstract class BaseSortableEntry<T extends BaseSortableEntry> extends BaseEntry implements Comparable<T> {
	public static final String COLUMN_SORT_INDEX = COL_SORT_INDEX;

	protected int sortIndex;

	public BaseSortableEntry() {}

	protected BaseSortableEntry(Parcel parcel) {
		super(parcel);
		sortIndex = parcel.readInt();
	}

	protected static String sqlCreateSortableTable(@NonNull String table, @NonNull String... columns) {
		String[] colExtend = new String[columns.length + 1];
		int i = 0;
		for (String c : columns) {
			colExtend[i++] = c;
		}
		colExtend[i] = typeInt(COLUMN_SORT_INDEX);
		return sqlCreateTableWithColumnArray(table, colExtend);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeInt(sortIndex);
	}

	public int getSortIndex() {
		return sortIndex;
	}

	public void setSortIndex(int sortIndex) {
		this.sortIndex = sortIndex;
	}

	@Override
	public int compareTo(@NonNull T another) {
		return sortIndex - another.sortIndex;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		int sortIndex = getInt(row, COLUMN_SORT_INDEX);
		if (sortIndex != this.sortIndex) {
			cv.put(COLUMN_SORT_INDEX, sortIndex);
		}
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_SORT_INDEX, sortIndex);
	}

	public static abstract class BaseSortableParseClass<ENTRY extends BaseSortableEntry, COMPARE extends BaseSortableParseClass>
			extends BaseParseClass<ENTRY>
			implements Comparable<COMPARE>
	{
		protected BaseSortableParseClass() {
			super();
		}

		protected BaseSortableParseClass(@NonNull String table) {
			super(table);
		}

		public int getSortIndex() {
			return getInt(COL_SORT_INDEX);
		}

		public void setSortIndex(int index) {
			put(COL_SORT_INDEX, index);
		}

		@Override
		public int compareTo(@NonNull COMPARE another) {
			return getSortIndex() - another.getSortIndex();
		}

		protected void fillSortableEntry(ENTRY entry) {
			fillEntry(entry);
			entry.setSortIndex(getSortIndex());
		}
	}
}
