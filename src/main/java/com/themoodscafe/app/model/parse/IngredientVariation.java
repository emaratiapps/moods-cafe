package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by ian on 08/05/15.
 */
public class IngredientVariation extends BaseSortableEntry<IngredientVariation> {
	private static final String TAG                         = IngredientVariation.class.getSimpleName();
	public static final  String TABLE                       = TAG;
	public static final  String COLUMN_INGREDIENT           = COL_INGREDIENT;
	public static final  String COLUMN_VARIATION            = COL_VARIATION;
	public static final  String COLUMN_PRICE                = COL_PRICE;
	public static final  String COLUMN_STOCK_STATUS         = COL_STOCK_STATUS;
	public static final  String COLUMN_TRAY_IMAGE_FILE_NAME = COL_TRAY_IMAGE_FILE_NAME;
	public static final  String COLUMN_TRAY_IMAGE_FILE_URL  = COL_TRAY_IMAGE_FILE_URL;
	public static final  String SQL_CREATE_TABLE            = sqlCreateSortableTable(
			TABLE,
			typeReferenceId(COLUMN_INGREDIENT, Ingredient.TABLE),
			typeReferenceId(COLUMN_VARIATION, Variation.TABLE),
			typeFloat(COLUMN_PRICE),
			typeReferenceId(COLUMN_STOCK_STATUS, StockStatus.TABLE),
			typeRemoteFile(COLUMN_TRAY_IMAGE_FILE_NAME, COLUMN_TRAY_IMAGE_FILE_URL));
	public static final  String SQL_DELETE_TABLE            = sqlDeleteTable(TABLE);

	protected long   ingredientId;
	protected long   variationId;
	protected float  price;
	protected long   stockStatusId;
	protected String imageFileName;
	protected String imageFileUrl;

	public IngredientVariation() {}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(ingredientId);
		dest.writeLong(variationId);
		dest.writeFloat(price);
		dest.writeLong(stockStatusId);
		dest.writeString(imageFileName);
		dest.writeString(imageFileUrl);
	}

	protected IngredientVariation(Parcel parcel) {
		super(parcel);
		ingredientId = parcel.readLong();
		variationId = parcel.readLong();
		price = parcel.readFloat();
		stockStatusId = parcel.readLong();
		imageFileName = parcel.readString();
		imageFileUrl = parcel.readString();
	}

	public static final Creator<IngredientVariation> CREATOR = new Creator<IngredientVariation>() {
		@Override
		public IngredientVariation createFromParcel(Parcel parcel) {
			return new IngredientVariation(parcel);
		}

		@Override
		public IngredientVariation[] newArray(int i) {
			return new IngredientVariation[i];
		}
	};

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getIngredientId() {
		return ingredientId;
	}

	public void setIngredientId(long ingredientId) {
		this.ingredientId = ingredientId;
	}

	public long getVariationId() {
		return variationId;
	}

	public void setVariationId(long variationId) {
		this.variationId = variationId;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getStockStatusId() {
		return stockStatusId;
	}

	public void setStockStatusId(long stockStatusId) {
		this.stockStatusId = stockStatusId;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@ParseClassName("IngredientVariation")
	public static class ParseClass extends BaseSortableParseClass<IngredientVariation, IngredientVariation.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull Ingredient.ParseClass ingredient,
		                  float price) {
			super(TABLE);
			setIngredient(ingredient);
			setPrice(price);
		}

		public ParseClass(@NonNull Ingredient.ParseClass ingredient,
		                  @NonNull Variation.ParseClass variation,
		                  float price) {
			this(ingredient, price);
			setVariation(variation);
		}

		public void setIngredient(@NonNull Ingredient.ParseClass ingredient) {
			put(COL_INGREDIENT, ingredient);
		}

		public Ingredient.ParseClass getIngredient() {
			return (Ingredient.ParseClass) getParseObject(COL_INGREDIENT);
		}

		public void setVariation(@NonNull Variation.ParseClass variation) {
			put(COL_VARIATION, variation);
		}

		public Variation.ParseClass getVariation() {
			return (Variation.ParseClass) getParseObject(COL_VARIATION);
		}

		public void setStockStatus(@NonNull StockStatus.ParseClass status) {
			put(COL_STOCK_STATUS, status);
		}

		public StockStatus.ParseClass getStockStatus() {
			return (StockStatus.ParseClass) get(COL_STOCK_STATUS);
		}

		public void setPrice(float price) {
			put(COL_PRICE, price);
		}

		public float getPrice() {
			Number number = getNumber(COL_PRICE);
			return number == null ? 0.0f : number.floatValue();
		}

		public void setTrayImage(@NonNull ParseFile image) {
			put(COL_TRAY_IMAGE, image);
		}

		public ParseFile getTrayImage() {
			return getParseFile(COL_TRAY_IMAGE);
		}

		@NonNull
		@Override
		public IngredientVariation newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			IngredientVariation entry = new IngredientVariation();
			fillSortableEntry(entry);
			entry.setIngredientId(getIdFromObjectId(readableDb, Ingredient.TABLE, getIngredient()));
			entry.setVariationId(getIdFromObjectId(readableDb, Variation.TABLE, getVariation()));
			entry.setPrice(getPrice());
			entry.setStockStatusId(getIdFromObjectId(readableDb, StockStatus.TABLE, getStockStatus()));
			ParseFile trayImage = getTrayImage();
			if (trayImage != null) {
//			entry.setImageFileName();
				entry.setImageFileUrl(trayImage.getUrl());
			}
			return entry;
		}
	}

}