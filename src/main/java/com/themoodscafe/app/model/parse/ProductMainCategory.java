package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class ProductMainCategory extends BaseSortableEntry<ProductMainCategory> {
	private static final String TAG                = ProductMainCategory.class.getSimpleName();
	public static final  String TABLE              = TAG;
	public static final  String COLUMN_NAME        = COL_NAME;
	public static final  String SQL_CREATE_TABLE   = sqlCreateSortableTable(TABLE, typeText(COLUMN_NAME));
	public static final  String COLUMN_IS_DISABELD = COL_IS_DISABLED;
	public static final  String SQL_DELETE_TABLE   = sqlDeleteTable(TABLE);

	public static final Creator<ProductMainCategory> CREATOR = new Creator<ProductMainCategory>() {
		@Override
		public ProductMainCategory createFromParcel(Parcel parcel) {
			return new ProductMainCategory(parcel);
		}

		@Override
		public ProductMainCategory[] newArray(int i) {
			return new ProductMainCategory[i];
		}
	};

	protected String name;

	public ProductMainCategory() {}

	protected ProductMainCategory(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		putStringIfChanged(row, COLUMN_NAME, name, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_NAME, this.name);
	}

	@ParseClassName("ProductMainCategory")
	public static class ParseClass extends BaseSortableParseClass<ProductMainCategory, ProductMainCategory.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name) {
			super(TABLE);
			setName(name);
		}

		public ParseClass(@NonNull String name, int sortIndex) {
			this(name);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public boolean isDisabled() {
			return getBoolean(COL_IS_DISABLED);
		}

		@NonNull
		@Override
		public ProductMainCategory newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductMainCategory entry = new ProductMainCategory();
			fillSortableEntry(entry);
			entry.setName(getName());
			return entry;
		}
	}
}