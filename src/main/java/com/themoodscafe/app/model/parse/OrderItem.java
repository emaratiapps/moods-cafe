package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderItem extends BaseEntry {
	private static final String TAG                      = OrderItem.class.getSimpleName();
	public static final  String TABLE                    = TAG;
	public static final  String COLUMN_ORDER             = COL_ORDER;
	public static final  String COLUMN_PRODUCT_VARIATION = COL_PRODUCT_VARIATION;
	public static final  String COLUMN_QUANTITY          = COL_QUANTITY;
	public static final  String SQL_CREATE_TABLE         = sqlCreateTable(
			TABLE,
			typeReferenceId(COLUMN_ORDER, Order.TABLE),
			typeReferenceId(COLUMN_PRODUCT_VARIATION, ProductVariation.TABLE),
			typeInt(COLUMN_QUANTITY));
	public static final  String SQL_DELETE_TABLE         = sqlDeleteTable(TABLE);

	public static final Creator<OrderItem> CREATOR = new Creator<OrderItem>() {
		@Override
		public OrderItem createFromParcel(Parcel parcel) {
			return new OrderItem(parcel);
		}

		@Override
		public OrderItem[] newArray(int i) {
			return new OrderItem[i];
		}
	};

	protected long orderId;
	protected long productVariation;
	protected int  quantity;

	public OrderItem() {}

	protected OrderItem(Parcel parcel) {
		super(parcel);
		orderId = parcel.readLong();
		productVariation = parcel.readLong();
		quantity = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(orderId);
		dest.writeLong(productVariation);
		dest.writeInt(quantity);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getProductVariation() {
		return productVariation;
	}

	public void setProductVariation(long productVariation) {
		this.productVariation = productVariation;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putLongIfChanged(row, COLUMN_ORDER, this.orderId, cv);
		putLongIfChanged(row, COLUMN_PRODUCT_VARIATION, this.productVariation, cv);
		putIntIfChanged(row, COLUMN_QUANTITY, this.quantity, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_ORDER, this.orderId);
		cv.put(COLUMN_PRODUCT_VARIATION, this.productVariation);
		cv.put(COLUMN_QUANTITY, this.quantity);
	}

	@ParseClassName("OrderItem")
	public static class ParseClass extends BaseParseClass<OrderItem> {

		public ParseClass() {}

		public ParseClass(@NonNull Order.ParseClass order,
		                  @NonNull ProductVariation.ParseClass productVariation,
		                  int quantity) {
			super(TABLE);
			setOrder(order);
			setProductVariation(productVariation);
			setQuantity(quantity);
		}

		public Order.ParseClass getOrder() {
			return (Order.ParseClass) getParseObject(COL_ORDER);
		}

		public void setOrder(@NonNull Order.ParseClass order) {
			put(COL_ORDER, order);
		}

		public ProductVariation.ParseClass getProductVariation() {
			return (ProductVariation.ParseClass) getParseObject(COL_PRODUCT_VARIATION);
		}

		public void setProductVariation(@NonNull ProductVariation.ParseClass variation) {
			put(COL_PRODUCT_VARIATION, variation);
		}

		public int getQuantity() {
			return getInt(COL_QUANTITY);
		}

		public void setQuantity(int quantity) {
			put(COL_QUANTITY, quantity);
		}

		@NonNull
		@Override
		public OrderItem newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderItem entry = new OrderItem();
			fillEntry(entry);
			entry.setOrderId(getIdFromObjectId(readableDb, Order.TABLE, getOrder()));
			entry.setProductVariation(getIdFromObjectId(readableDb, ProductVariation.TABLE, getProductVariation()));
			entry.setQuantity(getQuantity());
			return entry;
		}
	}
}
