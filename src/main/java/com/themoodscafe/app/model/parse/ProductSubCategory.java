package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class ProductSubCategory extends BaseSortableEntry<ProductSubCategory> {
	private static final String TAG                          = ProductSubCategory.class.getSimpleName();
	public static final  String TABLE                        = TAG;
	public static final  String COLUMN_NAME                  = COL_NAME;
	public static final  String COLUMN_PRODUCT_MAIN_CATEGORY = COL_PRODUCT_MAIN_CATEGORY;
	public static final  String COLUMN_IS_DISABELD           = COL_IS_DISABLED;
	public static final  String SQL_CREATE_TABLE             = sqlCreateSortableTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeReferenceId(COLUMN_PRODUCT_MAIN_CATEGORY, ProductMainCategory.TABLE));
	public static final  String SQL_DELETE_TABLE             = sqlDeleteTable(TABLE);

	public static final Creator<ProductSubCategory> CREATOR = new Creator<ProductSubCategory>() {
		@Override
		public ProductSubCategory createFromParcel(Parcel parcel) {
			return new ProductSubCategory(parcel);
		}

		@Override
		public ProductSubCategory[] newArray(int i) {
			return new ProductSubCategory[i];
		}
	};

	protected String name;
	protected long   productMainCategory;

	public ProductSubCategory() {}

	protected ProductSubCategory(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
		productMainCategory = parcel.readLong();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
		dest.writeLong(productMainCategory);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getProductMainCategory() {
		return productMainCategory;
	}

	public void setProductMainCategory(long productMainCategory) {
		this.productMainCategory = productMainCategory;
	}

	@ParseClassName("ProductSubCategory")
	public static class ParseClass extends BaseSortableParseClass<ProductSubCategory, ProductSubCategory.ParseClass> {
		public ParseClass() {}

		public ParseClass(@NonNull String name) {
			super(TABLE);
			setName(name);
		}

		public ParseClass(@NonNull String name, int sortIndex) {
			this(name);
			setSortIndex(sortIndex);
		}

		public ParseClass(@NonNull String name, @NonNull ProductMainCategory.ParseClass category, int sortIndex) {
			this(name);
			setProductMainCategory(category);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public ProductMainCategory.ParseClass getProductMainCategory() {
			return (ProductMainCategory.ParseClass) get(COL_PRODUCT_MAIN_CATEGORY);
		}

		public void setProductMainCategory(@NonNull ProductMainCategory.ParseClass category) {
			put(COL_PRODUCT_MAIN_CATEGORY, category);
		}

		public boolean isDisabled() {
			return getBoolean(COL_IS_DISABLED);
		}

		@NonNull
		@Override
		public ProductSubCategory newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductSubCategory entry = new ProductSubCategory();
			fillSortableEntry(entry);
			entry.setName(getName());
			entry.setProductMainCategory(getIdFromObjectId(readableDb, ProductMainCategory.TABLE, getProductMainCategory()));
			return entry;
		}
	}
}