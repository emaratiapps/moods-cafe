package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderStatus extends BaseEntry {
	private static final String TAG              = OrderStatus.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_NAME      = COL_NAME;
	public static final  String COLUMN_SEQUENCE  = COL_SEQUENCE;
	public static final  String SQL_CREATE_TABLE = sqlCreateTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeInt(COLUMN_SEQUENCE));
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<OrderStatus> CREATOR = new Creator<OrderStatus>() {
		@Override
		public OrderStatus createFromParcel(Parcel parcel) {
			return new OrderStatus(parcel);
		}

		@Override
		public OrderStatus[] newArray(int i) {
			return new OrderStatus[i];
		}
	};

	protected String name;
	protected int    sequence;

	public OrderStatus() {}

	protected OrderStatus(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
		sequence = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
		dest.writeInt(sequence);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSequence() {
		return sequence;
	}

	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putStringIfChanged(row, COLUMN_NAME, name, cv);
		putIntIfChanged(row, COLUMN_SEQUENCE, sequence, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_NAME, name);
		cv.put(COLUMN_SEQUENCE, sequence);
	}

	@ParseClassName("OrderStatus")
	public static class ParseClass extends BaseParseClass<OrderStatus> {

		public ParseClass() {}

		public ParseClass(@NonNull String name, int sequence) {
			super(TABLE);
			setName(name);
			setSequence(sequence);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public int getSequence() {
			return getInt(COL_SEQUENCE);
		}

		public void setSequence(int sequence) {
			put(COL_SEQUENCE, sequence);
		}

		@NonNull
		@Override
		public OrderStatus newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderStatus entry = new OrderStatus();
			fillEntry(entry);
			entry.setName(getName());
			entry.setSequence(getSequence());
			return entry;
		}
	}
}
