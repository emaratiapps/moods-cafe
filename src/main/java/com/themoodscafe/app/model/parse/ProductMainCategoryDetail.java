package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class ProductMainCategoryDetail extends BaseDetailEntry<ProductMainCategory> {
	private static final String TAG                          = ProductMainCategoryDetail.class.getSimpleName();
	public static final  String TABLE                        = TAG;
	public static final  String COLUMN_PRODUCT_MAIN_CATEGORY = COL_PRODUCT_MAIN_CATEGORY;
	public static final  String SQL_CREATE_TABLE             = sqlCreateDetailTable(TABLE, COLUMN_PRODUCT_MAIN_CATEGORY, ProductMainCategory.TABLE);
	public static final  String SQL_DELETE_TABLE             = sqlDeleteTable(TABLE);

	public static final Creator<ProductMainCategoryDetail> CREATOR = new Creator<ProductMainCategoryDetail>() {
		@Override
		public ProductMainCategoryDetail createFromParcel(Parcel parcel) {
			return new ProductMainCategoryDetail(parcel);
		}

		@Override
		public ProductMainCategoryDetail[] newArray(int i) {
			return new ProductMainCategoryDetail[i];
		}
	};

	public ProductMainCategoryDetail() {}

	protected ProductMainCategoryDetail(Parcel parcel) {
		super(parcel);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_PRODUCT_MAIN_CATEGORY;
	}

	@ParseClassName("ProductMainCategoryDetail")
	public static class ParseClass extends BaseDetailParseClass<ProductMainCategoryDetail, ProductMainCategory.ParseClass> {
		public ParseClass() {
			super(COL_PRODUCT_MAIN_CATEGORY);
		}

		public ParseClass(@NonNull ProductMainCategory.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_PRODUCT_MAIN_CATEGORY, referenceObject, languageCode, name);
		}

		protected ProductMainCategory.ParseClass getProductMainCategory() {
			return (ProductMainCategory.ParseClass) getParseObject(COL_PRODUCT_MAIN_CATEGORY);
		}

		public void setProductMainCategory(@NonNull ProductMainCategory.ParseClass category) {
			put(COL_PRODUCT_MAIN_CATEGORY, category);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return ProductMainCategory.TABLE;
		}

		@NonNull
		@Override
		public ProductMainCategoryDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductMainCategoryDetail entry = new ProductMainCategoryDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
