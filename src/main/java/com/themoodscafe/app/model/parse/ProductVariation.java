package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by ian on 08/05/15.
 */
public class ProductVariation extends BaseSortableEntry<ProductVariation> {
	private static final String TAG                         = ProductVariation.class.getSimpleName();
	public static final  String TABLE                       = TAG;
	public static final  String COLUMN_PRODUCT              = COL_PRODUCT;
	public static final  String COLUMN_VARIATION            = COL_VARIATION;
	public static final  String COLUMN_PRICE                = COL_PRICE;
	public static final  String COLUMN_STOCK_STATUS         = COL_STOCK_STATUS;
	public static final  String COLUMN_TRAY_IMAGE_FILE_NAME = COL_TRAY_IMAGE_FILE_NAME;
	public static final  String COLUMN_TRAY_IMAGE_FILE_URL  = COL_TRAY_IMAGE_FILE_URL;
	public static final  String SQL_CREATE_TABLE            = sqlCreateSortableTable(
			TABLE,
			typeReferenceId(COLUMN_PRODUCT, Product.TABLE),
			typeReferenceId(COLUMN_VARIATION, Variation.TABLE),
			typeFloat(COLUMN_PRICE),
			typeReferenceId(COLUMN_STOCK_STATUS, StockStatus.TABLE),
			typeRemoteFile(COLUMN_TRAY_IMAGE_FILE_NAME, COLUMN_TRAY_IMAGE_FILE_URL));
	public static final  String SQL_DELETE_TABLE            = sqlDeleteTable(TABLE);

	public static final Creator<ProductVariation> CREATOR = new Creator<ProductVariation>() {
		@Override
		public ProductVariation createFromParcel(Parcel parcel) {
			return new ProductVariation(parcel);
		}

		@Override
		public ProductVariation[] newArray(int i) {
			return new ProductVariation[i];
		}
	};

	protected long   productId;
	protected long   variationId;
	protected float  price;
	protected long   stockStatusId;
	protected String imageFileName;
	protected String imageFileUrl;

	public ProductVariation() {}

	protected ProductVariation(Parcel parcel) {
		super(parcel);
		productId = parcel.readLong();
		variationId = parcel.readLong();
		price = parcel.readFloat();
		stockStatusId = parcel.readLong();
		imageFileName = parcel.readString();
		imageFileUrl = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(productId);
		dest.writeLong(variationId);
		dest.writeFloat(price);
		dest.writeLong(stockStatusId);
		dest.writeString(imageFileName);
		dest.writeString(imageFileUrl);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getVariationId() {
		return variationId;
	}

	public void setVariationId(long variationId) {
		this.variationId = variationId;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getStockStatusId() {
		return stockStatusId;
	}

	public void setStockStatusId(long stockStatusId) {
		this.stockStatusId = stockStatusId;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@ParseClassName("ProductVariation")
	public static class ParseClass extends BaseSortableParseClass<ProductVariation, ProductVariation.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull Product.ParseClass product,
		                  float price) {
			super(TABLE);
			setProduct(product);
			setPrice(price);
		}

		public ParseClass(@NonNull Product.ParseClass product,
		                  @NonNull Variation.ParseClass variation,
		                  float price) {
			this(product, price);
			setVariation(variation);
		}

		public float getPrice() {
			Number number = getNumber(COL_PRICE);
			return number == null ? 0.0f : number.floatValue();
		}

		public void setPrice(float price) {
			put(COL_PRICE, price);
		}

		@NonNull
		public Product.ParseClass getProduct() {
			return (Product.ParseClass) getParseObject(COL_PRODUCT);
		}

		public void setProduct(@NonNull Product.ParseClass product) {
			put(COL_PRODUCT, product);
		}

		public Variation.ParseClass getVariation() {
			return (Variation.ParseClass) getParseObject(COL_VARIATION);
		}

		public void setVariation(@NonNull Variation.ParseClass variation) {
			put(COL_VARIATION, variation);
		}

		public StockStatus.ParseClass getStockStatus() {
			return (StockStatus.ParseClass) get(COL_STOCK_STATUS);
		}

		public void setStockStatus(@NonNull StockStatus status) {
			put(COL_STOCK_STATUS, status);
		}

		public ParseFile getTrayImage() {
			return getParseFile(COL_TRAY_IMAGE);
		}

		public void setTrayImage(@NonNull ParseFile image) {
			put(COL_TRAY_IMAGE, image);
		}

		@NonNull
		@Override
		public ProductVariation newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductVariation entry = new ProductVariation();
			fillSortableEntry(entry);
			entry.setProductId(getIdFromObjectId(readableDb, Product.TABLE, getProduct()));
			entry.setVariationId(getIdFromObjectId(readableDb, Variation.TABLE, getVariation()));
			entry.setPrice(getPrice());
			entry.setStockStatusId(getIdFromObjectId(readableDb, StockStatus.TABLE, getStockStatus()));

			ParseFile image = getTrayImage();
			if (image != null) {
//				entry.setImageFileName();
				entry.setImageFileUrl(image.getUrl());
			}
			return entry;
		}
	}
}