package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class ProductDetail extends BaseDetailEntry<Product> {
	private static final String TAG              = ProductDetail.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_PRODUCT   = COL_PRODUCT;
	public static final  String SQL_CREATE_TABLE = sqlCreateDetailTable(TABLE, COLUMN_PRODUCT, Product.TABLE);
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<ProductDetail> CREATOR = new Creator<ProductDetail>() {
		@Override
		public ProductDetail createFromParcel(Parcel parcel) {
			return new ProductDetail(parcel);
		}

		@Override
		public ProductDetail[] newArray(int i) {
			return new ProductDetail[i];
		}
	};

	public ProductDetail() {}

	protected ProductDetail(Parcel parcel) {
		super(parcel);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_PRODUCT;
	}

	@ParseClassName("ProductDetail")
	public static class ParseClass extends BaseDetailParseClass<ProductDetail, Product.ParseClass> {

		public ParseClass() {
			super(COL_PRODUCT);
		}

		public ParseClass(@NonNull Product.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_PRODUCT, referenceObject, languageCode, name);
		}

		@NonNull
		public Product.ParseClass getProduct() {
			return (Product.ParseClass) getParseObject(COL_PRODUCT);
		}

		public void setProduct(@NonNull Product.ParseClass product) {
			put(COL_PRODUCT, product);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return Product.TABLE;

		}

		@NonNull
		@Override
		public ProductDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductDetail entry = new ProductDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
