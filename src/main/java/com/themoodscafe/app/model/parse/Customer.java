package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class Customer extends BaseEntry {
	private static final String TAG              = Customer.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_EMAIL     = COL_EMAIL;
	public static final  String SQL_CREATE_TABLE = sqlCreateTable(TABLE, typeText(COLUMN_EMAIL));
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<Customer> CREATOR = new Creator<Customer>() {
		@Override
		public Customer createFromParcel(Parcel parcel) {
			return new Customer(parcel);
		}

		@Override
		public Customer[] newArray(int i) {
			return new Customer[i];
		}
	};

	protected String email;

	public Customer() {}

	protected Customer(Parcel parcel) {
		super(parcel);
		parcel.writeString(email);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(email);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		String email = getString(row, COLUMN_EMAIL);
		if (email == null || !email.equals(this.email)) {
			cv.put(COLUMN_EMAIL, this.email);
		}
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_EMAIL, this.email);
	}

	@ParseClassName("Customer")
	public static class ParseClass extends BaseParseClass<Customer> {

		public ParseClass() {}

		public ParseClass(@NonNull String email) {
			super(TABLE);
			setEmail(email);
		}

		public String getEmail() {
			return getString(COL_EMAIL);
		}

		public void setEmail(@NonNull String email) {
			put(COL_EMAIL, email);
		}

		@NonNull
		@Override
		public Customer newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Customer entry = new Customer();
			fillEntry(entry);
			entry.setEmail(getEmail());
			return entry;
		}
	}
}
