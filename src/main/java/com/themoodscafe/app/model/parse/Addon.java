package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class Addon extends BaseSortableEntry<Addon> {
	private static final String TAG                 = Addon.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_NAME         = COL_NAME;
	public static final  String COLUMN_PRICE        = COL_PRICE;
	public static final  String COLUMN_STOCK_STATUS = COL_STOCK_STATUS;
	public static final  String SQL_CREATE_TABLE    = sqlCreateSortableTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeFloat(COLUMN_PRICE),
			typeReferenceId(COLUMN_STOCK_STATUS, StockStatus.TABLE));
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public static final Creator<Addon> CREATOR = new Creator<Addon>() {
		@Override
		public Addon createFromParcel(Parcel parcel) {
			return new Addon(parcel);
		}

		@Override
		public Addon[] newArray(int i) {
			return new Addon[i];
		}
	};

	protected String name;
	protected float  price;
	protected long   stockStatusId;

	public Addon() {}

	protected Addon(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
		price = parcel.readFloat();
		stockStatusId = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
		dest.writeFloat(price);
		dest.writeLong(stockStatusId);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getStockStatusId() {
		return stockStatusId;
	}

	public void setStockStatusId(long stockStatusId) {
		this.stockStatusId = stockStatusId;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		putStringIfChanged(row, COLUMN_NAME, this.name, cv);
		putFloatIfChanged(row, COLUMN_PRICE, this.price, cv);
		putLongIfChanged(row, COLUMN_STOCK_STATUS, this.stockStatusId, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_NAME, this.name);
		cv.put(COLUMN_PRICE, this.price);
		cv.put(COLUMN_STOCK_STATUS, this.stockStatusId);
	}

	@ParseClassName("Addon")
	public static class ParseClass extends BaseSortableParseClass<Addon, Addon.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name,
		                  @NonNull ProductSubCategory.ParseClass productSubCategory,
		                  float price,
		                  int sortIndex) {
			super(TABLE);
			setName(name);
			setPrice(price);
			setProductSubCategory(productSubCategory);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public float getPrice() {
			Number number = getNumber(COL_PRICE);
			return number == null ? 0.0f : number.floatValue();
		}

		public void setPrice(float price) {
			put(COL_PRICE, price);
		}

		public StockStatus.ParseClass getStockStatus() {
			return (StockStatus.ParseClass) get(COL_STOCK_STATUS);
		}

		public void setStockStatus(@NonNull StockStatus status) {
			put(COL_STOCK_STATUS, status);
		}

		protected ProductSubCategory.ParseClass getProductSubCategory() {
			return (ProductSubCategory.ParseClass) getParseObject(COL_PRODUCT_SUB_CATEGORY);
		}

		public void setProductSubCategory(@NonNull ProductSubCategory.ParseClass category) {
			put(COL_PRODUCT_SUB_CATEGORY, category);
		}

		@NonNull
		@Override
		public Addon newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Addon entry = new Addon();
			fillSortableEntry(entry);
			entry.setName(getName());
			entry.setPrice(getPrice());
			entry.setStockStatusId(getIdFromObjectId(readableDb, StockStatus.TABLE, getStockStatus()));
			return entry;
		}
	}
}