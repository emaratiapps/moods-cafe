package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by ian on 08/05/15.
 */
public abstract class BaseDetailEntry<Entry extends BaseEntry> extends BaseEntry {
	public static final String COLUMN_NAME          = COL_NAME;
	public static final String COLUMN_DESCRIPTION   = COL_DESCRIPTION;
	public static final String COLUMN_LANGUAGE_CODE = COL_LANGUAGE_CODE;

	protected String languageCode;
	protected String name;
	protected String description;
	protected long   referenceId;

	public BaseDetailEntry() {}

	protected BaseDetailEntry(Parcel parcel) {
		super(parcel);
		languageCode = parcel.readString();
		name = parcel.readString();
		description = parcel.readString();
		referenceId = parcel.readInt();
	}

	protected static String sqlCreateDetailTable(@NonNull String table, @NonNull String column, @NonNull String referenceTable) {
		return sqlCreateTable(
				table,
				typeText(COLUMN_LANGUAGE_CODE),
				typeText(COLUMN_NAME),
				typeText(COLUMN_DESCRIPTION),
				typeReferenceId(column, referenceTable));
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	protected long getReferenceId() {
		return referenceId;
	}

	protected void setReferenceId(long referenceId) {
		this.referenceId = referenceId;
	}

	protected void setReferenceId(Entry referenceObject) {
		this.referenceId = referenceObject != null ? referenceObject.getId() : 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(languageCode);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeLong(referenceId);
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		String languageCode = getString(row,COLUMN_LANGUAGE_CODE);
		String name = getString(row, COLUMN_NAME);
		String description = getString(row, COLUMN_DESCRIPTION);
		if (languageCode == null || !languageCode.equals(this.languageCode)) {
			cv.put(COLUMN_LANGUAGE_CODE, this.languageCode);
		}
		if (name == null || !name.equals(this.name)) {
			cv.put(COLUMN_NAME, this.name);
		}
		if (description == null || !description.equals(this.description)) {
			cv.put(COLUMN_DESCRIPTION, this.description);
		}
		String referenceColumn = getReferenceColumn();
		long referenceId = getLong(row, referenceColumn);
		if (referenceId != this.referenceId) {
			cv.put(referenceColumn, this.referenceId);
		}
	}

	protected abstract String getReferenceColumn();

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_LANGUAGE_CODE, this.languageCode);
		cv.put(COLUMN_NAME, this.name);
		cv.put(COLUMN_DESCRIPTION, this.description);
		cv.put(getReferenceColumn(), this.referenceId);
	}

	public static abstract class BaseDetailParseClass<ENTRY extends BaseDetailEntry, REFERENCE extends BaseParseClass> extends BaseParseClass<ENTRY> {

		private final String referenceColumn;

		protected BaseDetailParseClass(@NonNull String referenceColumn) {
			this.referenceColumn = referenceColumn;
		}

		protected BaseDetailParseClass(@NonNull String table,
		                               @NonNull String referenceColumn,
		                               @NonNull REFERENCE referenceObject,
		                               @NonNull String languageCode,
		                               @NonNull String name) {
			super(table);
			this.referenceColumn = referenceColumn;
			setReferencedObject(referenceObject);
			setLanguageCode(languageCode);
			setName(name);
		}

		public String getLanguageCode() {
			return getString(COL_LANGUAGE_CODE);
		}

		public void setLanguageCode(@NonNull String name) {
			put(COL_LANGUAGE_CODE, name);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		@Nullable
		public String getDescription() {
			return getString(COL_DESCRIPTION);
		}

		public void setDescription(@Nullable String name) {
			put(COL_DESCRIPTION, name);
		}

		@NonNull
		@SuppressWarnings("unchecked")
		protected REFERENCE getReferencedObject() {
			return (REFERENCE) getParseObject(referenceColumn);
		}

		protected void setReferencedObject(@NonNull REFERENCE object) {
			put(referenceColumn, object);
		}

		protected void fillDetailEntry(@NonNull SQLiteDatabase readableDb, @NonNull ENTRY entry) {
			fillEntry(entry);
			entry.setLanguageCode(getLanguageCode());
			entry.setName(getName());
			entry.setDescription(getDescription());
			entry.setReferenceId(getIdFromObjectId(readableDb, getReferenceTableName(), getReferencedObject()));
		}

		@NonNull
		protected abstract String getReferenceTableName();
	}
}
