package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderStatusUpdate extends BaseEntry {
	private static final String TAG                 = OrderStatusUpdate.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_ORDER_STATUS = COL_ORDER_STATUS;
	public static final  String COLUMN_ORDER        = COL_ORDER;
	public static final  String SQL_CREATE_TABLE    = sqlCreateTable(
			TABLE,
			typeReferenceId(COLUMN_ORDER_STATUS, OrderStatus.TABLE),
			typeReferenceId(COLUMN_ORDER, Order.TABLE));
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public static final Creator<OrderStatusUpdate> CREATOR = new Creator<OrderStatusUpdate>() {
		@Override
		public OrderStatusUpdate createFromParcel(Parcel parcel) {
			return new OrderStatusUpdate(parcel);
		}

		@Override
		public OrderStatusUpdate[] newArray(int i) {
			return new OrderStatusUpdate[i];
		}
	};

	protected long orderStatusId;
	protected long orderId;

	public OrderStatusUpdate() {}

	protected OrderStatusUpdate(Parcel parcel) {
		super(parcel);
		orderStatusId = parcel.readLong();
		orderId = parcel.readLong();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(orderStatusId);
		dest.writeLong(orderId);
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putLongIfChanged(row, COLUMN_ORDER_STATUS, orderStatusId, cv);
		putLongIfChanged(row, COLUMN_ORDER, orderId, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_ORDER_STATUS, orderStatusId);
		cv.put(COLUMN_ORDER, orderId);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(long orderStatusId) {
		this.orderStatusId = orderStatusId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	@ParseClassName("OrderStatusUpdate")
	public static class ParseClass extends BaseParseClass<OrderStatusUpdate> {

		public ParseClass() {}

		public ParseClass(@NonNull OrderStatus.ParseClass orderStatus,
		                  @NonNull Order.ParseClass order) {
			super(TABLE);
			setOrderStatus(orderStatus);
			setOrder(order);
		}

		public OrderStatus.ParseClass getOrderStatus() {
			return (OrderStatus.ParseClass) get(COL_ORDER_STATUS);
		}

		public void setOrderStatus(@NonNull OrderStatus.ParseClass status) {
			put(COL_ORDER_STATUS, status);
		}

		public Order.ParseClass getOrder() {
			return (Order.ParseClass) getParseObject(COL_ORDER);
		}

		public void setOrder(@NonNull Order.ParseClass order) {
			put(COL_ORDER, order);
		}

		@NonNull
		@Override
		public OrderStatusUpdate newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderStatusUpdate entry = new OrderStatusUpdate();
			fillEntry(entry);
			entry.setOrderStatusId(getIdFromObjectId(readableDb, OrderStatus.TABLE, getOrderStatus()));
			entry.setOrderId(getIdFromObjectId(readableDb, Order.TABLE, getOrder()));
			return entry;
		}
	}
}
