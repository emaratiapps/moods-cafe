package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderItemIngredient extends BaseEntry {
	private static final String TAG                         = OrderItemIngredient.class.getSimpleName();
	public static final  String TABLE                       = TAG;
	public static final  String COLUMN_ORDER_ITEM           = COL_ORDER_ITEM;
	public static final  String COLUMN_INGREDIENT_VARIATION = COL_INGREDIENT_VARIATION;
	public static final  String COLUMN_QUANTITY             = COL_QUANTITY;
	public static final  String SQL_CREATE_TABLE            = sqlCreateTable(
			TABLE,
			typeReferenceId(COLUMN_ORDER_ITEM, OrderItem.TABLE),
			typeReferenceId(COLUMN_INGREDIENT_VARIATION, IngredientVariation.TABLE),
			typeInt(COLUMN_QUANTITY));
	public static final  String SQL_DELETE_TABLE            = sqlDeleteTable(TABLE);

	public static final Creator<OrderItemIngredient> CREATOR = new Creator<OrderItemIngredient>() {
		@Override
		public OrderItemIngredient createFromParcel(Parcel parcel) {
			return new OrderItemIngredient(parcel);
		}

		@Override
		public OrderItemIngredient[] newArray(int i) {
			return new OrderItemIngredient[i];
		}
	};

	protected long orderItemId;
	protected long ingredientVariationId;
	protected int quantity;

	public OrderItemIngredient() {}

	protected OrderItemIngredient(Parcel parcel) {
		super(parcel);
		orderItemId = parcel.readLong();
		ingredientVariationId = parcel.readLong();
		quantity = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(orderItemId);
		dest.writeLong(ingredientVariationId);
		dest.writeInt(quantity);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public long getIngredientVariationId() {
		return ingredientVariationId;
	}

	public void setIngredientVariationId(long ingredientVariationId) {
		this.ingredientVariationId = ingredientVariationId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putLongIfChanged(row, COLUMN_ORDER_ITEM, orderItemId, cv);
		putLongIfChanged(row, COLUMN_INGREDIENT_VARIATION, ingredientVariationId, cv);
		putIntIfChanged(row, COLUMN_QUANTITY, quantity, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_ORDER_ITEM, orderItemId);
		cv.put(COLUMN_INGREDIENT_VARIATION, ingredientVariationId);
		cv.put(COLUMN_QUANTITY, quantity);
	}

	@ParseClassName("OrderItemIngredient")
	public static class ParseClass extends BaseParseClass<OrderItemIngredient> {

		public ParseClass() {}

		public ParseClass(@NonNull OrderItem.ParseClass orderItem,
		                  @NonNull IngredientVariation.ParseClass ingredientVariation,
		                  int quantity) {
			super(TABLE);
			setOrderItem(orderItem);
			setIngredientVariation(ingredientVariation);
			setQuantity(quantity);
		}

		public OrderItem.ParseClass getOrderItem() {
			return (OrderItem.ParseClass) get(COL_ORDER_ITEM);
		}

		public void setOrderItem(@NonNull OrderItem.ParseClass item) {
			put(COL_ORDER_ITEM, item);
		}

		public IngredientVariation.ParseClass getIngredientVariation() {
			return (IngredientVariation.ParseClass) getParseObject(COL_ADDON);
		}

		public void setIngredientVariation(@NonNull IngredientVariation.ParseClass ingredientVariation) {
			put(COL_ADDON, ingredientVariation);
		}

		public int getQuantity() {
			return getInt(COL_QUANTITY);
		}

		public void setQuantity(int quantity) {
			put(COL_QUANTITY, quantity);
		}

		@NonNull
		@Override
		public OrderItemIngredient newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderItemIngredient entry = new OrderItemIngredient();
			fillEntry(entry);
			entry.setOrderItemId(getIdFromObjectId(readableDb, OrderItem.TABLE, getOrderItem()));
			entry.setIngredientVariationId(getIdFromObjectId(readableDb, Addon.TABLE, getIngredientVariation()));
			entry.setQuantity(getQuantity());
			return entry;
		}
	}
}
