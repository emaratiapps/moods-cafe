package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class ProductSubCategoryDetail extends BaseDetailEntry<ProductSubCategory> {
	private static final String TAG                         = ProductSubCategoryDetail.class.getSimpleName();
	public static final  String TABLE                       = TAG;
	public static final  String COLUMN_PRODUCT_SUB_CATEGORY = COL_PRODUCT_SUB_CATEGORY;
	public static final  String SQL_CREATE_TABLE            = sqlCreateDetailTable(TABLE, COLUMN_PRODUCT_SUB_CATEGORY, ProductSubCategory.TABLE);
	public static final  String SQL_DELETE_TABLE            = sqlDeleteTable(TABLE);

	public static final Creator<ProductSubCategoryDetail> CREATOR = new Creator<ProductSubCategoryDetail>() {
		@Override
		public ProductSubCategoryDetail createFromParcel(Parcel parcel) {
			return new ProductSubCategoryDetail(parcel);
		}

		@Override
		public ProductSubCategoryDetail[] newArray(int i) {
			return new ProductSubCategoryDetail[i];
		}
	};

	public ProductSubCategoryDetail() {}

	protected ProductSubCategoryDetail(Parcel parcel) {
		super(parcel);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_PRODUCT_SUB_CATEGORY;
	}

	@ParseClassName("ProductSubCategoryDetail")
	public static class ParseClass extends BaseDetailParseClass<ProductSubCategoryDetail, ProductSubCategory.ParseClass> {

		public ParseClass() {
			super(COL_PRODUCT_SUB_CATEGORY);
		}

		public ParseClass(@NonNull ProductSubCategory.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_PRODUCT_SUB_CATEGORY, referenceObject, languageCode, name);
		}

		protected ProductSubCategory.ParseClass getProductSubCategory() {
			return (ProductSubCategory.ParseClass) getParseObject(COL_PRODUCT_SUB_CATEGORY);
		}

		public void setProductSubCategory(@NonNull ProductSubCategory.ParseClass category) {
			put(COL_PRODUCT_SUB_CATEGORY, category);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return ProductSubCategory.TABLE;
		}

		@NonNull
		@Override
		public ProductSubCategoryDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			ProductSubCategoryDetail entry = new ProductSubCategoryDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
