package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class AddonDetail extends BaseDetailEntry<Addon> {
	private static final String TAG              = AddonDetail.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_ADDON     = COL_ADDON;
	public static final  String SQL_CREATE_TABLE = sqlCreateDetailTable(TABLE, COLUMN_ADDON, Addon.TABLE);
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<AddonDetail> CREATOR = new Creator<AddonDetail>() {
		@Override
		public AddonDetail createFromParcel(Parcel parcel) {
			return new AddonDetail(parcel);
		}

		@Override
		public AddonDetail[] newArray(int i) {
			return new AddonDetail[i];
		}
	};

	public AddonDetail() {}

	protected AddonDetail(Parcel parcel) {
		super(parcel);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_ADDON;
	}

	@ParseClassName("AddonDetail")
	public static class ParseClass extends BaseDetailParseClass<AddonDetail, Addon.ParseClass> {

		public ParseClass() {
			super(COL_ADDON);
		}

		public ParseClass(@NonNull Addon.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_ADDON, referenceObject, languageCode, name);
		}

		@NonNull
		public Addon.ParseClass getAddon() {
			return getReferencedObject();
		}

		public void setAddon(@NonNull Addon.ParseClass addon) {
			setReferencedObject(addon);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return Addon.TABLE;
		}

		@NonNull
		@Override
		public AddonDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			AddonDetail entry = new AddonDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
