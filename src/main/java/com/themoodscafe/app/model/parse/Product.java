package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by ian on 08/05/15.
 */
public class Product extends BaseSortableEntry<Product> {
	private static final String TAG                         = Product.class.getSimpleName();
	public static final  String TABLE                       = TAG;
	public static final  String COLUMN_NAME                 = COL_NAME;
	public static final  String COLUMN_PRODUCT_SUB_CATEGORY = COL_PRODUCT_SUB_CATEGORY;
	public static final  String COLUMN_HAS_INGREDIENTS      = COL_HAS_INGREDIENTS;
	public static final  String COLUMN_IMAGE_FILE_NAME      = COL_IMAGE_FILE_NAME;
	public static final  String COLUMN_IMAGE_FILE_URL       = COL_IMAGE_FILE_URL;
	public static final  String COLUMN_IS_DISABELD          = COL_IS_DISABLED;
	public static final  String SQL_CREATE_TABLE            = sqlCreateSortableTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeReferenceId(COLUMN_PRODUCT_SUB_CATEGORY, ProductSubCategory.TABLE),
			typeBool(COLUMN_HAS_INGREDIENTS),
			typeRemoteFile(COLUMN_IMAGE_FILE_NAME, COLUMN_IMAGE_FILE_URL));
	public static final  String SQL_DELETE_TABLE            = sqlDeleteTable(TABLE);

	public static final Creator<Product> CREATOR = new Creator<Product>() {
		@Override
		public Product createFromParcel(Parcel parcel) {
			return new Product(parcel);
		}

		@Override
		public Product[] newArray(int i) {
			return new Product[i];
		}
	};

	protected String  name;
	protected long    productSubCategoryId;
	protected boolean hasIngredients;
	protected String  imageFileName;
	protected String  iamgeFileUrl;

	public Product() {}

	protected Product(Parcel parcel) {
		super(parcel);
		productSubCategoryId = parcel.readLong();
		hasIngredients = parcel.readInt() != 0;
		imageFileName = parcel.readString();
		iamgeFileUrl = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(productSubCategoryId);
		dest.writeInt(hasIngredients ? 1 : 0);
		dest.writeString(imageFileName);
		dest.writeString(iamgeFileUrl);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getProductSubCategoryId() {
		return productSubCategoryId;
	}

	public void setProductSubCategoryId(long productSubCategoryId) {
		this.productSubCategoryId = productSubCategoryId;
	}

	public boolean isHasIngredients() {
		return hasIngredients;
	}

	public void setHasIngredients(boolean hasIngredients) {
		this.hasIngredients = hasIngredients;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getIamgeFileUrl() {
		return iamgeFileUrl;
	}

	public void setIamgeFileUrl(String iamgeFileUrl) {
		this.iamgeFileUrl = iamgeFileUrl;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		putStringIfChanged(row, COLUMN_NAME, name, cv);
		putLongIfChanged(row, COLUMN_PRODUCT_SUB_CATEGORY, productSubCategoryId, cv);
		putBooleanIfChanged(row, COLUMN_HAS_INGREDIENTS, hasIngredients, cv);
		putStringIfChanged(row, COLUMN_IMAGE_FILE_NAME, imageFileName, cv);
		putStringIfChanged(row, COLUMN_UPDATED_AT, iamgeFileUrl, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_NAME, name);
		cv.put(COLUMN_PRODUCT_SUB_CATEGORY, productSubCategoryId);
		cv.put(COLUMN_HAS_INGREDIENTS, hasIngredients);
		cv.put(COLUMN_IMAGE_FILE_NAME, imageFileName);
		cv.put(COLUMN_UPDATED_AT, iamgeFileUrl);
	}

	@ParseClassName("Product")
	public static class ParseClass extends BaseSortableParseClass<Product, Product.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name,
		                  @NonNull ProductSubCategory.ParseClass productCategory) {
			super(TABLE);
			setName(name);
			setProductSubCategory(productCategory);
		}

		public ParseClass(@NonNull String name,
		                  @NonNull ProductSubCategory.ParseClass productCategory,
		                  int sortIndex) {
			this(name, productCategory);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public ProductSubCategory.ParseClass getProductSubCategory() {
			return (ProductSubCategory.ParseClass) getParseObject(COL_PRODUCT_SUB_CATEGORY);
		}

		public void setProductSubCategory(@NonNull ProductSubCategory.ParseClass category) {
			put(COL_PRODUCT_SUB_CATEGORY, category);
		}

		public void setHasIngredients(boolean hasIngredients) {
			put(COL_HAS_INGREDIENTS, hasIngredients);
		}

		public boolean hasIngredients() {
			return getBoolean(COL_HAS_INGREDIENTS);
		}

		public ParseFile getImage() {
			return getParseFile(COL_IMAGE);
		}

		public void setImage(@NonNull ParseFile image) {
			put(COL_IMAGE, image);
		}

		public boolean isDisabled() {
			return getBoolean(COL_IS_DISABLED);
		}

		@NonNull
		@Override
		public Product newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Product entry = new Product();
			fillSortableEntry(entry);
			entry.setName(getName());
			entry.setProductSubCategoryId(getIdFromObjectId(readableDb, ProductSubCategory.TABLE, getProductSubCategory()));
			entry.setHasIngredients(hasIngredients());
			ParseFile image = getImage();
			if (image != null) {
//			entry.setImageFileName();
				entry.setIamgeFileUrl(image.getUrl());
			}
			return entry;
		}
	}
}