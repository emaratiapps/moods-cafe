package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class IngredientCategoryDetail extends BaseDetailEntry<IngredientCategory> {
	private static final String TAG                        = IngredientCategoryDetail.class.getSimpleName();
	public static final  String TABLE                      = TAG;
	public static final  String COLUMN_INGREDIENT_CATEGORY = COL_INGREDIENT_CATEGORY;
	public static final  String SQL_CREATE_TABLE           = sqlCreateDetailTable(TABLE, COLUMN_INGREDIENT_CATEGORY, IngredientCategory.TABLE);
	public static final  String SQL_DELETE_TABLE           = sqlDeleteTable(TABLE);

	public static final Creator<IngredientCategoryDetail> CREATOR = new Creator<IngredientCategoryDetail>() {
		@Override
		public IngredientCategoryDetail createFromParcel(Parcel parcel) {
			return new IngredientCategoryDetail(parcel);
		}

		@Override
		public IngredientCategoryDetail[] newArray(int i) {
			return new IngredientCategoryDetail[i];
		}
	};

	public IngredientCategoryDetail() {}

	public IngredientCategoryDetail(Parcel parcel) {
		super(parcel);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_INGREDIENT_CATEGORY;
	}

	@ParseClassName("IngredientCategoryDetail")
	public static class ParseClass extends BaseDetailParseClass<IngredientCategoryDetail, IngredientCategory.ParseClass> {

		public ParseClass() {
			super(COL_INGREDIENT_CATEGORY);
		}

		public ParseClass(@NonNull IngredientCategory.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_INGREDIENT_CATEGORY, referenceObject, languageCode, name);
		}

		public IngredientCategory.ParseClass getIngredientCategory() {
			return getReferencedObject();
		}

		public void setIngredientCategory(@NonNull IngredientCategory.ParseClass category) {
			setReferencedObject(category);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return IngredientCategory.TABLE;
		}

		@NonNull
		@Override
		public IngredientCategoryDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			IngredientCategoryDetail entry = new IngredientCategoryDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}

}
