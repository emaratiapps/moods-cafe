package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class IngredientCategory extends BaseSortableEntry<IngredientCategory> {
	private static final String TAG                   = IngredientCategory.class.getSimpleName();
	public static final  String TABLE                 = TAG;
	public static final  String COLUMN_NAME           = COL_NAME;
	public static final  String COLUMN_MIN_PER_RECIPE = COL_MIN_PER_RECIPE;
	public static final  String COLUMN_MAX_PER_RECIPE = COL_MAX_PER_RECIPE;
	public static final  String COLUMN_PRODUCT        = COL_PRODUCT;
	public static final  String SQL_CREATE_TABLE      = sqlCreateSortableTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeInt(COLUMN_MIN_PER_RECIPE),
			typeInt(COLUMN_MAX_PER_RECIPE),
			typeReferenceId(COLUMN_PRODUCT, Product.TABLE));
	public static final  String SQL_DELETE_TABLE      = sqlDeleteTable(TABLE);

	protected String name;
	protected int    minPerRecipe;
	protected int    maxPerRecipe;
	protected long   productId;

	public IngredientCategory() {}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
		dest.writeInt(minPerRecipe);
		dest.writeInt(maxPerRecipe);
		dest.writeLong(productId);
	}

	protected IngredientCategory(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
		minPerRecipe = parcel.readInt();
		maxPerRecipe = parcel.readInt();
		productId = parcel.readLong();
	}

	public static final Creator<IngredientCategory> CREATOR = new Creator<IngredientCategory>() {
		@Override
		public IngredientCategory createFromParcel(Parcel parcel) {
			return new IngredientCategory(parcel);
		}

		@Override
		public IngredientCategory[] newArray(int i) {
			return new IngredientCategory[i];
		}
	};

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxPerRecipe() {
		return maxPerRecipe;
	}

	public void setMaxPerRecipe(int maxPerRecipe) {
		this.maxPerRecipe = maxPerRecipe;
	}

	public int getMinPerRecipe() {
		return minPerRecipe;
	}

	public void setMinPerRecipe(int minPerRecipe) {
		this.minPerRecipe = minPerRecipe;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		String name = getString(row, COLUMN_NAME);
		int minPerRecipe = getInt(row, COLUMN_MIN_PER_RECIPE);
		int maxPerRecipe = getInt(row, COLUMN_MAX_PER_RECIPE);
		long productId = getLong(row, COLUMN_PRODUCT);
		if (name == null || !name.equals(this.name)) {
			cv.put(COLUMN_NAME, this.name);
		}
		if (minPerRecipe != this.minPerRecipe) {
			cv.put(COLUMN_MIN_PER_RECIPE, this.minPerRecipe);
		}
		if (maxPerRecipe != this.maxPerRecipe) {
			cv.put(COLUMN_MAX_PER_RECIPE, this.maxPerRecipe);
		}
		if (productId != this.productId) {
			cv.put(COLUMN_PRODUCT, this.productId);
		}
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_NAME, this.name);
	}

	@ParseClassName("IngredientCategory")
	public static class ParseClass extends BaseSortableParseClass<IngredientCategory, IngredientCategory.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name,
		                  @NonNull Product.ParseClass product,
		                  int minPerRecipe,
		                  int maxPerRecipe) {
			super(TAG);
			setName(name);
			setProduct(product);
			setMinPerRecipe(minPerRecipe);
			setMaxPerRecipe(maxPerRecipe);
		}

		public ParseClass(@NonNull String name,
		                  @NonNull Product.ParseClass product,
		                  int minPerRecipe,
		                  int maxPerRecipe,
		                  int sortIndex) {
			this(name, product, minPerRecipe, maxPerRecipe);
			setSortIndex(sortIndex);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setMaxPerRecipe(int max) {
			put(COL_MAX_PER_RECIPE, max);
		}

		public void setMinPerRecipe(int min) {
			put(COL_MIN_PER_RECIPE, min);
		}

		public int getMinPerRecipe() {
			return getInt(COL_MIN_PER_RECIPE);
		}

		public int getMaxPerRecipe() {
			return getInt(COL_MAX_PER_RECIPE);
		}

		public void setProduct(@NonNull Product.ParseClass product) {
			put(COL_PRODUCT, product);
		}

		@NonNull
		public Product.ParseClass getProduct() {
			return (Product.ParseClass) getParseObject(COL_PRODUCT);
		}

		@NonNull
		@Override
		public IngredientCategory newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			IngredientCategory entry = new IngredientCategory();
			fillSortableEntry(entry);
			entry.setName(getName());
			entry.setMaxPerRecipe(getMaxPerRecipe());
			entry.setProductId(getIdFromObjectId(readableDb, Product.TABLE, getProduct()));
			return entry;
		}
	}
}
