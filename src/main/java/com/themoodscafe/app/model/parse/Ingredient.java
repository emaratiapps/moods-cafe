package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by ian on 08/05/15.
 */
public class Ingredient extends BaseSortableEntry<Ingredient> {
	private static final String TAG                        = Ingredient.class.getSimpleName();
	public static final  String TABLE                      = TAG;
	public static final  String COLUMN_NAME                = COL_NAME;
	public static final  String COLUMN_INGREDIENT_CATEGORY = COL_INGREDIENT_CATEGORY;
	public static final  String COLUMN_IMAGE_FILE_NAME     = COL_IMAGE_FILE_NAME;
	public static final  String COLUMN_IMAGE_FILE_URL      = COL_IMAGE_FILE_URL;
	public static final  String SQL_CREATE_TABLE           = sqlCreateSortableTable(
			TABLE,
			typeText(COLUMN_NAME),
			typeReferenceId(COLUMN_INGREDIENT_CATEGORY, IngredientCategory.TABLE),
			typeRemoteFile(COLUMN_IMAGE_FILE_NAME, COLUMN_IMAGE_FILE_URL));
	public static final  String SQL_DELETE_TABLE           = sqlDeleteTable(TABLE);

	public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
		@Override
		public Ingredient createFromParcel(Parcel parcel) {
			return new Ingredient(parcel);
		}

		@Override
		public Ingredient[] newArray(int i) {
			return new Ingredient[i];
		}
	};

	protected String name;
	protected long ingredientCategoryId;
	protected String imageFileName;
	protected String imageFileUrl;

	public Ingredient() {}

	protected Ingredient(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
		ingredientCategoryId = parcel.readLong();
		imageFileName = parcel.readString();
		imageFileUrl = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
		dest.writeLong(ingredientCategoryId);
		dest.writeString(imageFileName);
		dest.writeString(imageFileUrl);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getIngredientCategoryId() {
		return ingredientCategoryId;
	}

	public void setIngredientCategoryId(long ingredientCategoryId) {
		this.ingredientCategoryId = ingredientCategoryId;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageFileUrl() {
		return imageFileUrl;
	}

	public void setImageFileUrl(String imageFileUrl) {
		this.imageFileUrl = imageFileUrl;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		String name = getString(row, COLUMN_NAME);
		int ingredientCategoryId = getInt(row, COLUMN_INGREDIENT_CATEGORY);
		String imageFileName = getString(row, COLUMN_IMAGE_FILE_NAME);
		String imageFileUrl = getString(row, COLUMN_IMAGE_FILE_URL);
		if (name == null || !name.equals(this.name)) {
			cv.put(COLUMN_NAME, name);
		}
		if (ingredientCategoryId != this.ingredientCategoryId) {
			cv.put(COLUMN_INGREDIENT_CATEGORY, ingredientCategoryId);
		}
//		if () TODO
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
	}

	@ParseClassName("Ingredient")
	public static class ParseClass extends BaseSortableParseClass<Ingredient, Ingredient.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name,
		                  @NonNull IngredientCategory.ParseClass ingredientCategory) {
			super(TABLE);
			setName(name);
			setIngredientCategory(ingredientCategory);
		}

		public ParseClass(@NonNull String name,
		                  @NonNull IngredientCategory.ParseClass ingredientCategory,
		                  int sortIndex) {
			this(name, ingredientCategory);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		protected IngredientCategory.ParseClass getIngredientCategory() {
			return (IngredientCategory.ParseClass) getParseObject(COL_INGREDIENT_CATEGORY);
		}

		public void setIngredientCategory(@NonNull IngredientCategory.ParseClass category) {
			put(COL_INGREDIENT_CATEGORY, category);
		}

		public ParseFile getImage() {
			return getParseFile(COL_IMAGE);
		}

		public void setImage(@NonNull ParseFile image) {
			put(COL_IMAGE, image);
		}

		@NonNull
		@Override
		public Ingredient newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Ingredient entry = new Ingredient();
			fillSortableEntry(entry);
			entry.setName(getName());
			entry.setIngredientCategoryId(getIdFromObjectId(readableDb, IngredientCategory.TABLE, getIngredientCategory()));
			ParseFile image = getImage();
			if (image != null) {
//			entry.setImageFileName();
				entry.setImageFileUrl(image.getUrl());
			}
			return entry;
		}
	}
}
