package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class Variation extends BaseSortableEntry<Variation> {
	private static final String TAG                   = Variation.class.getSimpleName();
	public static final  String TABLE                 = TAG;
	public static final  String COLUMN_NAME           = COL_NAME;
	public static final  String SQL_CREATE_TABLE      = sqlCreateSortableTable(TABLE, typeText(COLUMN_NAME));
	public static final  String SQL_DELETE_TABLE      = sqlDeleteTable(TABLE);

	public static final Creator<Variation> CREATOR = new Creator<Variation>() {
		@Override
		public Variation createFromParcel(Parcel parcel) {
			return new Variation(parcel);
		}

		@Override
		public Variation[] newArray(int i) {
			return new Variation[i];
		}
	};

	private String name;

	public Variation() {}

	protected Variation(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		putStringIfChanged(row, COLUMN_NAME, name, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_NAME, name);
	}

	@ParseClassName("Variation")
	public static class ParseClass extends BaseSortableParseClass<Variation, Variation.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull String name) {
			super(TABLE);
			setName(name);
		}

		public ParseClass(@NonNull String name, int sortIndex) {
			this(name);
			setSortIndex(sortIndex);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		@NonNull
		@Override
		public Variation newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Variation entry = new Variation();
			entry.setName(getName());
			return entry;
		}
	}
}
