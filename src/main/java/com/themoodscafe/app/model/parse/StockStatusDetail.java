package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.parse.ParseClassName;
import com.parse.ParseFile;

/**
 * Created by ian on 08/05/15.
 */
public class StockStatusDetail extends BaseDetailEntry<StockStatus> {
	private static final String TAG                 = StockStatusDetail.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_STOCK_STATUS = COL_STOCK_STATUS;
	public static final  String SQL_CREATE_TABLE    = sqlCreateDetailTable(TABLE, COLUMN_STOCK_STATUS, StockStatus.TABLE);
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public static final Creator<StockStatusDetail> CREATOR = new Creator<StockStatusDetail>() {
		@Override
		public StockStatusDetail createFromParcel(Parcel parcel) {
			return new StockStatusDetail(parcel);
		}

		@Override
		public StockStatusDetail[] newArray(int i) {
			return new StockStatusDetail[i];
		}
	};

	public StockStatusDetail() {}

	protected StockStatusDetail(Parcel parcel) {
		super(parcel);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_STOCK_STATUS;
	}

	@ParseClassName("StockStatusDetail")
	public static class ParseClass extends BaseDetailParseClass<StockStatusDetail, StockStatus.ParseClass> {

		public ParseClass() {
			super(COL_STOCK_STATUS);
		}

		public ParseClass(@NonNull StockStatus.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TABLE, COL_STOCK_STATUS, referenceObject, languageCode, name);
		}

		public StockStatus.ParseClass getStockStatus() {
			return (StockStatus.ParseClass) getReferencedObject();
		}

		public void setStockStatus(@NonNull StockStatus.ParseClass status) {
			setReferencedObject(status);
		}

		public ParseFile getImage() {
			return getParseFile(COL_IMAGE);
		}

		public void setImage(@Nullable ParseFile image) {
			put(COL_IMAGE, image);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return StockStatus.TABLE;
		}

		@NonNull
		@Override
		public StockStatusDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			StockStatusDetail entry = new StockStatusDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
