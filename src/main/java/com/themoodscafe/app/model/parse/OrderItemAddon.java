package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class OrderItemAddon extends BaseEntry {
	private static final String TAG               = OrderItemAddon.class.getSimpleName();
	public static final  String TABLE             = TAG;
	public static final  String COLUMN_ORDER_ITEM = COL_ORDER_ITEM;
	public static final  String COLUMN_ADDON      = COL_ADDON;
	public static final  String COLUMN_QUANTITY   = COL_QUANTITY;
	public static final  String SQL_CREATE_TABLE  = sqlCreateTable(
			TABLE,
			typeReferenceId(COLUMN_ORDER_ITEM, OrderItem.TABLE),
			typeReferenceId(COLUMN_ADDON, Addon.TABLE),
			typeInt(COLUMN_QUANTITY));
	public static final  String SQL_DELETE_TABLE  = sqlDeleteTable(TABLE);

	public static final Creator<OrderItemAddon> CREATOR = new Creator<OrderItemAddon>() {
		@Override
		public OrderItemAddon createFromParcel(Parcel parcel) {
			return new OrderItemAddon(parcel);
		}

		@Override
		public OrderItemAddon[] newArray(int i) {
			return new OrderItemAddon[i];
		}
	};

	protected long orderItemId;
	protected long addonId;
	protected int quantity;

	public OrderItemAddon() {}

	protected OrderItemAddon(Parcel parcel) {
		super(parcel);
		orderItemId = parcel.readLong();
		addonId = parcel.readLong();
		quantity = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(orderItemId);
		dest.writeLong(addonId);
		dest.writeInt(quantity);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public long getAddonId() {
		return addonId;
	}

	public void setAddonId(long addonId) {
		this.addonId = addonId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putLongIfChanged(row, COLUMN_ORDER_ITEM, orderItemId, cv);
		putLongIfChanged(row, COLUMN_ADDON, addonId, cv);
		putIntIfChanged(row, COLUMN_QUANTITY, quantity, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_ORDER_ITEM, orderItemId);
		cv.put(COLUMN_ADDON, addonId);
		cv.put(COLUMN_QUANTITY, quantity);
	}

	@ParseClassName("OrderItemAddon")
	public static class ParseClass extends BaseParseClass<OrderItemAddon> {

		public ParseClass() {}

		public ParseClass(@NonNull OrderItem.ParseClass orderItem,
		                  @NonNull Addon.ParseClass addon,
		                  int quantity) {
			super(TABLE);
			setOrderItem(orderItem);
			setAddon(addon);
			setQuantity(quantity);
		}

		public OrderItem.ParseClass getOrderItem() {
			return (OrderItem.ParseClass) get(COL_ORDER_ITEM);
		}

		public void setOrderItem(@NonNull OrderItem.ParseClass item) {
			put(COL_ORDER_ITEM, item);
		}

		public Addon.ParseClass getAddon() {
			return (Addon.ParseClass) getParseObject(COL_ADDON);
		}

		public void setAddon(@NonNull Addon.ParseClass addon) {
			put(COL_ADDON, addon);
		}

		public int getQuantity() {
			return getInt(COL_QUANTITY);
		}

		public void setQuantity(int quantity) {
			put(COL_QUANTITY, quantity);
		}

		@NonNull
		@Override
		public OrderItemAddon newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			OrderItemAddon entry = new OrderItemAddon();
			fillEntry(entry);
			entry.setOrderItemId(getIdFromObjectId(readableDb, OrderItem.TABLE, getOrderItem()));
			entry.setAddonId(getIdFromObjectId(readableDb, Addon.TABLE, getAddon()));
			entry.setQuantity(getQuantity());
			return entry;
		}
	}
}
