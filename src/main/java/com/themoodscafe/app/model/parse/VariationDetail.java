package com.themoodscafe.app.model.parse;

import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class VariationDetail extends BaseDetailEntry<Variation> {
	private static final String TAG              = VariationDetail.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_VARIATION = COL_VARIATION;
	public static final  String SQL_CREATE_TABLE = sqlCreateDetailTable(TABLE, COLUMN_VARIATION, Variation.TABLE);
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<VariationDetail> CREATOR = new Creator<VariationDetail>() {
		@Override
		public VariationDetail createFromParcel(Parcel parcel) {
			return new VariationDetail(parcel);
		}

		@Override
		public VariationDetail[] newArray(int i) {
			return new VariationDetail[i];
		}
	};

	public VariationDetail() {}

	protected VariationDetail(Parcel parcel) {
		super(parcel);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	@Override
	protected String getReferenceColumn() {
		return COLUMN_VARIATION;
	}

	@ParseClassName("VariationDetail")
	public static class ParseClass extends BaseDetailParseClass<VariationDetail, Variation.ParseClass> {

		public ParseClass() {
			super(COL_VARIATION);
		}

		public ParseClass(@NonNull Variation.ParseClass referenceObject,
		                  @NonNull String languageCode,
		                  @NonNull String name) {
			super(TAG, COL_VARIATION, referenceObject, languageCode, name);
		}

		public Variation.ParseClass getVariation() {
			return (Variation.ParseClass) getReferencedObject();
		}

		public void setVariation(@NonNull Variation.ParseClass variation) {
			setReferencedObject(variation);
		}

		@NonNull
		@Override
		protected String getReferenceTableName() {
			return Variation.TABLE;
		}

		@NonNull
		@Override
		public VariationDetail newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			VariationDetail entry = new VariationDetail();
			fillDetailEntry(readableDb, entry);
			return entry;
		}
	}
}
