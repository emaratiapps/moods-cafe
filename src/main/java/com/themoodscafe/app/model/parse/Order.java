package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class Order extends BaseSortableEntry<Order> {
	private static final String TAG                 = Order.class.getSimpleName();
	public static final  String TABLE               = TAG;
	public static final  String COLUMN_CUSTOMER     = COL_CUSTOMER;
	public static final  String COLUMN_PRICE_TOTAL  = COL_PRICE_TOTAL;
	public static final  String COLUMN_IS_TAKEAWAY  = COL_IS_TAKEAWAY;
	public static final  String COLUMN_NOTE         = COL_NOTE;
	public static final String COLUMN_PHONE_NUMBER = COL_PHONE_NUMBER;
	public static final  String COLUMN_ORDER_STATUS = COL_ORDER_STATUS;
	public static final  String SQL_CREATE_TABLE    = sqlCreateSortableTable(
			TABLE,
			typeReferenceId(COLUMN_CUSTOMER, Customer.TABLE),
			typeFloat(COLUMN_PRICE_TOTAL),
			typeBool(COLUMN_IS_TAKEAWAY),
			typeText(COLUMN_NOTE),
			typeReferenceId(COLUMN_ORDER_STATUS, PhoneNumber.TABLE),
			typeReferenceId(COLUMN_ORDER_STATUS, OrderStatus.TABLE));
	public static final  String SQL_DELETE_TABLE    = sqlDeleteTable(TABLE);

	public static final Creator<Order> CREATOR = new Creator<Order>() {
		@Override
		public Order createFromParcel(Parcel parcel) {
			return new Order(parcel);
		}

		@Override
		public Order[] newArray(int i) {
			return new Order[i];
		}
	};

	protected long    customerId;
	protected float   priceTotal;
	protected boolean isTakeaway;
	protected long    orderStatusId;
	protected String  note;
	protected long phoneNumberId;

	public Order() {}

	protected Order(Parcel parcel) {
		super(parcel);
		customerId = parcel.readInt();
		priceTotal = parcel.readFloat();
		isTakeaway = parcel.readInt() != 0;
		note = parcel.readString();
		phoneNumberId = parcel.readLong();
		orderStatusId = parcel.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeLong(customerId);
		dest.writeFloat(priceTotal);
		dest.writeInt(isTakeaway ? 1 : 0);
		dest.writeString(note);
		dest.writeLong(phoneNumberId);
		dest.writeLong(orderStatusId);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public float getPriceTotal() {
		return priceTotal;
	}

	public void setPriceTotal(float priceTotal) {
		this.priceTotal = priceTotal;
	}

	public boolean isTakeaway() {
		return isTakeaway;
	}

	public void setIsTakeaway(boolean isTakeaway) {
		this.isTakeaway = isTakeaway;
	}

	public long getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(long orderStatusId) {
		this.orderStatusId = orderStatusId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public long getPhoneNumberId() {
		return phoneNumberId;
	}

	public void setPhoneNumberId(long phoneNumberId) {
		this.phoneNumberId = phoneNumberId;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		super.onAllChangedValues(cv, row);
		putLongIfChanged(row, COLUMN_CUSTOMER, customerId, cv);
		putFloatIfChanged(row, COLUMN_PRICE_TOTAL, priceTotal, cv);
		putBooleanIfChanged(row, COLUMN_IS_TAKEAWAY, isTakeaway, cv);
		putStringIfChanged(row, COLUMN_NOTE, note, cv);
		putLongIfChanged(row, COLUMN_PHONE_NUMBER, phoneNumberId, cv);
		putLongIfChanged(row, COLUMN_ORDER_STATUS, orderStatusId, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		super.onAllContentValues(cv);
		cv.put(COLUMN_CUSTOMER, this.customerId);
		cv.put(COLUMN_PRICE_TOTAL, this.priceTotal);
		cv.put(COLUMN_IS_TAKEAWAY, this.isTakeaway);
		cv.put(COLUMN_NOTE, this.note);
		cv.put(COLUMN_PHONE_NUMBER, this.phoneNumberId);
		cv.put(COLUMN_ORDER_STATUS, this.orderStatusId);
	}

	@ParseClassName("Order")
	public static class ParseClass extends BaseSortableParseClass<Order, Order.ParseClass> {

		public ParseClass() {}

		public ParseClass(@NonNull Customer.ParseClass customer,
		                  @NonNull OrderStatus.ParseClass orderStatus,
		                  float priceTotal) {
			super(TAG);
			setCustomer(customer);
			setOrderStatus(orderStatus);
			setPriceTotal(priceTotal);
		}

		public Customer.ParseClass getCustomer() {
			return (Customer.ParseClass) getParseObject(COL_CUSTOMER);
		}

		public void setCustomer(@NonNull Customer.ParseClass customer) {
			put(COL_CUSTOMER, customer);
		}

		public OrderStatus.ParseClass getOrderStatus() {
			return (OrderStatus.ParseClass) get(COL_ORDER_STATUS);
		}

		public void setOrderStatus(@NonNull OrderStatus.ParseClass status) {
			put(COL_ORDER_STATUS, status);
		}

		public float getPriceTotal() {
			Number number = getNumber(COL_PRICE_TOTAL);
			return number == null ? 0.0f : number.floatValue();
		}

		public void setPriceTotal(float price) {
			put(COL_PRICE_TOTAL, price);
		}

		public void setIsTakeAway(boolean isTakeAway) {
			put(COL_IS_TAKEAWAY, isTakeAway);
		}

		public boolean isTakeaway() {
			return getBoolean(COL_IS_TAKEAWAY);
		}

		public String getNote() {
			return getString(COL_NOTE);
		}

		public void setNote(String note) {
			put(COL_NOTE, note);
		}

		public PhoneNumber.ParseClass getPhoneNumber() {
			return (PhoneNumber.ParseClass) get(COL_PHONE_NUMBER);
		}

		public void setPhoneNumber(@NonNull PhoneNumber.ParseClass phoneNumber) {
			put(COL_PHONE_NUMBER, phoneNumber);
		}

		@NonNull
		@Override
		public Order newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			Order entry = new Order();
			fillSortableEntry(entry);
			entry.setCustomerId(getIdFromObjectId(readableDb, Customer.TABLE, getCustomer()));
			entry.setOrderStatusId(getIdFromObjectId(readableDb, OrderStatus.TABLE, getOrderStatus()));
			entry.setPriceTotal(getPriceTotal());
			entry.setIsTakeaway(isTakeaway());
			entry.setNote(getNote());
			entry.setPhoneNumberId(getIdFromObjectId(readableDb, PhoneNumber.TABLE, getPhoneNumber()));
			return entry;
		}
	}
}