package com.themoodscafe.app.model.parse;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.annotation.NonNull;
import com.parse.ParseClassName;

/**
 * Created by ian on 08/05/15.
 */
public class StockStatus extends BaseEntry {
	private static final String TAG              = StockStatus.class.getSimpleName();
	public static final  String TABLE            = TAG;
	public static final  String COLUMN_NAME      = COL_NAME;
	public static final  String SQL_CREATE_TABLE = sqlCreateTable(TABLE, typeText(COLUMN_NAME));
	public static final  String SQL_DELETE_TABLE = sqlDeleteTable(TABLE);

	public static final Creator<StockStatus> CREATOR = new Creator<StockStatus>() {
		@Override
		public StockStatus createFromParcel(Parcel parcel) {
			return new StockStatus(parcel);
		}

		@Override
		public StockStatus[] newArray(int i) {
			return new StockStatus[i];
		}
	};

	protected String name;

	public StockStatus() {}

	protected StockStatus(Parcel parcel) {
		super(parcel);
		name = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeString(name);
	}

	@NonNull
	@Override
	public String getTableName() {
		return TABLE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected void onAllChangedValues(@NonNull ContentValues cv, @NonNull Cursor row) {
		putStringIfChanged(row, COLUMN_NAME, name, cv);
	}

	@Override
	protected void onAllContentValues(@NonNull ContentValues cv) {
		cv.put(COLUMN_NAME, this.name);
	}

	@ParseClassName("StockStatus")
	public static class ParseClass extends BaseParseClass<StockStatus> {
		public ParseClass() {}

		public ParseClass(@NonNull String name) {
			super(TABLE);
			setName(name);
		}

		public String getName() {
			return getString(COL_NAME);
		}

		public void setName(@NonNull String name) {
			put(COL_NAME, name);
		}

		public void setIsDefault(boolean isDefault) {
			put(COL_IS_DEFAULT, isDefault);
		}

		public boolean isDefault() {
			return getBoolean(COL_IS_DEFAULT);
		}

		@NonNull
		@Override
		public StockStatus newDatabaseEntryInstance(@NonNull SQLiteDatabase readableDb) {
			StockStatus entry = new StockStatus();
			fillEntry(entry);
			entry.setName(getName());
			return entry;
		}
	}
}
