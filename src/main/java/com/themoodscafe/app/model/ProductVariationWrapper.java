package com.themoodscafe.app.model;

import com.themoodscafe.app.model.parse.ProductVariation;
import com.themoodscafe.app.model.parse.ProductVariation.ParseClass;

/**
 * Created by ian on 11/05/15.
 */
public class ProductVariationWrapper {
	private ProductVariation.ParseClass productVariation;
	private int                         quantity;

	public ProductVariationWrapper(ParseClass productVariation, int quantity) {
		this.productVariation = productVariation;
		this.quantity = quantity;
	}

	public ParseClass getProductVariation() {
		return productVariation;
	}

	public void setProductVariation(ParseClass productVariation) {
		this.productVariation = productVariation;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int increaseQuantity(int increaseBy) {
		this.quantity += increaseBy;
		return this.quantity;
	}

	public int decreaseQuantity(int decreaseBy) {
		this.quantity = Math.max(0, this.quantity - decreaseBy);
		return this.quantity;
	}
}
