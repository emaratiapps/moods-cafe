package com.themoodscafe.app.manager.view.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.parse.*;
import com.themoodscafe.app.R;
import com.themoodscafe.app.model.ProductVariationWrapper;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.model.parse.Order.ParseClass;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQuery.FetchFrom;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.util.EmailUriBuilder;
import com.themoodscafe.app.util.NetUtil;
import com.themoodscafe.app.util.ParseUpdater;
import com.themoodscafe.app.util.ParseUpdater.InitializeListener;
import com.themoodscafe.app.util.Prefers;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ManagerActivity extends FragmentActivity implements InitializeListener,
		OnMenuItemClickListener,
		OnClickListener
{

	private static final String TAG                             = ManagerActivity.class.getSimpleName();
	private static final String ARG_FIRST_UPDATE                = TAG + ".FirstUpdateArg";
	private static final String ARG_POSITION                    = TAG + ".PositionArg";
	private static final String ARG_CONFIG_AUTO_UPDATE          = TAG + ".ConfigAutoUpdateArg";
	private static final String ARG_CONFIG_AUTO_TRIGGER         = TAG + ".ConfigAutoTriggerArg";
	private static final String ARG_CONFIG_CONFIRM_ACTION       = TAG + ".ConfigConfirmActionArg";
	private static final String ARG_CONFIG_AUTO_UPDATE_INTERVAL = TAG + ".ConfigAutoUpdateIntervalArg";

	private ViewPager         viewPager;
	private OrderPagerAdapter pagerAdapter;
	private ProgressDialog    progressDialog;
	private OrderListAdapter  orderListAdapter;
	private DrawerLayout      drawerLayout;
	private ListView          orderListView;
	private TextView          txtAutoUpdate;

	private int     position                 = 0;
	private boolean doubleBackpress          = false;
	private boolean configAutoUpdate         = true;
	private boolean configAutoTrigger        = true;
	private boolean configConfirmAction      = true;
	private int     configAutoUpdateInterval = 10;
	private Handler autoUpdateHandler;
	private boolean visibleUpdate = true;

	private static void onAnyError(@NonNull String message) {
		Log.e(TAG, message);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manager);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		if (savedInstanceState != null) {
			position = savedInstanceState.getInt(ARG_POSITION, position);
			configAutoUpdate = savedInstanceState.getBoolean(ARG_CONFIG_AUTO_UPDATE, configAutoUpdate);
			configAutoTrigger = savedInstanceState.getBoolean(ARG_CONFIG_AUTO_TRIGGER, configAutoTrigger);
			configConfirmAction = savedInstanceState.getBoolean(ARG_CONFIG_CONFIRM_ACTION, configConfirmAction);
			configAutoUpdateInterval = savedInstanceState.getInt(ARG_CONFIG_AUTO_UPDATE_INTERVAL, configAutoUpdateInterval);
		}

//		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//		toolbar.setOnMenuItemClickListener(this);

		pagerAdapter = new OrderPagerAdapter(this, getSupportFragmentManager());
		pagerAdapter.setConfigConfirmActions(configConfirmAction);

		viewPager = (ViewPager) findViewById(R.id.view_pager);
		viewPager.setAdapter(pagerAdapter);

		orderListAdapter = new OrderListAdapter();
		orderListView = (ListView) findViewById(R.id.order_list_view);
		orderListView.setAdapter(orderListAdapter);
		orderListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				viewPager.setCurrentItem(position);
				drawerLayout.closeDrawers();
			}
		});
		orderListView.setEmptyView(findViewById(R.id.txt_no_orders));

		pagerAdapter.setListener(new OrderDataListener() {
			@Override
			public void onOrderDataChanged(List<ParseClass> orders) {
				orderListAdapter.setOrders(orders);

				if (orders != null) {
					if (orders.size() > 0) {
						position = (position + 2) % orders.size();
						viewPager.setCurrentItem(position);
					} else {
						visibleUpdate = true;
						viewPager.setCurrentItem(0);
						if (configAutoTrigger) {
							if (!configAutoUpdate) {
								configAutoUpdate = true;
								txtAutoUpdate.setText("Automatic update turned ON");
							}

							initializeAutoUpdate();
						}
					}
				}
			}

		});

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		findViewById(R.id.ibtn_list).setOnClickListener(this);
		findViewById(R.id.ibtn_refresh).setOnClickListener(this);
		findViewById(R.id.ibtn_settings).setOnClickListener(this);

		txtAutoUpdate = (TextView) findViewById(R.id.txt_auto_update);

		ParseUser.logInInBackground("manager", "Manager1", new LogInCallback() {
			@Override
			public void done(ParseUser parseUser, ParseException e) {
				if (e != null) {
					onAnyError("Signin error:" + e.getMessage());
					return;
				}
				initialize();
			}
		});
	}

	private void showSettings() {

		ViewGroup root = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);
		View view = LayoutInflater.from(this).inflate(R.layout.settings_view, root, false);

		final DialogViewHolder vh = new DialogViewHolder();
		vh.tglAutoUpdate = (ToggleButton) view.findViewById(R.id.tgl_auto_update);
		vh.tglAutoUpdate.setChecked(configAutoUpdate);
		vh.tglAutoTrigger = (ToggleButton) view.findViewById(R.id.tgl_auto_trigger);
		vh.tglAutoTrigger.setChecked(configAutoTrigger);
		vh.tglConfirmActions = (ToggleButton) view.findViewById(R.id.tgl_confirm_actions);
		vh.tglConfirmActions.setChecked(configConfirmAction);
		vh.edtUpdateInterval = (EditText) view.findViewById(R.id.edt_auto_update_interval);
		vh.edtUpdateInterval.setText(String.valueOf(configAutoUpdateInterval));

		final boolean previousAutoUpdateConfig = configAutoUpdate;
		configAutoUpdate = false;
		if (autoUpdateHandler != null) {
			autoUpdateHandler.removeCallbacksAndMessages(null);
		}

		new AlertDialog.Builder(this)
				.setView(view)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						configAutoUpdate = vh.tglAutoUpdate.isChecked();
						configAutoTrigger = vh.tglAutoTrigger.isChecked();
						configConfirmAction = vh.tglConfirmActions.isChecked();
						configAutoUpdateInterval = Integer.parseInt(vh.edtUpdateInterval.getText().toString());
						pagerAdapter.setConfigConfirmActions(configConfirmAction);
						if (configAutoUpdate) {
							initializeAutoUpdate();
						}
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						configAutoUpdate = previousAutoUpdateConfig;
						if (configAutoUpdate) {
							initializeAutoUpdate();
						}
					}
				})
				.show();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_POSITION, position);
		outState.putBoolean(ARG_CONFIG_AUTO_UPDATE, configAutoUpdate);
		outState.putBoolean(ARG_CONFIG_AUTO_TRIGGER, configAutoTrigger);
		outState.putBoolean(ARG_CONFIG_CONFIRM_ACTION, configConfirmAction);
		outState.putInt(ARG_CONFIG_AUTO_UPDATE_INTERVAL, configAutoUpdateInterval);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ibtn_list:
			drawerLayout.openDrawer(Gravity.LEFT);
			break;

		case R.id.ibtn_refresh:
			onRefresh();
			break;

		case R.id.ibtn_settings:
			showSettings();
			break;
		}
	}

	private void firstUpdate() {
		if (NetUtil.isOnline()) {
			ParseUpdater.initialize(this);
		} else {
			new AlertDialog.Builder(this)
					.setMessage("Your menu is empty. Connect your WiFi to get content")
					.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							firstUpdate();
						}
					})
					.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					})
					.show();
		}
	}

	private void backgroundUpdate() {
		if (NetUtil.isOnline()) {
			ParseUpdater.initialize(this);
		}
		queryOrderStatus();
	}

	private void onRefresh() {
		if (autoUpdateHandler != null) {
			autoUpdateHandler.removeCallbacksAndMessages(null);
		}
		txtAutoUpdate.setText("");
		queryOrderStatus();
	}

	private void initialize() {
//		if (Prefers.with(this).getBoolean(ARG_FIRST_UPDATE, true)) {
		firstUpdate();
//		} else {
//			backgroundUpdate();
//		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		return false;
	}

	private void showProgress() {
		if (progressDialog == null) {
			progressDialog = new ProgressDialog(this);
			progressDialog.setCancelable(false);
			progressDialog.setProgressStyle(android.R.attr.progressBarStyleHorizontal);
			progressDialog.setMessage("Updating...");
		}
		if (visibleUpdate) {
			progressDialog.setProgress(0);
			progressDialog.show();
		}
	}

	private void setProgressDialog(int progress) {
		if (progressDialog != null) {
			progressDialog.setProgress(progress);
		}
	}

	private void hideProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	@Override
	public void onUpdateStart(@NonNull Class<?> theClass) {
		showProgress();
	}

	@Override
	public void onUpdateComplete(@NonNull Class<?> theClass) {}

	@Override
	public void onError(@NonNull Class<?> theClass, @NonNull String reason) {}

	@Override
	public void onProgress(int progress) {
		setProgressDialog(position);
	}

	@Override
	public void onAllComplete() {
		hideProgressDialog();
		Prefers.with(this).putBoolean(ARG_FIRST_UPDATE, false);
		queryOrderStatus();
	}

	private void queryOrderStatus() {
		showProgress();
		DataStoreQuery.query(OrderStatus.ParseClass.class, new DataStoreQueryListener<OrderStatus.ParseClass>() {
			@Override
			public void onQuery(@NonNull ParseQuery<OrderStatus.ParseClass> query) {
				query.whereEqualTo(OrderStatus.COLUMN_SEQUENCE, 1);
			}

			@Override
			public void onSuccess(@NonNull List<OrderStatus.ParseClass> results) {
				if (results.size() < 1) {
					return;
				}

				final OrderStatus.ParseClass firstState = results.get(0);
				queryOrders(firstState);
			}

			@Override
			public void onError(@NonNull String message) {
				onAnyError(message);
			}
		});
	}

	private void initializeAutoUpdate() {
		if (autoUpdateHandler == null) {
			autoUpdateHandler = new Handler();
		}
		autoUpdateHandler.removeCallbacksAndMessages(null);
//		txtAutoUpdate.setText("Auto update initiated");
		autoUpdateHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				onRefresh();
			}
		}, configAutoUpdateInterval * 1000);
	}

	private void queryOrders(final OrderStatus.ParseClass firstState) {
		new DataStoreQuery<>(Order.ParseClass.class)
				.setFetchFrom(FetchFrom.REMOTE_STORAGE)
				.setListener(new DataStoreQueryListener<ParseClass>() {
					@Override
					public void onQuery(@NonNull ParseQuery<Order.ParseClass> query) {
						query.whereEqualTo(Order.COLUMN_ORDER_STATUS, firstState);
						query.setLimit(1000);
					}

					@Override
					public void onSuccess(@NonNull List<Order.ParseClass> results) {
						pagerAdapter.setOrders(results);
						if (results.size() > 0) {
							txtAutoUpdate.setText("");
							visibleUpdate = false;
						}
						onPostExecute();
					}

					@Override
					public void onError(@NonNull String message) {
						onAnyError(message);
						onPostExecute();
					}

					private void onPostExecute() {
						hideProgressDialog();

						if (configAutoUpdate) {
							txtAutoUpdate.setText("Updating after " + configAutoUpdateInterval + " seconds");
							initializeAutoUpdate();
						}
					}
				})
				.execute();
	}

	@Override
	public void onBackPressed() {
		if (doubleBackpress) {
			hideProgressDialog();
			finish();
		} else if (progressDialog != null && progressDialog.isShowing()) {
			Toast.makeText(this, "Currently updating.\nPress back again to force exit", Toast.LENGTH_LONG).show();
			doubleBackpress = true;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackpress = false;
				}
			}, Toast.LENGTH_LONG);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void finish() {
		if (autoUpdateHandler != null) {
			autoUpdateHandler.removeCallbacksAndMessages(null);
		}
		super.finish();
	}

	public interface OrderDataListener {
		void onOrderDataChanged(List<Order.ParseClass> orders);
	}

	private interface OnActionListener {
		void onCancelled(int position);

		void onComplete(int position);
	}

	private static class DialogViewHolder {
		ToggleButton tglAutoUpdate, tglAutoTrigger, tglConfirmActions;
		EditText edtUpdateInterval;
	}

	private static class ViewHolder {
		TextView txtState, txtDateSent, txtPhoneNumber;
	}

	private static class OrderListAdapter extends BaseAdapter {
		private List<Order.ParseClass> orders;

		public void setOrders(List<ParseClass> orders) {
			this.orders = orders;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return orders != null ? orders.size() : 0;
		}

		@Override
		public Order.ParseClass getItem(int position) {
			return orders != null ? orders.get(position) : null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView != null ? convertView : inflate(parent);
			Object tag = view.getTag(R.id.tag_view_holder);
			if (tag instanceof ViewHolder) {
				populate((ViewHolder) tag, position);
			}
			return view;
		}

		private View inflate(ViewGroup parent) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_list_item, parent, false);
			ViewHolder vh = new ViewHolder();
			vh.txtState = (TextView) view.findViewById(R.id.txt_state);
			vh.txtDateSent = (TextView) view.findViewById(R.id.txt_date_sent);
			vh.txtPhoneNumber = (TextView) view.findViewById(R.id.txt_phone_number);
			view.setTag(R.id.tag_view_holder, vh);
			return view;
		}

		private void populate(final ViewHolder vh, int position) {
			Order.ParseClass order = getItem(position);
			final PhoneNumber.ParseClass phone = order.getPhoneNumber();
			phone.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
				@Override
				public void done(ParseObject parseObject, ParseException e) {
					if (e == null) {
						vh.txtPhoneNumber.setText("tel: " + phone.getPhoneNumber());
					}
				}
			});
			String label = String.valueOf(position + 1) + ". ";
			OrderStatus.ParseClass status = order.getOrderStatus();
			if (status != null) {
				label += status.getName();
			}
			vh.txtState.setText(label);
			vh.txtDateSent.setText(OrderFragment.DATE_FORMATTER.format(order.getUpdatedAt()));
		}
	}

	public static class OrderPagerAdapter extends FragmentStatePagerAdapter implements OnActionListener {
		private final Context           context;
		private List<Order.ParseClass> orders = new ArrayList<>();
		private       OrderDataListener listener;
		private boolean configConfirmActions = true;

		public OrderPagerAdapter(Context context, FragmentManager fm) {
			super(fm);
			this.context = context;
		}

		public void setConfigConfirmActions(boolean configConfirmActions) {
			this.configConfirmActions = configConfirmActions;
		}

		public void setListener(OrderDataListener listener) {
			this.listener = listener;
		}

		public Order.ParseClass remove(int position) {
			if (position >= 0 && position < orders.size()) {
				Order.ParseClass order = orders.remove(position);
				notifyDataSetChanged();

				if (listener != null) {
					listener.onOrderDataChanged(orders);
				}
				return order;
			}
			return null;
		}

		private void setState(final Order.ParseClass order, final int sequence) {
			if (order == null) return;

			DataStoreQuery.query(OrderStatus.ParseClass.class, new DataStoreQueryListener<OrderStatus.ParseClass>() {
				@Override
				public void onQuery(@NonNull ParseQuery<OrderStatus.ParseClass> query) {
					query.whereEqualTo(OrderStatus.COLUMN_SEQUENCE, sequence);
				}

				@Override
				public void onSuccess(@NonNull List<OrderStatus.ParseClass> results) {
					if (results.size() > 0) {
						order.setOrderStatus(results.get(0));
					} else {
						order.put(Order.COLUMN_ORDER_STATUS, null);
					}
					order.saveInBackground(new SaveCallback() {
						@Override
						public void done(ParseException e) {
							order.unpinInBackground();
						}
					});
				}

				@Override
				public void onError(@NonNull String message) {}
			});

		}

		private void cancelOrder(int position) {
			setState(remove(position), 3);
		}

		private void completeOrder(int position) {
			setState(remove(position), 4);
		}

		@Override
		public void onCancelled(final int position) {
			if (configConfirmActions) {
				new AlertDialog.Builder(context)
						.setMessage("Is this order finished? It will be removed permanently")
						.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								cancelOrder(position);
							}
						})
						.setNegativeButton("Keep", null)
						.show();
			} else {
				cancelOrder(position);
			}
		}

		@Override
		public void onComplete(final int position) {
			if (configConfirmActions) {
				new AlertDialog.Builder(context)
						.setMessage("Cancel this order? It will be removed permanently")
						.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								completeOrder(position);
							}
						})
						.setNegativeButton("Keep", null)
						.show();
			} else {
				completeOrder(position);
			}
		}

		@Override
		public int getItemPosition(Object object) {
			if (object instanceof OrderFragment) {
				OrderFragment frag = (OrderFragment) object;
				String orderObjectId = frag.getOrderObjectId();
				if (orderObjectId != null && orders.size() > 0) {
					for (Order.ParseClass order : orders) {
						if (orderObjectId.equals(order.getObjectId())) {
							return orders.indexOf(order);
						}
					}
				}
			}
			return POSITION_NONE;
		}

		public void setOrders(List<ParseClass> orders) {
			this.orders.clear();
			this.orders.addAll(orders);
			notifyDataSetChanged();
			if (listener != null) {
				listener.onOrderDataChanged(orders);
			}
		}

		@Override
		public Fragment getItem(int position) {
			return OrderFragment.newInstance(orders.get(position), position);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			Object object = super.instantiateItem(container, position);
			if (object instanceof OrderFragment) {
				OrderFragment orderFragment = (OrderFragment) object;
				orderFragment.setListener(this);
			}
			return object;
		}

		@Override
		public int getCount() {
			return orders.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return String.format("%d / %d", position + 1, getCount());
		}
	}

	public static class OrderFragment extends Fragment implements OnClickListener {
		public static final  SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss", Locale.ENGLISH);
		private static final String           TAG            = OrderFragment.class.getSimpleName();
		private static final String           ARG_OBJECT_ID  = TAG + ".ObjectIdArg";
		private static final String           ARG_POSITION   = TAG + ".PositionArg";
		private String   orderObjectId;
		private TextView txtOrderStatus, txtDateModified, txtDateAdded, txtTotal;
		private Button btnPhoneNumber, btnEmail, btnNote, btnCancel, btnComplete;
		private CheckBox          chkTakeaway;
		private ListView          orderItemListView;
		private String            customerNote;
		private SalesOrderAdapter salesOrderAdapter;
		private int position = -1;

		private WeakReference<OnActionListener> listenerReference;
		private Order.ParseClass                order;

		public OrderFragment() {}

		public static OrderFragment newInstance(@NonNull Order.ParseClass parseClass, int position) {
			Bundle args = new Bundle();
			args.putString(ARG_OBJECT_ID, parseClass.getObjectId());
			args.putInt(ARG_POSITION, position);
			OrderFragment fragment = new OrderFragment();
			fragment.setArguments(args);
			return fragment;
		}

		public OnActionListener getListener() {
			return listenerReference != null ? listenerReference.get() : null;
		}

		public void setListener(OnActionListener listener) {
			this.listenerReference = new WeakReference<>(listener);
		}

		public int getPosition() {
			return position;
		}

		@Override
		public void onSaveInstanceState(Bundle outState) {
			super.onSaveInstanceState(outState);
			outState.putString(ARG_OBJECT_ID, orderObjectId);
			outState.putInt(ARG_POSITION, position);
		}

		public String getOrderObjectId() {
			return orderObjectId;
		}

		@Nullable
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View root = inflater.inflate(R.layout.order_detail, container, false);

			txtOrderStatus = (TextView) root.findViewById(R.id.txt_order_status);
			txtDateModified = (TextView) root.findViewById(R.id.txt_date_modified);
			txtDateAdded = (TextView) root.findViewById(R.id.txt_date_sent);
			txtTotal = (TextView) root.findViewById(R.id.txt_total);

			btnPhoneNumber = (Button) root.findViewById(R.id.btn_phone_number);
			btnEmail = (Button) root.findViewById(R.id.btn_email);
			btnNote = (Button) root.findViewById(R.id.btn_note);
			btnCancel = (Button) root.findViewById(R.id.btn_cancel);
			btnComplete = (Button) root.findViewById(R.id.btn_complete);
			chkTakeaway = (CheckBox) root.findViewById(R.id.chk_takeaway);

			btnEmail.setOnClickListener(this);
			btnPhoneNumber.setOnClickListener(this);
			btnNote.setOnClickListener(this);
			btnCancel.setOnClickListener(this);
			btnComplete.setOnClickListener(this);

			btnEmail.setEnabled(false);
			btnEmail.setVisibility(View.GONE);
			btnPhoneNumber.setEnabled(false);
			btnNote.setEnabled(false);
			btnNote.setVisibility(View.GONE);
			chkTakeaway.setVisibility(View.GONE);

			salesOrderAdapter = new SalesOrderAdapter();
			orderItemListView = (ListView) root.findViewById(R.id.order_list_view);
			orderItemListView.setAdapter(salesOrderAdapter);

			Bundle args = savedInstanceState != null ? savedInstanceState : getArguments();
			if (args != null) {
				String id = args.getString(ARG_OBJECT_ID);
				if (id != null && !"".equals(id)) {
					orderObjectId = id;
				}

				position = args.getInt(ARG_POSITION, position);
			}

			if (orderObjectId != null) {
				populate(orderObjectId);
			}

			return root;

		}

		private void populate(@NonNull final String orderObjectId) {
			DataStoreQuery.query(Order.ParseClass.class, new DataStoreQueryListener<ParseClass>() {
				@Override
				public void onQuery(@NonNull ParseQuery<ParseClass> query) {
					query.whereEqualTo(BaseEntry.COLUMN_OBJECT_ID, orderObjectId);
				}

				@Override
				public void onSuccess(@NonNull List<Order.ParseClass> results) {
					if (results.size() > 0) {
						order = results.get(0);

						txtDateAdded.setText(DATE_FORMATTER.format(order.getCreatedAt()));
						txtOrderStatus.setText("Moved to \'" + order.getOrderStatus().getName() + "\' on:");
						txtDateModified.setText(DATE_FORMATTER.format(order.getUpdatedAt()));

						String note = order.getNote();
						if (note != null && !"".equals(note)) {
							customerNote = note;
							btnNote.setVisibility(View.VISIBLE);
							btnNote.setEnabled(true);
						}

						final Customer.ParseClass customer = order.getCustomer();
						if (customer != null) {
							customer.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
								@Override
								public void done(ParseObject parseObject, ParseException e) {
									if (e == null) {
										String email = customer.getEmail();
										if (!"".equals(email)) {
											btnEmail.setVisibility(View.VISIBLE);
											btnEmail.setText(email);
										}
									}
								}
							});
						}

						final PhoneNumber.ParseClass phone = order.getPhoneNumber();
						if (phone != null) {
							phone.fetchIfNeededInBackground(new GetCallback<ParseObject>() {
								@Override
								public void done(ParseObject parseObject, ParseException e) {
									if (e == null) {
										btnPhoneNumber.setText(phone.getPhoneNumber());
										btnPhoneNumber.setEnabled(true);
									}
								}
							});
						}

						if (order.isTakeaway()) {
							chkTakeaway.setChecked(true);
							chkTakeaway.setVisibility(View.VISIBLE);
						}

						new DataStoreQuery<>(OrderItem.ParseClass.class)
								.setFetchFrom(FetchFrom.REMOTE_STORAGE)
								.setListener(new DataStoreQueryListener<OrderItem.ParseClass>() {

									@Override
									public void onQuery(@NonNull ParseQuery<OrderItem.ParseClass> query) {
										query.whereEqualTo(OrderItem.COLUMN_ORDER, order);
									}

									@Override
									public void onSuccess(@NonNull List<OrderItem.ParseClass> results) {
										for (OrderItem.ParseClass item : results) {
											salesOrderAdapter.add(new ProductVariationWrapper(item.getProductVariation(), item.getQuantity()));
										}

										txtTotal.setText(salesOrderAdapter.getNetTotalString());
									}

									@Override
									public void onError(@NonNull String message) {
										onAnyError(message);
									}
								})
								.execute();
					}
				}

				@Override
				public void onError(@NonNull String message) {
					onAnyError(message);
				}
			});
		}

		private void onEmail() {
			new EmailUriBuilder(getActivity())
					.addRecipient(btnEmail.getText().toString())
					.send();
		}

		private void onPhoneNumber() {
			String phoneNumber = btnPhoneNumber.getText().toString();


			Uri uri = Uri.parse("tel:" + phoneNumber);
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(uri);
			try {
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Toast.makeText(getActivity(), "Phone app not found", Toast.LENGTH_LONG).show();
			}
		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_phone_number:
				onPhoneNumber();
				break;
			case R.id.btn_email:
				onEmail();
				break;
			case R.id.btn_note:
				new AlertDialog.Builder(getActivity())
						.setTitle("Order notes")
						.setMessage(customerNote)
						.setCancelable(true)
						.setPositiveButton(android.R.string.ok, null)
						.show();
				break;
			case R.id.btn_cancel:
				onCancel();
				break;

			case R.id.btn_complete:
				onComplete();
				break;
			}
		}

		private void onCancel() {
			OnActionListener listener = getListener();
			if (listener != null) {
				listener.onCancelled(position);
			}
		}

		private void onComplete() {
			OnActionListener listener = getListener();
			if (listener != null) {
				listener.onComplete(position);
			}
		}
	}

}
