package com.themoodscafe.app.manager.view.activity;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.themoodscafe.app.R;
import com.themoodscafe.app.model.ProductVariationWrapper;
import com.themoodscafe.app.model.parse.ProductVariation;
import com.themoodscafe.app.model.parse.Variation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 11/05/15.
 */
public class SalesOrderAdapter extends BaseAdapter {

	private static final DecimalFormat                 DECIMAL_FORMAT = new DecimalFormat("#.#");
	private final        List<ProductVariationWrapper> list           = new ArrayList<>();

	public List<ProductVariationWrapper> getList() {
		return list;
	}

	public void add(ProductVariationWrapper item) {
		list.add(item);
		notifyDataSetChanged();
	}

	public int add(ProductVariation.ParseClass pv) {
		String objectId = pv.getObjectId();
		for (ProductVariationWrapper pvw : list) {
			if (objectId.equals(pvw.getProductVariation().getObjectId())) {
				int quantity = pvw.increaseQuantity(1);
				notifyDataSetChanged();
				return quantity;
			}
		}

		add(new ProductVariationWrapper(pv, 1));
		return 1;
	}

	public int increaseItem(int position, int increaseBy) {
		ProductVariationWrapper pvw = list.get(position);
		int quantity = pvw.increaseQuantity(increaseBy);
		notifyDataSetChanged();
		return quantity;
	}

	public int decreaseItem(int position, int decreaseBy) {
		ProductVariationWrapper pvw = list.get(position);
		int quantity = pvw.decreaseQuantity(decreaseBy);
		if (quantity <= 0) {
			list.remove(position);
		}
		notifyDataSetChanged();
		return quantity;
	}

	public float getNetTotal() {
		float total = 0f;
		for (ProductVariationWrapper item : list) {
			total += item.getProductVariation().getPrice() * item.getQuantity();
		}
		return total;
	}

	public String getNetTotalString() {
		return DECIMAL_FORMAT.format(getNetTotal());
	}

	public int getOrderCount() {
		int count = 0;
		for (ProductVariationWrapper pvw : list) {
			count += pvw.getQuantity();
		}
		return count;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public ProductVariationWrapper getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View root = convertView != null ? convertView : inflate(parent, R.layout.order_item_list_item);
		Object tag = root.getTag(R.id.tag_view_holder);
		if (tag instanceof ViewHolder) {
			populate((ViewHolder) tag, position);
		}
		return root;
	}

	@NonNull
	public View inflate(ViewGroup parent, int layoutResource) {
		View view = LayoutInflater.from(parent.getContext()).inflate(layoutResource, parent, false);
		ViewHolder vh = new ViewHolder();
		vh.txtItemName = (TextView) view.findViewById(R.id.txt_item_name);
		vh.txtUnitPrice = (TextView) view.findViewById(R.id.txt_unit_price);
		vh.txtMultiplier = (TextView) view.findViewById(R.id.txt_multiplier);
		vh.txtTotalPrice = (TextView) view.findViewById(R.id.txt_total_price);
//		FontUtil.setBvoliTypeface(vh.txtItemName);
//		FontUtil.setBvoliTypeface(vh.txtUnitPrice);
//		FontUtil.setBvoliTypeface(vh.txtMultiplier);
//		FontUtil.setBvoliTypeface(vh.txtTotalPrice);
		view.setTag(R.id.tag_view_holder, vh);
		return view;
	}

	public void populate(@NonNull ViewHolder vh, int position) {
		ProductVariationWrapper item = getItem(position);
		ProductVariation.ParseClass pv = item.getProductVariation();
		String name = pv.getProduct().getName();

		Variation.ParseClass variation = pv.getVariation();
		if (variation != null) {
			name += " (" + variation.getName() + ")";
		}

		vh.txtItemName.setText(name);
		float unitPrice = pv.getPrice();
		int qty = item.getQuantity();
		if (qty > 1) {
			vh.txtUnitPrice.setVisibility(View.VISIBLE);
			vh.txtMultiplier.setVisibility(View.VISIBLE);
			vh.txtUnitPrice.setText(DECIMAL_FORMAT.format(unitPrice));
			vh.txtMultiplier.setText("×" + qty);
			vh.txtTotalPrice.setText(DECIMAL_FORMAT.format(unitPrice * qty));
		} else {
			vh.txtUnitPrice.setVisibility(View.GONE);
			vh.txtMultiplier.setVisibility(View.GONE);
			vh.txtTotalPrice.setText(DECIMAL_FORMAT.format(unitPrice));
		}
	}

	public static class ViewHolder {
		TextView txtItemName, txtUnitPrice, txtMultiplier, txtTotalPrice;
	}
}