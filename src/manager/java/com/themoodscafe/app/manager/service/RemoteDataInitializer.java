package com.themoodscafe.app.manager.service;

import android.support.annotation.NonNull;
import android.util.Log;
import com.parse.*;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.model.parse.BaseEntry.BaseParseClass;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ian on 06/05/15.
 */
public final class RemoteDataInitializer implements SaveCallback {
	private static final String                               TAG                         = RemoteDataInitializer.class.getSimpleName();
	private static final RemoteDataInitializer                INSTANCE                    = new RemoteDataInitializer();
	private final        List<StockStatus.ParseClass>         STOCK_STATUSES              = new ArrayList<>();
	private final        List<OrderStatus.ParseClass>         ORDER_STATUSES              = new ArrayList<>();
	private final        List<Variation.ParseClass>           VARIATIONS                  = new ArrayList<>();
	private final        List<ProductMainCategory.ParseClass> PRODUCT_MAIN_CATEGORIES     = new ArrayList<>();
	private final        List<ProductSubCategory.ParseClass>  PRODUCT_SUB_CATEGORIES      = new ArrayList<>();
	private final        List<Product.ParseClass>             PRODUCTS                    = new ArrayList<>();
	private final        List<ProductVariation.ParseClass>    PRODUCT_VARIATIONS          = new ArrayList<>();
	private final        List<Addon.ParseClass>               ADDONS                      = new ArrayList<>();
	private final        List<IngredientCategory.ParseClass>  INGREDIENT_CATEGORIES       = new ArrayList<>();
	private final        List<Ingredient.ParseClass>          INGREDIENTS                 = new ArrayList<>();
	private final        List<IngredientVariation.ParseClass> INGREDIENT_VARIATIONS       = new ArrayList<>();
	private final        Counter                              COUNTER                     = new Counter();
	private final        ParseACL                             roleAcl                     = new ParseACL();
	private              int                                  sortIndexProduct            = 0;
	private              int                                  sortIndexAddon              = 0;
	private              int                                  sortIndexIngredient         = 0;
	private              int                                  productSubCategorySortIndex = 0;
	private ParseRole adminRole;
	private ParseRole managerRole;
	private ParseRole staffRole;

	private static <T extends BaseParseClass> T add(List<T> list, T item) {
		list.add(item);
		return item;
	}

	public static void initialize() {
		INSTANCE.init();
	}

	public void init() {
		initContent();
		initializeStaffRole();
	}

	private StockStatus.ParseClass addStockStatus(@NonNull String name) {
		return add(STOCK_STATUSES, new StockStatus.ParseClass(name));
	}

	private OrderStatus.ParseClass addOrderStatus(@NonNull String name, int sequence) {
		return add(ORDER_STATUSES, new OrderStatus.ParseClass(name, sequence));
	}

	private Variation.ParseClass addVariation(@NonNull String name, int sortIndex) {
		return add(VARIATIONS, new Variation.ParseClass(name, sortIndex));
	}

	private ProductMainCategory.ParseClass addProductMainCategory(@NonNull String name, int sortIndex) {
		return add(PRODUCT_MAIN_CATEGORIES, new ProductMainCategory.ParseClass(name, sortIndex));
	}

	private ProductSubCategory.ParseClass addProductSubCategory(@NonNull String name, @NonNull ProductMainCategory.ParseClass parent) {
		productSubCategorySortIndex += 1;
		return add(PRODUCT_SUB_CATEGORIES, new ProductSubCategory.ParseClass(name, parent, productSubCategorySortIndex));
	}

	private Product.ParseClass addProduct(@NonNull String name, @NonNull ProductSubCategory.ParseClass productCategory) {
		sortIndexProduct += 1;
		return add(PRODUCTS, new Product.ParseClass(name, productCategory, sortIndexProduct));
	}

	private ProductVariation.ParseClass addProductVariation(@NonNull Product.ParseClass product, @NonNull Variation.ParseClass variation, float price) {
		return add(PRODUCT_VARIATIONS, new ProductVariation.ParseClass(product, variation, price));
	}

	private ProductVariation.ParseClass addProductVariation(@NonNull Product.ParseClass product, @NonNull Variation.ParseClass variation, double price) {
		return addProductVariation(product, variation, (float) price);
	}

	private ProductVariation.ParseClass addProductVariation(@NonNull Product.ParseClass product, float price) {
		return add(PRODUCT_VARIATIONS, new ProductVariation.ParseClass(product, price));
	}

	private ProductVariation.ParseClass addProductVariation(@NonNull Product.ParseClass product, double price) {
		return addProductVariation(product, (float) price);
	}

	private Addon.ParseClass addAddon(@NonNull String name, @NonNull ProductSubCategory.ParseClass productCategory, float price) {
		sortIndexAddon += 1;
		return add(ADDONS, new Addon.ParseClass(name, productCategory, price, sortIndexAddon));
	}

	private IngredientCategory.ParseClass addIngredientCategory(@NonNull String name, @NonNull Product.ParseClass product, int minPerRecipe, int maxPerRecipe, int sortIndex) {
		return add(INGREDIENT_CATEGORIES, new IngredientCategory.ParseClass(name, product, minPerRecipe, maxPerRecipe, sortIndex));
	}

	private Ingredient.ParseClass addIngredient(@NonNull String name, @NonNull IngredientCategory.ParseClass ingredientCategory) {
		sortIndexIngredient += 1;
		return add(INGREDIENTS, new Ingredient.ParseClass(name, ingredientCategory, sortIndexIngredient));
	}

	private IngredientVariation.ParseClass addIngredientVariation(@NonNull Ingredient.ParseClass ingredient, @NonNull Variation.ParseClass variation, float price) {
		return add(INGREDIENT_VARIATIONS, new IngredientVariation.ParseClass(ingredient, variation, price));
	}

	private IngredientVariation.ParseClass addIngredientVariation(@NonNull String name, @NonNull IngredientCategory.ParseClass ingredientCategory, @NonNull Variation.ParseClass variation, float price) {
		return addIngredientVariation(addIngredient(name, ingredientCategory), variation, price);
	}

	private IngredientVariation.ParseClass addIngredientVariation(@NonNull Ingredient.ParseClass ingredient, float price) {
		return add(INGREDIENT_VARIATIONS, new IngredientVariation.ParseClass(ingredient, price));
	}

	private IngredientVariation.ParseClass addIngredientVariation(@NonNull String name, @NonNull IngredientCategory.ParseClass ingredientCategory, float price) {
		return addIngredientVariation(addIngredient(name, ingredientCategory), price);
	}

	private void initContent() {
		StockStatus.ParseClass available = addStockStatus("available");
		addStockStatus("hidden");
		addStockStatus("running out");

		OrderStatus.ParseClass verifying = addOrderStatus("verifying", 1);
		OrderStatus.ParseClass pending = addOrderStatus("pending", 2);
		OrderStatus.ParseClass cancelled = addOrderStatus("cancelled", 3);
		OrderStatus.ParseClass complete = addOrderStatus("complete", 4);

		Variation.ParseClass medium = addVariation("medium", 1);
		Variation.ParseClass large = addVariation("large", 2);
		Variation.ParseClass mediumBox = addVariation("medium box", 3);
		Variation.ParseClass largeBox = addVariation("large box", 4);

		ProductMainCategory.ParseClass sandwhich = addProductMainCategory("Sandwhich", 1);
		ProductMainCategory.ParseClass drink = addProductMainCategory("Drinks", 2);
		ProductMainCategory.ParseClass salad = addProductMainCategory("Salads", 3);
		ProductMainCategory.ParseClass sweet = addProductMainCategory("Sweets", 4);

		ProductSubCategory.ParseClass sandwhichPremade = addProductSubCategory("Premade Sandwhich", sandwhich);
		ProductSubCategory.ParseClass sandwhichCustom = addProductSubCategory("Custom Sandwhich", sandwhich);

		ProductSubCategory.ParseClass saladChicken = addProductSubCategory("Salads with Chicken", salad);
		ProductSubCategory.ParseClass saladOther = addProductSubCategory("Salads other", salad);

		ProductSubCategory.ParseClass frufflesMilkshake = addProductSubCategory("Fruffles Milkshake", drink);
		ProductSubCategory.ParseClass coffee = addProductSubCategory("Coffee", drink);
		ProductSubCategory.ParseClass iceCoffee = addProductSubCategory("Ice Coffee", drink);
		ProductSubCategory.ParseClass iceBlended = addProductSubCategory("Ice Blended", drink);
		ProductSubCategory.ParseClass iceTea = addProductSubCategory("Ice Tea", drink);
		ProductSubCategory.ParseClass mojito = addProductSubCategory("Mojito", drink);
		ProductSubCategory.ParseClass smoothie = addProductSubCategory("Smoothies", drink);
		ProductSubCategory.ParseClass freshJuiceAndDrinks = addProductSubCategory("Fresh Juice & Drinks", drink);

		ProductSubCategory.ParseClass moodsSpecialSweet = addProductSubCategory("Moods Special Sweets", sweet);
		ProductSubCategory.ParseClass cake = addProductSubCategory("Cake", sweet);

		/* Sandwhich */
		Product.ParseClass mixedSandwhichBox = addProduct("Mixed Sandwhich Box", sandwhichPremade);
		addProductVariation(mixedSandwhichBox, mediumBox, 60);
		addProductVariation(mixedSandwhichBox, largeBox, 110);

		Product.ParseClass makeYourOwnSandwhich = addProduct("Make Your Own Sandwhich", sandwhichCustom);
		makeYourOwnSandwhich.setHasIngredients(true);
		addProductVariation(makeYourOwnSandwhich, 0);

		/* Fruffles Milkshake */
		Product.ParseClass fmsCoconut = addProduct("Fruffles Milkshake - Coconut", frufflesMilkshake);
		Product.ParseClass fmsCocoreo = addProduct("Fruffles Milkshake - Cocoreo", frufflesMilkshake);
		Product.ParseClass fmsOreo = addProduct("Fruffles Milkshake - Oreo", frufflesMilkshake);
		Product.ParseClass fmsPeanutButter = addProduct("Fruffles Milkshake - Peanut Butter", frufflesMilkshake);
		Product.ParseClass fmsRedVelvet = addProduct("Fruffles Drink - Red Velvet", frufflesMilkshake);
		Product.ParseClass fmsWhiteVelvet = addProduct("Fruffles Milkshake - White Velvet", frufflesMilkshake);
		addProductVariation(fmsCoconut, 0);
		addProductVariation(fmsCocoreo, 0);
		addProductVariation(fmsOreo, 0);
		addProductVariation(fmsPeanutButter, 0);
		addProductVariation(fmsRedVelvet, 0);
		addProductVariation(fmsWhiteVelvet, 0);

		/* Drinks > Coffee */
		Product.ParseClass espresso = addProduct("Espresso", coffee);
		Product.ParseClass americano = addProduct("Americano", coffee);
		Product.ParseClass latte = addProduct("Latte", coffee);
		Product.ParseClass cappuccino = addProduct("Cappuccino", coffee);
		Product.ParseClass mocha = addProduct("Mocha", coffee);
		Product.ParseClass hotChocolate = addProduct("Hot Chocolate", coffee);
		Product.ParseClass caramelMocha = addProduct("Caramel Mocha", coffee);
		Product.ParseClass caramelMacchiato = addProduct("Caramel Macchiato", coffee);
		addProductVariation(espresso, 7);
		addProductVariation(americano, medium, 8);
		addProductVariation(americano, large, 10);
		addProductVariation(latte, medium, 10);
		addProductVariation(latte, large, 12);
		addProductVariation(cappuccino, medium, 10);
		addProductVariation(cappuccino, large, 12);
		addProductVariation(mocha, medium, 10);
		addProductVariation(mocha, large, 12);
		addProductVariation(hotChocolate, medium, 10);
		addProductVariation(hotChocolate, large, 12);
		addProductVariation(caramelMocha, medium, 10);
		addProductVariation(caramelMocha, large, 12);
		addProductVariation(caramelMacchiato, medium, 10);
		addProductVariation(caramelMacchiato, large, 12);

		/* Drinks > Ice Coffee */
		Product.ParseClass iceAmericano = addProduct("Ice Americano", iceCoffee);
		Product.ParseClass iceMocha = addProduct("Ice Mocha", iceCoffee);
		Product.ParseClass iceChocolate = addProduct("Ice Chocolate", iceCoffee);
		Product.ParseClass iceCaramelMacchiato = addProduct("Ice Caramel Macchiato", iceCoffee);
		Product.ParseClass iceLatte = addProduct("Ice Latte", iceCoffee);
		addProductVariation(iceAmericano, medium, 13);
		addProductVariation(iceAmericano, large, 15);
		addProductVariation(iceMocha, medium, 13);
		addProductVariation(iceMocha, large, 15);
		addProductVariation(iceChocolate, medium, 13);
		addProductVariation(iceChocolate, large, 15);
		addProductVariation(iceCaramelMacchiato, medium, 13);
		addProductVariation(iceCaramelMacchiato, large, 15);
		addProductVariation(iceLatte, medium, 13);
		addProductVariation(iceLatte, large, 15);

		/* Drinks > Ice Blended Coffee */
		Product.ParseClass iceBlendedVanilla = addProduct("Ice Blended Vanilla", iceBlended);
		Product.ParseClass iceBlendedCaramel = addProduct("Ice Blended Caramel", iceBlended);
		Product.ParseClass iceBlendedHazelnut = addProduct("Ice Blended Hazelnut", iceBlended);
		Product.ParseClass iceBlendedDarkChoco = addProduct("Ice Blended Dark Chocolate", iceBlended);
		Product.ParseClass iceBlendedWhiteChoco = addProduct("Ice Blended White Chocolate", iceBlended);
		addProductVariation(iceBlendedVanilla, medium, 13);
		addProductVariation(iceBlendedVanilla, large, 15);
		addProductVariation(iceBlendedCaramel, medium, 13);
		addProductVariation(iceBlendedCaramel, large, 15);
		addProductVariation(iceBlendedHazelnut, medium, 13);
		addProductVariation(iceBlendedHazelnut, large, 15);
		addProductVariation(iceBlendedDarkChoco, medium, 13);
		addProductVariation(iceBlendedDarkChoco, large, 15);
		addProductVariation(iceBlendedWhiteChoco, medium, 13);
		addProductVariation(iceBlendedWhiteChoco, large, 15);

		/* Drinks > Ice Tea */
		Product.ParseClass iceTeaMango = addProduct("Mango Ice Tea", iceTea);
		Product.ParseClass iceTeaPeach = addProduct("Peach Ice Tea", iceTea);
		Product.ParseClass iceTeaLemon = addProduct("Lemon Ice Tea", iceTea);
		addProductVariation(iceTeaMango, medium, 12);
		addProductVariation(iceTeaMango, large, 14);
		addProductVariation(iceTeaPeach, medium, 12);
		addProductVariation(iceTeaPeach, large, 14);
		addProductVariation(iceTeaLemon, medium, 12);
		addProductVariation(iceTeaLemon, large, 14);

		/* Drinks > Mojito */
		Product.ParseClass mojitoClassic = addProduct("Classic Mojito", mojito);
		Product.ParseClass mojitoRaspberry = addProduct("Raspberry Mojito", mojito);
		Product.ParseClass mojitoStrawberry = addProduct("Strawberry Mojito", mojito);
		Product.ParseClass mojitoMango = addProduct("Mango Mojito", mojito);
		addProductVariation(mojitoClassic, medium, 12);
		addProductVariation(mojitoClassic, large, 14);
		addProductVariation(mojitoRaspberry, medium, 12);
		addProductVariation(mojitoRaspberry, large, 14);
		addProductVariation(mojitoStrawberry, medium, 12);
		addProductVariation(mojitoStrawberry, large, 14);
		addProductVariation(mojitoMango, medium, 12);
		addProductVariation(mojitoMango, large, 14);

		/* Drinks > Smoothies */
		Product.ParseClass smoothieMango = addProduct("Mango Smoothie", smoothie);
		Product.ParseClass smoothieBanana = addProduct("Banana Smoothie", smoothie);
		Product.ParseClass smoothieRaspberry = addProduct("Raspberry Smoothie", smoothie);
		Product.ParseClass smoothieStrawberry = addProduct("Strawberry Smoothie", smoothie);
		addProductVariation(smoothieMango, medium, 11);
		addProductVariation(smoothieMango, large, 13);
		addProductVariation(smoothieBanana, medium, 11);
		addProductVariation(smoothieBanana, large, 13);
		addProductVariation(smoothieRaspberry, medium, 11);
		addProductVariation(smoothieRaspberry, large, 13);
		addProductVariation(smoothieStrawberry, medium, 11);
		addProductVariation(smoothieStrawberry, large, 13);

		/* Drinks > Fresh Juice & Drinks */
		Product.ParseClass orangeJuice = addProduct("Orange Juice", freshJuiceAndDrinks);
		Product.ParseClass lemonWithMint = addProduct("Lemon with Mint", freshJuiceAndDrinks);
		Product.ParseClass pepsi = addProduct("Pepsi", freshJuiceAndDrinks);
		Product.ParseClass sevenUp = addProduct("7 Up", freshJuiceAndDrinks);
		Product.ParseClass bottledWater = addProduct("Bottled Water", freshJuiceAndDrinks);
		addProductVariation(orangeJuice, medium, 5);
		addProductVariation(orangeJuice, large, 7);
		addProductVariation(lemonWithMint, medium, 5);
		addProductVariation(lemonWithMint, large, 7);
		addProductVariation(pepsi, 1.5);
		addProductVariation(sevenUp, 1.5);
		addProductVariation(bottledWater, 1);

		/* Salads */
		Product.ParseClass saladCaesar = addProduct("Caesar's Salad", saladChicken);
		Product.ParseClass saladTuna = addProduct("Tuna Salad", saladOther);
		Product.ParseClass saladGreek = addProduct("Greek Salad", saladOther);
		Product.ParseClass saladMixedVegetable = addProduct("Mixed Vegetable Salad", saladOther);
		addProductVariation(saladCaesar, 14);
		addProductVariation(saladTuna, 15);
		addProductVariation(saladGreek, 14);
		addProductVariation(saladMixedVegetable, 13);

		/* Sweets > Moods Special */
		Product.ParseClass miniNutellaRound = addProduct("Mini Nutella Round", moodsSpecialSweet);
		Product.ParseClass miniNutellaCrepe = addProduct("Mini Nutella Crepe", moodsSpecialSweet);
		addProductVariation(miniNutellaRound, 15);
		addProductVariation(miniNutellaCrepe, 15);

		/* Sweets > Cakes */
		Product.ParseClass cakeCheeseNewYork = addProduct("New York Cheese Cake", cake);
		Product.ParseClass cakeCheeseRaspberry = addProduct("Raspberry Cheese Cake", cake);
		Product.ParseClass cakeCheeseChocoMousse = addProduct("Chocolate Mousse Cheese Cake", cake);
		Product.ParseClass cakeDoubleChcolate = addProduct("Double Chocolate Cake", cake);
		Product.ParseClass cakeCarrot = addProduct("Carrot Cake", cake);
		addProductVariation(cakeCheeseNewYork, 15);
		addProductVariation(cakeCheeseRaspberry, 15);
		addProductVariation(cakeCheeseChocoMousse, 15);
		addProductVariation(cakeDoubleChcolate, 16);
		addProductVariation(cakeCarrot, 16);

		Addon.ParseClass coffeeExtraVanilla = addAddon("Extra Vanilla Flavour", coffee, 3);
		Addon.ParseClass coffeeExtraCaramel = addAddon("Extra Caramel Flavour", coffee, 3);
		Addon.ParseClass coffeeExtraHazelnut = addAddon("Extra Hazelnut Flavour", coffee, 3);
		Addon.ParseClass saladExtraChicken = addAddon("Extra Chicken", saladChicken, 2);
		Addon.ParseClass moodSpecialExtraStrawberry = addAddon("Extra Strawberry", moodsSpecialSweet, 3);
		Addon.ParseClass moodSpecialExtraBlueberry = addAddon("Extra Blueberry", moodsSpecialSweet, 3);
		Addon.ParseClass sandwhichExtraCheese = addAddon("Extra Cheese", sandwhichCustom, 2);

		IngredientCategory.ParseClass breadType = addIngredientCategory("Bread Type", makeYourOwnSandwhich, 0, 1, 1);
		IngredientCategory.ParseClass fillingType = addIngredientCategory("Filling Type", makeYourOwnSandwhich, 0, 1, 2);
		IngredientCategory.ParseClass vegetableType = addIngredientCategory("Vegetable Type", makeYourOwnSandwhich, 0, -1, 3);
		IngredientCategory.ParseClass sauceType = addIngredientCategory("Sauce Type", makeYourOwnSandwhich, 0, -1, 4);

		/* Bread Type */
		addIngredientVariation("Ciabatta Oregano", breadType, 14);
		addIngredientVariation("Ciabatta Brown", breadType, 14);
		addIngredientVariation("Ciabatta White", breadType, 14);
		addIngredientVariation("Ciabatta Sun-dried Tomato", breadType, 14);
		addIngredientVariation("Ciabatta Olive", breadType, 14);
		addIngredientVariation("Tortilla", breadType, 12);
		addIngredientVariation("White Toast", breadType, 11);
		addIngredientVariation("Brown Toast", breadType, 11);

		/* Filling Type */
		addIngredientVariation("Chicken Mayonnaise", fillingType, 0);
		addIngredientVariation("Tuna Maynnaise", fillingType, 0);
		addIngredientVariation("Egg Mayonnaise", fillingType, 0);
		addIngredientVariation("Chicken Tikka", fillingType, 0);
		addIngredientVariation("Chicken Breast Fillet", fillingType, 0);
		addIngredientVariation("Omelette", fillingType, 0);
		addIngredientVariation("Mexican Chicken", fillingType, 0);
		addIngredientVariation("Steak Beef", fillingType, 0);
		addIngredientVariation("Turkey", fillingType, 0);
		addIngredientVariation("Halumi", fillingType, 0);

		/* Vegetable Type */
		addIngredientVariation("Lettuce", vegetableType, 0);
		addIngredientVariation("Cucumber", vegetableType, 0);
		addIngredientVariation("Tomato", vegetableType, 0);

		/* Sauce Type */
		addIngredientVariation("Pesto", sauceType, 0);
		addIngredientVariation("BBQ", sauceType, 0);
		addIngredientVariation("Honey Mustard", sauceType, 0);
		addIngredientVariation("Chipotle", sauceType, 0);
		addIngredientVariation("Sun Dried Tomato", sauceType, 0);
	}

	private void saveAdminItems(ParseRole admin) {
		ParseACL acl = new ParseACL();
		acl.setPublicReadAccess(true);
		acl.setPublicWriteAccess(false);
		acl.setRoleWriteAccess(admin, true);

		for (ParseObject obj : STOCK_STATUSES) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : ORDER_STATUSES) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
	}

	private void saveManagerItems(ParseRole manager) {
		ParseACL acl = new ParseACL();
		acl.setPublicReadAccess(true);
		acl.setPublicWriteAccess(false);
		acl.setRoleWriteAccess(manager, true);

		for (ParseObject obj : VARIATIONS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : PRODUCT_MAIN_CATEGORIES) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : PRODUCT_SUB_CATEGORIES) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : INGREDIENT_CATEGORIES) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : PRODUCTS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : PRODUCT_VARIATIONS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : INGREDIENTS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : INGREDIENT_VARIATIONS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
		for (ParseObject obj : ADDONS) {
			obj.setACL(acl);
			addPending();
			obj.saveInBackground(INSTANCE);
		}
	}

	private void initializeStaffRole() {
		roleAcl.setPublicReadAccess(true);
		staffRole = new ParseRole("Staff", roleAcl);
		staffRole.saveInBackground(new SimpleSaveCallback("staffRole") {
			@Override
			protected void onSuccess() {
				initializeManagerRole();
			}
		});
	}

	private void initializeManagerRole() {
		managerRole = new ParseRole("Manager", roleAcl);
		managerRole.getRoles().add(staffRole);
		managerRole.saveInBackground(new SimpleSaveCallback("managerRole") {
			@Override
			protected void onSuccess() {
				saveManagerItems(managerRole);
				initializeAdminRole();
			}
		});
	}

	private void initializeAdminRole() {
		adminRole = new ParseRole("Administrator", roleAcl);
		adminRole.getRoles().add(managerRole);
		adminRole.saveInBackground(new SimpleSaveCallback("adminRole") {
			@Override
			protected void onSuccess() {
				saveAdminItems(adminRole);
			}
		});
	}

	private void addPending() {
		synchronized (COUNTER) {
			COUNTER.pendingCOunt += 1;
			Log.v(TAG, "Pending: " + COUNTER.pendingCOunt);
		}
	}

	@Override
	public void done(ParseException e) {
		synchronized (COUNTER) {
			String message;
			String error = null;
			if (e != null) {
				COUNTER.errorCount += 1;
				error = "Error " + COUNTER.errorCount + ": " + e.getMessage();
			} else {
				COUNTER.saveCount += 1;
			}
			message = "  Pending: " + COUNTER.pendingCOunt
					+ "  Saved: " + COUNTER.saveCount;
			if (error != null) {
				message += "  Error: " + error;
			}
			Log.v(TAG, message);
		}
	}

	private abstract static class SimpleSaveCallback implements SaveCallback {
		private final String tag;

		public SimpleSaveCallback(String tag) {
			this.tag = tag;
		}

		protected abstract void onSuccess();

		@Override
		public void done(ParseException e) {
			if (e == null) {
				onSuccess();
			} else {
				Log.v(TAG, "[" + tag + "] Error: " + e.getMessage());
			}
		}
	}

	private class Counter {
		int saveCount    = 0;
		int errorCount   = 0;
		int pendingCOunt = 0;
	}

}
