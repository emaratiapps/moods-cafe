package com.themoodscafe.app.client.view.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.nineoldandroids.view.ViewHelper;
import com.themoodscafe.app.R;
import com.themoodscafe.app.client.view.fragment.OrderSelectFragment;
import com.themoodscafe.app.util.Email;
import com.themoodscafe.app.util.WebUtil;

public class MainActivity extends FragmentActivity implements
		OnClickListener,
		DrawerListener
{
	private DrawerLayout drawerLayout;
	private ViewGroup    rightDrawer;
	private ImageButton  ibtnBack;
	private ImageView    imgLogo;

	private int backButtonOffset = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_client);
		if (getResources().getBoolean(R.bool.portrait_only)) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		drawerLayout.setDrawerListener(this);

		rightDrawer = (ViewGroup) findViewById(R.id.right_drawer);
		setScreenPercentage(rightDrawer, 0.66f);

		ibtnBack = (ImageButton) findViewById(R.id.ibtn_back);
		ibtnBack.setOnClickListener(this);
		backButtonOffset = ibtnBack.getDrawable().getIntrinsicWidth() * 2;
		ViewHelper.setTranslationX(ibtnBack, -backButtonOffset);

		imgLogo = (ImageView) findViewById(R.id.img_logo);
		imgLogo.setOnClickListener(this);

		int[] ids = {
				R.id.ibtn_open_drawer,
				R.id.btn_about,
				R.id.btn_share,
				R.id.btn_rate,
				R.id.btn_feedback,
				R.id.btn_developer
		};

		for (int id : ids) {
			findViewById(id).setOnClickListener(this);
		}

		initilizeButtons();
	}

	private void setScreenPercentage(@NonNull final ViewGroup drawer, float percent) {
		int width = Math.round(getResources().getDisplayMetrics().widthPixels * percent);
		DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) drawer.getLayoutParams();
		params.width = width;
		drawer.setLayoutParams(params);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ibtn_back:
			onBackPressed();
			break;

		case R.id.ibtn_open_drawer:
			if (!drawerLayout.isDrawerOpen(rightDrawer)) {
				drawerLayout.openDrawer(rightDrawer);
			}
			break;

		case R.id.ibtn_sandwhiches:
			openProducts(0);
			break;

		case R.id.ibtn_drinks:
			openProducts(1);
			break;

		case R.id.ibtn_salads:
			openProducts(2);
			break;

		case R.id.ibtn_sweets:
			openProducts(3);
			break;

		case R.id.btn_about:
			WebUtil.browse(this, "http://themoodscafe.com");
			break;

		case R.id.btn_share:
			WebUtil.share(this);
			break;

		case R.id.btn_rate:
			WebUtil.rate(this);
			break;

		case R.id.btn_feedback:
			Email.compose(this, "feedback@emaratiapps.com");
			break;

		case R.id.btn_developer:
			WebUtil.browse(this, "http://emaratiapps.com");
			break;
		}
	}

	private final int[] CATEGORY_BUTTON_IDS = {
			R.id.ibtn_sandwhiches,
			R.id.ibtn_drinks,
			R.id.ibtn_salads,
			R.id.ibtn_sweets,
	};

	private void initilizeButtons() {
		for (int id : CATEGORY_BUTTON_IDS) {
			findViewById(id).setOnClickListener(MainActivity.this);
		}/*
		DataStoreQuery.query(ProductMainCategory.ParseClass.class, new DataStoreQueryListener<ParseClass>() {
			@Override
			public void onQuery(@NonNull ParseQuery<ParseClass> query) {
				query.whereNotEqualTo(ProductMainCategory.COLUMN_IS_DISABELD, true);
			}

			@Override
			public void onSuccess(@NonNull List<ParseClass> results) {
			}

			@Override
			public void onError(@NonNull String message) {

			}
		});	*/
	}

	private void hideMainCategories() {
		for (int id : CATEGORY_BUTTON_IDS) {
			findViewById(id).setEnabled(false);
		}
		ViewCompat.animate(findViewById(R.id.main_categories_container))
				.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime))
				.alpha(0)
				.start();
	}

	private void showMainCategories() {
		for (int id : CATEGORY_BUTTON_IDS) {
			findViewById(id).setEnabled(true);
		}
		ViewCompat.animate(findViewById(R.id.main_categories_container))
				.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime))
				.alpha(1)
				.start();
	}

	private void openProducts(int page) {
		getSupportFragmentManager()
				.beginTransaction()
				.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left)
				.replace(R.id.content, OrderSelectFragment.newInstance(page))
				.addToBackStack(null)
				.commit();

		hideMainCategories();
		showBackButton();
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset) {
	}

	@Override
	public void onDrawerOpened(View drawerView) {
	}

	@Override
	public void onDrawerClosed(View drawerView) {
	}

	@Override
	public void onDrawerStateChanged(int newState) {
	}

	private void showBackButton() {
		ibtnBack.setEnabled(true);
		int w = ibtnBack.getWidth();
		int duration = 400;

		ViewCompat.animate(imgLogo)
				.setDuration(duration)
				.translationX(w)
				.start();

		ViewCompat.animate(ibtnBack)
				.setDuration(duration)
				.translationX(0)
				.start();
	}

	private void hideBackButton() {
		ibtnBack.setEnabled(false);
		int duration = 400;

		ViewCompat.animate(imgLogo)
				.setDuration(duration)
				.translationX(0)
				.start();

		ViewCompat.animate(ibtnBack)
				.setDuration(duration)
				.translationX(-backButtonOffset)
				.start();
	}

	@Override
	public void onBackPressed() {
		FragmentManager fm = getSupportFragmentManager();
		int entries = fm.getBackStackEntryCount();
		if (entries == 1) {
			hideBackButton();
			showMainCategories();
		}
		super.onBackPressed();
	}
}

