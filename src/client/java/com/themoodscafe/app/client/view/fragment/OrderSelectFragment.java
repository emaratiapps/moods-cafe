package com.themoodscafe.app.client.view.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;
import com.squareup.picasso.Picasso;
import com.themoodscafe.app.R;
import com.themoodscafe.app.client.view.adapter.OrderSelectPagerAdapter;
import com.themoodscafe.app.client.view.adapter.ProductViewHolder.SaleItemListener;
import com.themoodscafe.app.client.view.adapter.SalesOrderAdapter;
import com.themoodscafe.app.model.ProductVariationWrapper;
import com.themoodscafe.app.model.parse.*;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQuery.FetchFrom;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.util.FontUtil;
import com.themoodscafe.app.util.NetUtil;

import java.util.List;

/**
 * Created by ian on 06/05/15.
 */
public class OrderSelectFragment extends Fragment implements
		PanelSlideListener,
		SaleItemListener,
		OnClickListener,
		OnItemClickListener,
		OnEditorActionListener
{

	private static final String TAG      = OrderSelectFragment.class.getSimpleName();
	private static final String ARG_PAGE = TAG + ".PageArg";

	private SlidingUpPanelLayout slidingContainer;
	//	private View                 panelContainer;
//	private View                 panelHeaderContainer;
	private ViewPager            viewPager;
	//	private ImageView            productContainer;
	private ImageView            imgReviewOrderHint;
	private ImageView            imgSalesOrderHighlight;
	private ImageView            imgSalesOrderItemHint;
	private View                 imgSalesOrderItemBox;
	private ImageButton          ibtnReviewOrder;
	//	private ListView             salesListView;
//	private Button               btnAddMore;
//	private Button               btnSendOrder;
	private TextView             txtSalesTotal;
	private TextView             txtAddedItem;
	private EditText             edtNotes;
	private EditText             edtEmail;
	private EditText             edtMobile;
	private CheckBox             chkTakeaway;

	private AlertDialog             saveDialog;
	private OrderSelectPagerAdapter pagerAdapter;
	private SalesOrderAdapter       salesOrderAdapter;

	private int     currentPage       = 0;
	private boolean showOrderItemHint = true;

	public OrderSelectFragment() {}

	public static OrderSelectFragment newInstance(int page) {
		Bundle args = new Bundle();
		args.putInt(ARG_PAGE, page);
		OrderSelectFragment frag = new OrderSelectFragment();
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(ARG_PAGE, currentPage);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_products, container, false);

		Bundle args = savedInstanceState != null ? savedInstanceState : getArguments();
		if (args != null) {
			currentPage = args.getInt(ARG_PAGE, currentPage);
		}

		slidingContainer = (SlidingUpPanelLayout) root.findViewById(R.id.sliding_container);
		slidingContainer.setPanelSlideListener(this);
		slidingContainer.setPanelState(PanelState.HIDDEN);
		slidingContainer.setDragView(R.id.drag_view);

		View panelContainer = root.findViewById(R.id.panel_container);
		panelContainer.setBackgroundColor(Color.TRANSPARENT);

//		productContainer = (ImageView) root.findViewById(R.id.selected_product_container);

		PagerTabStrip pagerTabStrip = (PagerTabStrip) root.findViewById(R.id.pager_tab_strip);

		int titleCount = pagerTabStrip.getChildCount();
		for (int i = 0; i < titleCount; ++i) {
			View view = pagerTabStrip.getChildAt(i);
			if (view instanceof TextView) {
				FontUtil.setBvoliTypeface((TextView) view);
			}
		}

		pagerAdapter = new OrderSelectPagerAdapter(getChildFragmentManager());
		pagerAdapter.setItemListener(this);

		viewPager = (ViewPager) root.findViewById(R.id.view_pager);
		viewPager.setAdapter(pagerAdapter);

		salesOrderAdapter = new SalesOrderAdapter();
		ListView salesListView = (ListView) root.findViewById(R.id.sales_list_view);
		salesListView.setAdapter(salesOrderAdapter);
		salesListView.setOnItemClickListener(this);

		ibtnReviewOrder = (ImageButton) root.findViewById(R.id.ibtn_review_order);
		ibtnReviewOrder.setOnClickListener(this);

		imgReviewOrderHint = (ImageView) root.findViewById(R.id.img_review_order_hint);
		imgReviewOrderHint.setVisibility(View.INVISIBLE);
		imgSalesOrderItemHint = (ImageView) root.findViewById(R.id.img_sales_order_item_hint);
		imgSalesOrderItemBox = root.findViewById(R.id.sales_order_item_box);

		imgSalesOrderHighlight = (ImageView) root.findViewById(R.id.img_review_order_highlight);

		txtAddedItem = (TextView) root.findViewById(R.id.txt_added_item);
		txtAddedItem.setVisibility(View.INVISIBLE);

		toggleReviewOrderButton(false);

		Button btnAddMore = (Button) root.findViewById(R.id.btn_add_more);
		btnAddMore.getBackground().setColorFilter(new LightingColorFilter(0xFF202020, 0xFF897965));
		btnAddMore.setOnClickListener(this);

		Button btnSendOrder = (Button) root.findViewById(R.id.btn_send_order);
		btnSendOrder.getBackground().setColorFilter(0xFFA50913, Mode.MULTIPLY);
		btnSendOrder.setOnClickListener(this);

		txtSalesTotal = (TextView) root.findViewById(R.id.txt_sales_total);
		txtSalesTotal.setText("0");

		chkTakeaway = (CheckBox) root.findViewById(R.id.chk_takeaway);
		edtNotes = (EditText) root.findViewById(R.id.edt_notes);
		edtEmail = (EditText) root.findViewById(R.id.edt_email);
		edtMobile = (EditText) root.findViewById(R.id.edt_mobile);

		edtNotes.addTextChangedListener(new RemoveErrorTextWatcher(edtNotes));
		edtNotes.setOnEditorActionListener(this);
		edtEmail.addTextChangedListener(new RemoveErrorTextWatcher(edtEmail));
		edtEmail.setOnEditorActionListener(this);
		edtMobile.addTextChangedListener(new PhoneNumberTextWatcher(edtMobile));
		edtMobile.setOnEditorActionListener(this);

		FontUtil.setBvoliTypeface(btnAddMore);
		FontUtil.setBvoliTypeface(btnSendOrder);
		FontUtil.setBvoliTypeface(txtSalesTotal);
		FontUtil.setBvoliTypeface(chkTakeaway);
		FontUtil.setBvoliTypeface((TextView) root.findViewById(R.id.txt_total_label));
		FontUtil.setBvoliTypeface((TextView) root.findViewById(R.id.txt_currency));
		FontUtil.setBvoliTypeface((TextView) root.findViewById(R.id.txt_required));
		FontUtil.setBvoliTypeface(edtNotes);
		FontUtil.setBvoliTypeface(edtEmail);
		FontUtil.setBvoliTypeface(edtMobile);

		queryCategories();
		return root;
	}

	private void queryCategories() {
		DataStoreQuery.query(ProductMainCategory.ParseClass.class, new DataStoreQueryListener<ProductMainCategory.ParseClass>() {
			@Override
			public void onQuery(@NonNull ParseQuery<ProductMainCategory.ParseClass> query) {
				query.whereNotEqualTo(ProductMainCategory.COLUMN_IS_DISABELD, true);
			}

			@Override
			public void onSuccess(@NonNull List<ProductMainCategory.ParseClass> results) {
				int size = results.size();
				if (size > 0) {
					pagerAdapter.update(results);
					currentPage = currentPage % size;
					viewPager.setCurrentItem(currentPage);
				}
			}

			@Override
			public void onError(@NonNull String reason) {
				Log.e(TAG, reason);
			}

		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ibtn_review_order:
			slidingContainer.setPanelState(PanelState.EXPANDED);
			break;

		case R.id.btn_add_more:
			closeImeInput();
			slidingContainer.setPanelState(PanelState.HIDDEN);
			break;

		case R.id.btn_send_order:
			closeImeInput();
			initiateSend();
			break;
		}
	}

	private void toggleReviewOrderButton(boolean isEnabled) {
		ibtnReviewOrder.setEnabled(isEnabled);
		ViewCompat.setAlpha(ibtnReviewOrder, isEnabled ? 1 : 0.33f);
		if (slidingContainer.getPanelState() != PanelState.HIDDEN) {
			slidingContainer.setPanelState(PanelState.HIDDEN);
		}
	}

	private void updateNetTotal() {
		txtSalesTotal.setText(salesOrderAdapter.getNetTotalString());
	}

	@Override
	public void onProductSelected(@NonNull ProductVariation.ParseClass productVariation) {
		int quantity = salesOrderAdapter.add(productVariation);
		updateNetTotal();
		toggleReviewOrderButton(true);

		if (salesOrderAdapter.getOrderCount() == 1) {
			imgReviewOrderHint.setVisibility(View.VISIBLE);
			imgReviewOrderHint.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.point_down_hint));
		}

		Drawable bg = imgSalesOrderHighlight.getBackground();
		if (bg instanceof AnimationDrawable) {
			AnimationDrawable animationBg = (AnimationDrawable) bg;
			if (animationBg.isRunning()) {
				animationBg.stop();
			}
			animationBg.start();
		}

		String name = productVariation.getProduct().getName();
		Variation.ParseClass variation = productVariation.getVariation();
		if (variation != null) {
			name += " (" + variation.getName() + ")";
		}

		if (quantity > 1) {
			name += " ×" + quantity;
		}

		txtAddedItem.setText(name);
		txtAddedItem.setVisibility(View.VISIBLE);
		txtAddedItem.clearAnimation();
		txtAddedItem.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_slide_out));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

		final View itemView = salesOrderAdapter.inflate(parent, R.layout.sales_order_item_dialog);
		Object tag = itemView.getTag(R.id.tag_view_holder);
		if (tag instanceof SalesOrderAdapter.ViewHolder) {
			salesOrderAdapter.populate((SalesOrderAdapter.ViewHolder) tag, position);
		}

		Product.ParseClass product = salesOrderAdapter.getItem(position).getProductVariation().getProduct();
		if (product != null) {
			ParseFile file = product.getImage();
			if (file != null) {
				String url = file.getUrl();
				Picasso.with(getActivity()).load(url).into((ImageView) itemView.findViewById(R.id.img_preview));
			}
		}

		final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
				.setView(itemView)
				.setNeutralButton(android.R.string.ok, null)
				.setNegativeButton("-1", null)
				.setPositiveButton("+1", null)
				.create();

		alertDialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				alertDialog
						.getButton(AlertDialog.BUTTON_NEUTRAL)
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								alertDialog.dismiss();
							}
						});

				alertDialog
						.getButton(AlertDialog.BUTTON_POSITIVE)
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								salesOrderAdapter.increaseItem(position, 1);
								updateNetTotal();

								Object tag = itemView.getTag(R.id.tag_view_holder);
								if (tag instanceof SalesOrderAdapter.ViewHolder) {
									salesOrderAdapter.populate((SalesOrderAdapter.ViewHolder) tag, position);
								}
							}
						});

				alertDialog
						.getButton(AlertDialog.BUTTON_NEGATIVE)
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								int quantity = salesOrderAdapter.decreaseItem(position, 1);
								updateNetTotal();

								if (quantity > 0) {
									Object tag = itemView.getTag(R.id.tag_view_holder);
									if (tag instanceof SalesOrderAdapter.ViewHolder) {
										salesOrderAdapter.populate((SalesOrderAdapter.ViewHolder) tag, position);
									}
								} else {
									alertDialog.dismiss();
								}

								if (salesOrderAdapter.getCount() == 0) {
									toggleReviewOrderButton(false);
								}
							}
						});
			}
		});

		alertDialog.show();
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (actionId == EditorInfo.IME_ACTION_SEND) {
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			return true;
		}
		return false;
	}

	private void closeImeInput() {
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edtMobile.getWindowToken(), 0);
	}

	@Override
	public void onPanelSlide(View view, float v) {}

	@Override
	public void onPanelCollapsed(View view) {
		closeImeInput();
	}

	@Override
	public void onPanelExpanded(View view) {
		if (showOrderItemHint) {
			showOrderItemHint = false;
			imgSalesOrderItemHint.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.point_down_hint));
			ViewCompat.animate(imgSalesOrderItemBox).alpha(0).setDuration(300).setStartDelay(1300).start();
		}
	}

	@Override
	public void onPanelAnchored(View view) {}

	@Override
	public void onPanelHidden(View view) {}

	private boolean validateForm() {
		boolean valid = true;

		Editable editableEmail = edtEmail.getText();
		String email = editableEmail != null ? editableEmail.toString() : "";
		if (!"".equals(email) && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
			edtEmail.setError("Invalid email address");
			valid = false;
		}

		if ("".equals(edtMobile.getText().toString())) {
			edtMobile.setError("Required for confirmation");
			valid = false;
		}

		return valid;
	}

	private boolean isOnline() {
		if (!NetUtil.isOnline()) {
			new AlertDialog.Builder(getActivity())
					.setMessage("To send your order, connect your WiFi")
					.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							initiateSend();
						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.show();

			return false;
		}
		return true;
	}

	private void initiateSend() {
		if (!validateForm() || !isOnline()) return;

		if (saveDialog != null) {
			saveDialog.dismiss();
		}

		saveDialog = new AlertDialog.Builder(getActivity())
				.setMessage("Sending order...")
				.setCancelable(false)
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						saveDialog = null;
					}
				})
				.create();
		saveDialog.show();

		/* TODO: Make remote queries */
		new DataStoreQuery<>(Customer.ParseClass.class)
				.setListener(new DataStoreQueryListener<Customer.ParseClass>() {
					@Override
					public void onQuery(@NonNull ParseQuery<Customer.ParseClass> query) {
						query.whereEqualTo(Customer.COLUMN_EMAIL, edtEmail.getText().toString());
					}

					@Override
					public void onSuccess(@NonNull List<Customer.ParseClass> results) {
						if (saveDialog == null) return;

						if (results.size() > 0) {
							savePhoneNumber(results.get(0));
						} else {
							final Customer.ParseClass newCustomer = new Customer.ParseClass();
							String email = edtEmail.getText().toString();
							newCustomer.setEmail(email);
							newCustomer.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										onSendError(e.getMessage());
									} else {
										savePhoneNumber(newCustomer);
									}
								}
							});
						}
					}

					@Override
					public void onError(@NonNull String message) {
						onSendError(message);
					}
				})
				.execute();
	}

	private void onSendError(@NonNull String message) {
		if (saveDialog != null) {
			saveDialog.dismiss();
			saveDialog = null;

			new AlertDialog.Builder(getActivity())
					.setTitle("Order not sent")
					.setIcon(android.R.drawable.stat_sys_warning)
					.setMessage("Unable to process your request. Try again later")
					.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.show();
		}
		Log.e(TAG, message);
	}

	private void savePhoneNumber(@NonNull final Customer.ParseClass customer) {
		new DataStoreQuery<>(PhoneNumber.ParseClass.class)
				.setListener(new DataStoreQueryListener<PhoneNumber.ParseClass>() {
					@Override
					public void onQuery(@NonNull ParseQuery<PhoneNumber.ParseClass> query) {
						query.whereEqualTo(PhoneNumber.COLUMN_CUSTOMER, customer);
						query.whereEqualTo(PhoneNumber.COLUMN_PHONE_NUMBER, edtMobile.getText().toString());
					}

					@Override
					public void onSuccess(@NonNull List<PhoneNumber.ParseClass> results) {
						if (results.size() > 0) {
							saveOrder(customer, results.get(0));
						} else {
							final PhoneNumber.ParseClass newPhoneNumber = new PhoneNumber.ParseClass();
							newPhoneNumber.setCustomer(customer);
							newPhoneNumber.setPhoneNumber(edtMobile.getText().toString());
							newPhoneNumber.setIsPrimary(results.size() == 0);
							newPhoneNumber.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										onSendError(e.getMessage());
									} else {
										saveOrder(customer, newPhoneNumber);
									}
								}
							});
						}
					}

					@Override
					public void onError(@NonNull String message) {
						onSendError(message);
					}
				})
				.execute();
	}

	private void saveOrder(@NonNull final Customer.ParseClass customer, @NonNull final PhoneNumber.ParseClass phoneNumber) {
		final Order.ParseClass order = new Order.ParseClass();
		order.setCustomer(customer);
		order.setPriceTotal(salesOrderAdapter.getNetTotal());
		order.setIsTakeAway(chkTakeaway.isChecked());
		order.setNote(edtNotes.getText().toString());
		order.setPhoneNumber(phoneNumber);
		order.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null) {
					onSendError(e.getMessage());
					return;
				}

				saveOrderItem(order, 0);
			}
		});
	}

	private void saveOrderItem(@NonNull final Order.ParseClass order, final int position) {
		if (saveDialog == null) {
			return;
		}

		if (position >= salesOrderAdapter.getCount()) {
			setOrderStatus(order);
			return;
		}

		ProductVariationWrapper pvw = salesOrderAdapter.getItem(position);
		OrderItem.ParseClass orderItem = new OrderItem.ParseClass();
		orderItem.setProductVariation(pvw.getProductVariation());
		orderItem.setQuantity(pvw.getQuantity());
		orderItem.setOrder(order);
		orderItem.saveInBackground(new SaveCallback() {
			@Override
			public void done(ParseException e) {
				if (e != null) {
					onSendError(e.getMessage());
					return;
				}

				saveOrderItem(order, position + 1);
			}
		});
	}

	private void setOrderStatus(@NonNull final Order.ParseClass order) {
		new DataStoreQuery<>(OrderStatus.ParseClass.class)
				.setFetchFrom(FetchFrom.BOTH_STORAGE)
				.setListener(new DataStoreQueryListener<OrderStatus.ParseClass>() {
					@Override
					public void onQuery(@NonNull ParseQuery<OrderStatus.ParseClass> query) {
						query.whereEqualTo(OrderStatus.COLUMN_SEQUENCE, 1);
					}

					@Override
					public void onSuccess(@NonNull List<OrderStatus.ParseClass> results) {
						if (results.size() > 0) {
							order.setOrderStatus(results.get(0));
							order.saveInBackground(new SaveCallback() {
								@Override
								public void done(ParseException e) {
									if (e != null) {
										onSendError(e.getMessage());
									} else {
										onSaveComplete();
									}
								}
							});
						} else {
							onSendError("No results for OrderStatus.ParseClass");
						}
					}

					@Override
					public void onError(@NonNull String message) {
						onSendError(message);
					}
				})
				.execute();
	}

	private void onSaveComplete() {
		saveDialog.dismiss();
		saveDialog = null;

		new AlertDialog.Builder(getActivity())
				.setTitle("Order successful")
				.setMessage("Your order is now being processed. Please wait for a call confirmation")
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						getActivity().onBackPressed();
					}
				})
				.show();
	}

	private static class RemoveErrorTextWatcher implements TextWatcher {
		private final EditText editText;

		public RemoveErrorTextWatcher(@NonNull EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {}

		@Override
		public void afterTextChanged(Editable s) {
			editText.setError(null);
		}
	}

	private static class PhoneNumberTextWatcher extends PhoneNumberFormattingTextWatcher {
		private final EditText editText;

		public PhoneNumberTextWatcher(@NonNull EditText editText) {
			super();
			this.editText = editText;
		}

		@Override
		public synchronized void afterTextChanged(Editable s) {
			super.afterTextChanged(s);
			editText.setError(null);
		}
	}
}
