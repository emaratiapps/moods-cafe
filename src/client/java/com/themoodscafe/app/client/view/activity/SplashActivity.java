package com.themoodscafe.app.client.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.themoodscafe.app.R;
import com.themoodscafe.app.util.NetUtil;
import com.themoodscafe.app.util.ParseUpdater;
import com.themoodscafe.app.util.ParseUpdater.InitializeListener;
import com.themoodscafe.app.util.Prefers;

/**
 * Created by ian on 10/07/14.
 */
public class SplashActivity extends Activity implements InitializeListener, Runnable {

	private static final String TAG              = SplashActivity.class.getSimpleName();
	private static final String ARG_FIRST_UPDATE = TAG + ".FirstUpdateArg";

	private TextView    textView;
	private TextView    warningText;
	private ProgressBar progressBar;
//	private String errorText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		progressBar = (ProgressBar) findViewById(R.id.progress_bar);
		textView = (TextView) findViewById(R.id.text);
		warningText = (TextView) findViewById(R.id.text_warning);

		if (Prefers.with(this).getBoolean(ARG_FIRST_UPDATE, true)) {
			textView.setText("Loading...");
			textView.setVisibility(View.VISIBLE);
			progressBar.setVisibility(View.VISIBLE);
			warningText.setVisibility(View.INVISIBLE);
			firstUpdate();
		} else {
			textView.setVisibility(View.INVISIBLE);
			progressBar.setVisibility(View.INVISIBLE);
			backgroundUpdate();
		}
	}

	private void firstUpdate() {
		if (NetUtil.isOnline()) {
			ParseUpdater.initialize(this);
		} else {
			new AlertDialog.Builder(this)
					.setMessage("Your menu is empty. Connect your WiFi to get content")
					.setPositiveButton("Try again", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							firstUpdate();
						}
					})
					.setNegativeButton("Exit", new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					})
					.show();
		}
	}

	private void backgroundUpdate() {
		if (NetUtil.isOnline()) {
			warningText.setVisibility(View.INVISIBLE);
			ParseUpdater.initialize(null); /* TODO: Notify on update */
		} else {
			warningText.setVisibility(View.VISIBLE);
		}

		new Handler().postDelayed(this, 2000);
	}

	private void updateText(/*String className*/) {
		textView.setText("Updating catalogue... " + progressBar.getProgress() + "%");
	}

	@Override
	public void onUpdateStart(@NonNull Class<?> theClass) {
		updateText();//theClass.getCanonicalName());
	}

	@Override
	public void onUpdateComplete(@NonNull Class<?> theClass) {
		updateText();//theClass.getCanonicalName());
	}

	@Override
	public void onError(@NonNull Class<?> theClass, @NonNull String reason) {
//		errorText = "\n" + theClass.getCanonicalName() + " error: " + reason;
		updateText();//theClass.getCanonicalName());
	}

	@Override
	public void onProgress(int progress) {
		progressBar.setProgress(progress);
	}

	@Override
	public void onAllComplete() {
		Prefers.with(this).putBoolean(ARG_FIRST_UPDATE, false);
		new Handler().postDelayed(this, 1000);
	}

	@Override
	public void run() {
		startActivity(new Intent(this, MainActivity.class));
	}
}
