package com.themoodscafe.app.client.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.themoodscafe.app.R;
import com.themoodscafe.app.model.parse.ProductVariation;
import com.themoodscafe.app.model.parse.Variation;
import com.themoodscafe.app.model.parse.Variation.ParseClass;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ian on 09/05/15.
 */
public class ProductVariationAdapter extends BaseAdapter {
	private static final DecimalFormat                           DECIMAL_FORMAT = new DecimalFormat("#.#");
	private final        List<PriceItem>                         list           = new ArrayList<>();
	private final        List<GetCallback<Variation.ParseClass>> callbacks      = new ArrayList<>();

	private boolean isOpaque   = false;
//	private boolean isAnimated = true;

	private void add(@NonNull final ProductVariation.ParseClass pv) {
		final String price = DECIMAL_FORMAT.format(pv.getPrice());

		Variation.ParseClass variation = pv.getVariation();
		if (variation == null) {
			list.add(new PriceItem(pv, "", price));
			notifyDataSetChanged();
			return;
		}

		GetCallback<ParseClass> fetchCallback = new GetCallback<ParseClass>() {
			@Override
			public void done(Variation.ParseClass variation, ParseException e) {
				synchronized (callbacks) {
					if (callbacks.contains(this)) {
						callbacks.remove(this);
					} else {
						return;
					}
				}

				if (e != null) {
					return;
				}

				synchronized (list) {
					list.add(new PriceItem(pv, variation.getName(), price));
					Collections.sort(list);
					notifyDataSetChanged();
				}
			}
		};

		synchronized (callbacks) {
			callbacks.add(fetchCallback);
		}

		variation.fetchIfNeededInBackground(fetchCallback);
	}

	public boolean isOpaque() {
		return isOpaque;
	}

	public void setIsOpaque(boolean isOpaque) {
		if (this.isOpaque != isOpaque) {
			this.isOpaque = isOpaque;
//			isAnimated = !isOpaque;
			notifyDataSetChanged();
		}
	}

	public void clear() {
		synchronized (callbacks) {
			callbacks.clear();
		}
		synchronized (list) {
			list.clear();
		}
		notifyDataSetChanged();
	}

	public void addAll(@NonNull List<ProductVariation.ParseClass> list) {
		for (ProductVariation.ParseClass pv : list) {
			add(pv);
		}
	}

	public void replaceList(@NonNull List<ProductVariation.ParseClass> list) {
		clear();
		addAll(list);
	}

	@Override
	public int getCount() {
		synchronized (list) {
			return list.size();
		}
	}

	@Override
	public PriceItem getItem(int position) {
		synchronized (list) {
			return list.get(position);
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View root = convertView != null ? convertView : inflate(parent);

		Object tag = root.getTag(R.id.tag_view_holder);
		if (tag instanceof ViewHolder) {
			populate((ViewHolder) tag, position);

		}

//		if (isAnimated) {
//			animate(root);
//		}
		return root;
	}

/*	private void animate(@NonNull View root) {
		ViewCompat.setAlpha(root, 0);
		ViewCompat.animate(root).alpha(1).setDuration(100).start();
	}*/

	private View inflate(@NonNull ViewGroup parent) {
		View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_variation_list_item, parent, false);
		ViewHolder vh = new ViewHolder();
		vh.txtName = (TextView) root.findViewById(R.id.txt_name);
		vh.txtPrice = (TextView) root.findViewById(R.id.txt_price);
		vh.container = root.findViewById(R.id.container);
		root.setTag(R.id.tag_view_holder, vh);
		return root;
	}

	private void populate(@NonNull ViewHolder vh, int position) {
		PriceItem item = getItem(position);
		String name = item.getName();
		if (!"".equals(name)) {
			vh.txtName.setVisibility(View.VISIBLE);
			vh.txtName.setText(name);
		} else {
			vh.txtName.setVisibility(View.GONE);
		}

		String price = item.getPrice();
		vh.txtPrice.setText(price);

		vh.container.setBackgroundResource(
				isOpaque
				? R.drawable.bg_variation_opaque_selector
				: R.drawable.bg_variation_transparent_selector);
	}

	public static class PriceItem implements Comparable<PriceItem> {
		private final ProductVariation.ParseClass productVariation;
		private final String                      name, price;

		public PriceItem(ProductVariation.ParseClass productVariation, String name, String price) {
			this.productVariation = productVariation;
			this.name = name;
			this.price = price;
		}

		public ProductVariation.ParseClass getProductVariation() {
			return productVariation;
		}

		@NonNull
		public String getName() {
			return name;
		}

		@NonNull
		public String getPrice() {
			return price;
		}

		@Override
		public int compareTo(@NonNull PriceItem another) {
			ProductVariation.ParseClass anotherProdVar = another.getProductVariation();
			if (productVariation != null && anotherProdVar != null) {
				int sortIndex = productVariation.getSortIndex();
				int anotherSortIndex = anotherProdVar.getSortIndex();
				if (sortIndex > 0 && anotherSortIndex > 0) {
					return sortIndex - anotherSortIndex;
				}

				Variation.ParseClass variation = productVariation.getVariation();
				Variation.ParseClass anotherVariation = anotherProdVar.getVariation();
				if (variation != null && anotherVariation != null) {
					int si = variation.getSortIndex();
					int asi = anotherVariation.getSortIndex();
					if (si > 0 && asi > 0) {
						return si - asi;
					}
				}
			}
			return 0;
		}
	}

	private class ViewHolder {
		TextView txtName, txtPrice;
		View container;
	}
}