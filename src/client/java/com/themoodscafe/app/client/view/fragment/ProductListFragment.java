package com.themoodscafe.app.client.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.parse.ParseQuery;
import com.themoodscafe.app.R;
import com.themoodscafe.app.client.view.adapter.ProductListAdapter;
import com.themoodscafe.app.client.view.adapter.ProductViewHolder.SaleItemListener;
import com.themoodscafe.app.model.parse.ProductMainCategory;
import com.themoodscafe.app.model.parse.ProductMainCategory.ParseClass;
import com.themoodscafe.app.model.parse.ProductSubCategory;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.service.SimpleDataStoreQueryListener;

import java.util.List;

/**
 * Created by ian on 06/05/15.
 */
public class ProductListFragment extends Fragment implements DataStoreQueryListener<ProductSubCategory.ParseClass> {
	private static final String TAG             = ProductListFragment.class.getSimpleName();
	private static final String ARG_CATEGORY_ID = TAG + ".CategoryIdArg";

	private String categoryObjectId = "";
	private ProductListAdapter             productListAdapter;
	private SaleItemListener               itemListener;
	private ProductMainCategory.ParseClass mainCategory;

	public ProductListFragment() {}

	public static ProductListFragment newInstance(@NonNull ProductMainCategory.ParseClass category) {
		Bundle args = new Bundle();
		args.putString(ARG_CATEGORY_ID, category.getObjectId());
		ProductListFragment frag = new ProductListFragment();
		frag.setArguments(args);
		return frag;
	}

	public void setListener(SaleItemListener listener) {
		this.itemListener = listener;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(ARG_CATEGORY_ID, categoryObjectId);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = LayoutInflater.from(getActivity()).inflate(R.layout.recycler_layout, container, false);

		Bundle args = savedInstanceState != null ? savedInstanceState : getArguments();
		if (args != null) {
			String id = args.getString(ARG_CATEGORY_ID);
			if (id != null && !"".equals(id)) {
				categoryObjectId = id;
			}
		}

		DataStoreQuery.query(ProductMainCategory.ParseClass.class, new SimpleDataStoreQueryListener<ProductMainCategory.ParseClass>() {

			@Override
			public void onQuery(@NonNull ParseQuery<ParseClass> query) {
				query.whereNotEqualTo(ProductMainCategory.COLUMN_IS_DISABELD, true);
			}

			@Override
			public void onSuccess(@NonNull List<ProductMainCategory.ParseClass> results) {
				for (ProductMainCategory.ParseClass category : results) {
					if (categoryObjectId.equals(category.getObjectId())) {
						mainCategory = category;
						DataStoreQuery.query(ProductSubCategory.ParseClass.class, ProductListFragment.this);
						break;
					}
				}
			}

		});

		productListAdapter = new ProductListAdapter();
		productListAdapter.setListener(itemListener);

		LayoutManager layoutManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
		RecyclerView recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(productListAdapter);

		return root;
	}

	@Override
	public void onQuery(@NonNull ParseQuery<ProductSubCategory.ParseClass> query) {
		query.whereEqualTo(ProductSubCategory.COLUMN_PRODUCT_MAIN_CATEGORY, mainCategory);
		query.whereNotEqualTo(ProductSubCategory.COLUMN_IS_DISABELD, true);
	}

	@Override
	public void onSuccess(@NonNull List<ProductSubCategory.ParseClass> results) {
		productListAdapter.update(results);
	}

	@Override
	public void onError(@NonNull String reason) {}
}
