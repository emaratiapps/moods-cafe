package com.themoodscafe.app.client.view.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;
import com.themoodscafe.app.client.view.adapter.ProductViewHolder.SaleItemListener;
import com.themoodscafe.app.client.view.fragment.ProductListFragment;
import com.themoodscafe.app.model.parse.ProductMainCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ian on 07/05/15.
 */
public class OrderSelectPagerAdapter extends FragmentPagerAdapter {
	private final List<ProductMainCategory.ParseClass> mainCategories = new ArrayList<>();
	private final List<Fragment>                       fragments      = new ArrayList<>();

	private SaleItemListener itemListener;

	public OrderSelectPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	public void update(@NonNull List<ProductMainCategory.ParseClass> list) {
//		for (ProductMainCategory.ParseClass item : list) {
//			ProductMainCategory pmc = new ProductMainCategory();
//			item.fillEntry(pmc);
//			mainCategories.add(pmc);
//		}
		mainCategories.clear();
		mainCategories.addAll(list);
		Collections.sort(mainCategories);
		notifyDataSetChanged();
	}

	public void setItemListener(SaleItemListener itemListener) {
		this.itemListener = itemListener;
	}

	@Override
	public int getCount() {
		return mainCategories.size();
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return mainCategories.get(position).getName();
	}

	@Override
	public Fragment getItem(int position) {
		ProductListFragment frag = ProductListFragment.newInstance(mainCategories.get(position));
		frag.setListener(itemListener);
		return frag;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Object object = super.instantiateItem(container, position);
		if (object instanceof ProductListFragment) {
			ProductListFragment productListFra = (ProductListFragment) object;
			productListFra.setListener(itemListener);
		}
		return object;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		if (object instanceof ProductListFragment) {
			ProductListFragment productListFra = (ProductListFragment) object;
			productListFra.setListener(itemListener);
		}
		super.destroyItem(container, position, object);
	}
}
