package com.themoodscafe.app.client.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.squareup.picasso.Picasso;
import com.themoodscafe.app.R;
import com.themoodscafe.app.model.parse.Product;
import com.themoodscafe.app.model.parse.ProductVariation;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.util.FontUtil;

import java.util.List;

/**
 * Created by ian on 06/05/15.
 */
public class ProductViewHolder extends RecyclerView.ViewHolder implements
		OnClickListener,
		OnItemClickListener,
		DataStoreQueryListener<ProductVariation.ParseClass>
{
	private final boolean isHeader;

	private TextView                txtName;
	private ImageView               imageView;
	private ListView                listView;
	private TextView                txtSelect;
	private Product.ParseClass      product;
	private SaleItemListener        listener;
	private ProductVariationAdapter adapter;

	public ProductViewHolder(@NonNull View view, boolean isHeader) {
		super(view);
		this.isHeader = isHeader;

		txtName = (TextView) view.findViewById(R.id.txt_name);

		if (!isHeader) {
			imageView = (ImageView) view.findViewById(R.id.img_preview);

			View btnSelect = view.findViewById(R.id.btn_full_container);
			btnSelect.setOnClickListener(this);

			txtSelect = (TextView) view.findViewById(R.id.txt_select);
			txtSelect.setVisibility(View.INVISIBLE);

			listView = (ListView) view.findViewById(R.id.list_view);
			listView.setOnItemClickListener(this);
			toggleListView(false);
		}
	}

	public void bindHeader(String text, int bgColor) {
		if (!isHeader) return;
		FontUtil.setBvoliTypeface(txtName);
		txtName.setText(text);
		txtName.setBackgroundColor(bgColor);
		product = null;
	}

	public void bindContent(@NonNull Product.ParseClass product) {
		if (isHeader) return;
		this.product = product;

		String name = product.getName();
		txtName.setTypeface(null);
		txtName.setText(name);

		ParseFile imageFile = product.getImage();
		String imageUrl = imageFile != null ? imageFile.getUrl() : null;

		if (imageUrl != null) {
			Picasso.with(txtName.getContext())
					.load(imageUrl)
					.placeholder(R.drawable.product_placeholder)
					.into(imageView);
		} else {
			imageView.setImageResource(R.drawable.product_placeholder);
		}

		if (adapter == null) {
			adapter = new ProductVariationAdapter();
			listView.setAdapter(adapter);
		} else {
			adapter.clear();
		}

		contractList();
		DataStoreQuery.query(ProductVariation.ParseClass.class, this);
	}

	public void setListener(SaleItemListener listener) {
		this.listener = listener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_full_container:
			onContainerClick();
			break;
		}
	}

	private void toggleListView(boolean enabled) {
		listView.setEnabled(enabled);
		listView.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
	}

	private void onContainerClick() {
		if (adapter == null) return;

		int count = adapter.getCount();
		if (count == 1) {
			selectItem(adapter, 0);
		} else {
			toggleList();
		}
	}

	private void toggleList() {
		boolean show = !adapter.isOpaque();
		adapter.setIsOpaque(show);
		txtSelect.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
	}

	private void contractList() {
		adapter.setIsOpaque(false);
		txtSelect.setVisibility(View.INVISIBLE);
	}

	private void selectItem(@NonNull ProductVariationAdapter pvAdapter, int position) {
		ProductVariation.ParseClass pv = pvAdapter.getItem(position).getProductVariation();
		if (listener != null && pv != null) {
			listener.onProductSelected(pv);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Adapter adapter = parent.getAdapter();
		if (adapter instanceof ProductVariationAdapter) {
			selectItem((ProductVariationAdapter) adapter, position);
		}
		contractList();
	}

	@Override
	public void onQuery(@NonNull ParseQuery<ProductVariation.ParseClass> query) {
		query.whereEqualTo(ProductVariation.COLUMN_PRODUCT, product);
	}

	@Override
	public void onSuccess(@NonNull List<ProductVariation.ParseClass> results) {
		if (isHeader || product == null || results.size() == 0) {
			return;
		}

		if (!product.getObjectId().equals(results.get(0).getProduct().getObjectId())) {
			return;
		}

		adapter.replaceList(results);
		toggleListView(true);
	}

	@Override
	public void onError(@NonNull String reason) {}

	public interface SaleItemListener {
		void onProductSelected(@NonNull ProductVariation.ParseClass product);
	}
}