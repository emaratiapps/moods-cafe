package com.themoodscafe.app.client.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.parse.ParseQuery;
import com.themoodscafe.app.R;
import com.themoodscafe.app.client.view.adapter.ProductViewHolder.SaleItemListener;
import com.themoodscafe.app.model.parse.Product;
import com.themoodscafe.app.model.parse.ProductSubCategory;
import com.themoodscafe.app.service.DataStoreQuery;
import com.themoodscafe.app.service.DataStoreQueryListener;
import com.themoodscafe.app.util.ParseUpdater;

import java.util.*;

/**
 * Created by ian on 06/05/15.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductViewHolder> implements DataStoreQueryListener<Product.ParseClass> {

	private static final String TAG               = ProductListAdapter.class.getSimpleName();
	private static final int    VIEW_TYPE_HEADER  = 0;
	private static final int    VIEW_TYPE_CONTENT = 1;

	private final ArrayList<LineItem>                 lineItems     = new ArrayList<>();
	private final List<ProductSubCategory.ParseClass> subCategories = new ArrayList<>();
	private final List<Product.ParseClass>            products      = new ArrayList<>();

	private SaleItemListener listener;

	public ProductListAdapter() {}

	public void update(@NonNull List<ProductSubCategory.ParseClass> list) {
		subCategories.addAll(ParseUpdater.getNew(list, subCategories));
		Collections.sort(subCategories);
		DataStoreQuery.query(Product.ParseClass.class, this);
	}

	@Override
	public void onQuery(@NonNull ParseQuery<Product.ParseClass> query) {
		query.whereContainedIn(Product.COLUMN_PRODUCT_SUB_CATEGORY, subCategories);
		query.whereNotEqualTo(Product.COLUMN_IS_DISABELD, true);
	}

	@Override
	public void onSuccess(@NonNull List<Product.ParseClass> results) {
		synchronized (products) {

			products.addAll(ParseUpdater.getNew(results, products));
			Collections.sort(products);

			Map<String, List<Product.ParseClass>> contentMap = new HashMap<>();

			for (Product.ParseClass product : products) {
				String objectId = product.getProductSubCategory().getObjectId();
				List<Product.ParseClass> productList = contentMap.get(objectId);

				if (productList == null) {
					productList = new ArrayList<>();
				}

				productList.add(product);
				contentMap.put(objectId, productList);
			}

			lineItems.clear();
			for (ProductSubCategory.ParseClass subCategory : subCategories) {
				List<Product.ParseClass> productList = contentMap.get(subCategory.getObjectId());
				if (productList == null) continue;

				lineItems.add(new LineItem(VIEW_TYPE_HEADER, subCategory.getName()));
				for (Product.ParseClass product : productList) {
					lineItems.add(new LineItem(VIEW_TYPE_CONTENT, product));
				}
			}

			notifyDataSetChanged();
		}
	}

	@Override
	public void onError(@NonNull String reason) {

	}

	public void setListener(SaleItemListener listener) {
		this.listener = listener;
	}

	@Override
	public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		ProductViewHolder viewHolder;
		if (viewType == VIEW_TYPE_HEADER) {
			viewHolder = new ProductViewHolder(inflater.inflate(R.layout.product_list_header, parent, false), true);
		} else {
			viewHolder = new ProductViewHolder(inflater.inflate(R.layout.product_list_item, parent, false), false);
		}
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(ProductViewHolder holder, int position) {
		final LineItem item = lineItems.get(position);

		StaggeredGridLayoutManager.LayoutParams layoutParams =
				(StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();

		switch (item.viewType) {
		case VIEW_TYPE_HEADER:
			holder.bindHeader(item.name, 0xffc39965);
			holder.setListener(null);
			layoutParams.setFullSpan(true);
			break;

		case VIEW_TYPE_CONTENT:
			holder.bindContent(item.product);
			holder.setListener(listener);
			layoutParams.setFullSpan(false);
			break;
		}
	}

	@Override
	public int getItemViewType(int position) {
		return lineItems.get(position).viewType;
	}

	@Override
	public int getItemCount() {
		return lineItems.size();
	}

	private static class LineItem {
		public int     viewType;
		public String  name;
		public Product.ParseClass product;

		public LineItem(int viewType, String name) {
			this.viewType = viewType;
			this.name = name;
		}

		public LineItem(int viewType, Product.ParseClass product) {
			this.viewType = viewType;
			this.product = product;
		}
	}
}
